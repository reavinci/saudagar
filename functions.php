<?php
define('RT_THEME_NAME', wp_get_theme()->get('Name'));
define('RT_THEME_SLUG', 'saudagar');
define('RT_THEME_DOMAIN', wp_get_theme()->get('TextDomain'));
define('RT_THEME_VERSION', wp_get_theme()->get('Version'));
define('RT_THEME_URL', 'https://www.saudagarwp.com');
define('RT_THEME_DOC', 'https: //www.panduan.webforia.id/saudagar');

// include all core file
require get_template_directory() . '/core/core.php';