<?php
namespace Retheme\Admin;

class Admin
{
    public function __construct()
    {
        if (is_admin()) {
            $this->include_part();
            add_action('admin_enqueue_scripts', array($this, 'register_scripts'), 999);
            add_action('admin_notices', array($this, 'tabs'), 1);
            add_action('admin_menu', array($this, 'add_menu_panel'));
            add_action('after_setup_theme', array($this, 'add_theme_panel'));

        }

    }

    public function include_part()
    {
        include_once dirname(__FILE__) . '/functions.php';
    }

    public function register_scripts()
    {
        wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', '4.7.0');
        wp_enqueue_style('retheme-admin', get_template_directory_uri() . '/core/admin/assets/css/retheme-admin.css');
        wp_enqueue_script('retheme-admin', get_template_directory_uri() . '/core/admin/assets/js/retheme-admin.js', array('jquery'), '1.0.0', true);
    }

    public function tabs()
    {
        if (in_array(rt_admin_get_page(), rt_admin_theme_page())) {
            rt_admin_get_template_part('tab');

        }

    }

    /**
     * Add menu panel
     */
    public function add_menu_panel()
    {

        add_menu_page('Theme Panel', 'Theme Panel', 'manage_options', 'theme-panel', false, '', 999);

        add_submenu_page('theme-panel', 'Dashboard', 'Dashboard', 'manage_options', 'theme-panel', function () {
            get_template_part('core/admin/views/dashboard');
        }, 1);

        if (rt_is_premium_plan()) {
            add_submenu_page('theme-panel', 'License', 'License', 'manage_options', 'theme-license', function () {
                get_template_part('core/admin/views/license');
            }, 99);
        }

        if (rt_is_free()) {
            add_submenu_page('theme-panel', 'Upgrade Pro', '<span class="rta-menu-pro">Upgrade Pro <i class="fa fa-star"></i></span>', 'manage_options', 'theme-pro', function () {
                get_template_part('core/admin/views/upgrade-pro');
            }, 99);
        }

    }

    /**
     * Added ACF option panel
     *
     * @return void
     */
    public function add_theme_panel()
    {
        if (function_exists('acf_add_options_page') && rt_is_dev()) {

            acf_add_options_page(array(
                'page_title' => 'Theme Options',
                'menu_title' => 'Theme Options',
                'menu_slug' => 'theme-options',
                'parent_slug' => 'theme-panel',
                'position' => 1,
                'capability' => 'edit_posts',
                'redirect' => false,
            ));
        }

    }
}

new Admin();
