<?php
namespace Retheme;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Widget_Base;
use Retheme\Helper;
use Retheme\HTML;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Elementor_Base extends Widget_Base
{

    public function get_name()
    {
        return 'retheme-base';
    }

    public function setting_slider($args = '')
    {
        $slide = !empty($args['slide']) ? $args['slide'] : true;
        $spacing = !empty($args['spacing']) ? $args['spacing'] : true;
        $navigation = !empty($args['navigation']) ? $args['navigation'] : true;

        $this->start_controls_section(
            'setting_slider',
            [
                'label' => __('Slider', RT_THEME_DOMAIN),
            ]
        );

        $this->add_control(
            'slider_nav',
            [
                'label' => __('Navigation', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_off' => __('Off', RT_THEME_DOMAIN),
                'label_on' => __('On', RT_THEME_DOMAIN),
                'return_value' => 'yes',

            ]
        );

        $this->add_control(
            'slider_pagination',
            [
                'label' => __('Pagination', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', RT_THEME_DOMAIN),
                'label_on' => __('On', RT_THEME_DOMAIN),
                'return_value' => 'yes',

            ]
        );

        $this->add_control(
            'slider_loop',
            [
                'label' => __('Infinite Loop', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'false',
                'label_off' => __('Off', RT_THEME_DOMAIN),
                'label_on' => __('On', RT_THEME_DOMAIN),
                'return_value' => 'true',

            ]
        );

        $this->add_control(
            'slider_auto_play',
            [
                'label' => __('Auto Play', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'false',
                'label_on' => __('On', RT_THEME_DOMAIN),
                'label_off' => __('Off', RT_THEME_DOMAIN),
                'return_value' => 'true',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'style_slider_navigation',
            [
                'label' => __('Slider', RT_THEME_DOMAIN),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'style_slider_nav',
            [
                'label' => __('Navigation', RT_THEME_DOMAIN),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        /* start navigation color */
        $this->start_controls_tabs('slider_nav_tabs');
        $this->start_controls_tab(
            'slider_nav_normal',
            [
                'label' => __('Normal', RT_THEME_DOMAIN),
            ]
        );

        $this->add_control(
            'style_slider_navigation_color',
            [
                'label' => __('Navigation Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next i,
                         {{WRAPPER}}  .rt-slider .owl-prev i' => 'color: {{VALUE}} !important;',
                ],
            ]
        );

        $this->add_control(
            'style_slider_navigation_bg',
            [
                'label' => __('Background Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next,
                         {{WRAPPER}} .rt-slider .owl-prev' => 'background-color: {{VALUE}} !important;',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'slider_nav_hover',
            [
                'label' => __('Hover', RT_THEME_DOMAIN),
            ]
        );

        $this->add_control(
            'style_slider_navigation_color_hover',
            [
                'label' => __('Navigation Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next:hover i,
                         {{WRAPPER}}  .rt-slider .owl-prev:hover i' => 'color: {{VALUE}} !important;',
                ],
            ]
        );

        $this->add_control(
            'style_slider_navigation_bg_hover',
            [
                'label' => __('Background Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next:hover,
                         {{WRAPPER}} .rt-slider .owl-prev:hover' => 'background-color: {{VALUE}} !important;',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

        $this->add_control(
            'style_slider_navigation_radius',
            [
                'label' => __('Border Radius', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next,
                     {{WRAPPER}} .rt-slider .owl-prev' => 'border-radius: {{SIZE}}{{UNIT}}!important;',
                ],
            ]
        );

        $this->add_responsive_control(
            'style_slider_navigation_position_top',
            [
                'label' => __('Top Indent', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next,
                     {{WRAPPER}} .rt-slider .owl-prev' => 'top: {{SIZE}}{{UNIT}}!important;',
                ],
                'condition' => [
                    'slider_nav!' => 'header',
                ],
            ]
        );

        $this->add_responsive_control(
            'style_slider_navigation_position_side',
            [
                'label' => __('Side Indent', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'range' => [
                    'px' => [
                        'min' => -45,
                        'max' => 300,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-prev' => 'left: {{SIZE}}{{UNIT}}!important;',
                    '{{WRAPPER}} .rt-slider .owl-next' => 'right: {{SIZE}}{{UNIT}}!important;',
                ],
                'condition' => [
                    'slider_nav!' => 'header',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'style_slider_shadow',
                'selector' => '{{WRAPPER}} .rt-slider .owl-next,
                                   {{WRAPPER}} .rt-slider .owl-prev',
            ]
        );

        $this->add_control(
            'style_slider_pagination',
            [
                'label' => __('Pagination', RT_THEME_DOMAIN),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'pagination_color',
            [
                'label' => __('Pagination Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .owl-dot span' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'pagination_hover_color',
            [
                'label' => __('Pagination Active Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .owl-dot.active span' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

    }

    public function setting_carousel($args = '')
    {

        $defaults = array(
            'slide' => true,
            'gap' => true,
            'navigation' => true,
        );

        $settings = wp_parse_args($args, $defaults);

        $this->start_controls_section(
            'setting_carousel',
            [
                'label' => __('Carousel', RT_THEME_DOMAIN),
            ]
        );

        if (empty($args['carousel'])) {
            $this->add_control(
                'carousel',
                [
                    'label' => __('Carousel', RT_THEME_DOMAIN),
                    'type' => Controls_Manager::SWITCHER,
                    'default' => 'no',
                    'label_off' => __('Off', RT_THEME_DOMAIN),
                    'label_on' => __('On', RT_THEME_DOMAIN),
                    'return_value' => 'yes',

                ]
            );
        }

        if (!empty($settings['slide'])) {
            $slides_to_show = range(1, 10);
            $slides_to_show = array_combine($slides_to_show, $slides_to_show);
            $this->add_responsive_control(
                'slider_item',
                [
                    'label' => __('Slides to Show', RT_THEME_DOMAIN),
                    'type' => Controls_Manager::SELECT,
                    'options' => $slides_to_show,
                    'devices' => ['desktop', 'tablet', 'mobile'],
                    'desktop_default' => 4,
                    'tablet_default' => 3,
                    'mobile_default' => 1,

                ]
            );

        }
        if (!empty($settings['gap'])) {

            $this->add_control(
                'slider_gap',
                [
                    'label' => __('Spacing', RT_THEME_DOMAIN),
                    'type' => Controls_Manager::NUMBER,
                    'default' => 10,
                    'min' => 0,
                    'max' => 100,
                    'step' => 1,
                    'description' => __('spacing between Carousel item', RT_THEME_DOMAIN),
                ]
            );

        }
        if (!empty($settings['navigation'])) {
            $this->add_control(
                'slider_nav',
                [
                    'label' => __('Navigation', RT_THEME_DOMAIN),
                    'type' => Controls_Manager::SELECT,
                    'default' => 'beside',
                    'options' => array(
                        'none' => 'None',
                        'beside' => 'On Side',
                        'header' => 'On Header',
                    ),
                ]
            );

        }

        $this->add_control(
            'slider_pagination',
            [
                'label' => __('Pagination', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', RT_THEME_DOMAIN),
                'label_on' => __('On', RT_THEME_DOMAIN),
                'return_value' => 'yes',

            ]
        );

        $this->add_control(
            'slider_loop',
            [
                'label' => __('Infinite Loop', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'false',
                'label_off' => __('Off', RT_THEME_DOMAIN),
                'label_on' => __('On', RT_THEME_DOMAIN),
                'return_value' => 'true',

            ]
        );

        $this->add_control(
            'slider_auto_play',
            [
                'label' => __('Auto Play', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'false',
                'label_on' => __('On', RT_THEME_DOMAIN),
                'label_off' => __('Off', RT_THEME_DOMAIN),
                'return_value' => 'true',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'style_slider_navigation',
            [
                'label' => __('Carousel', RT_THEME_DOMAIN),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'style_slider_nav',
            [
                'label' => __('Navigation', RT_THEME_DOMAIN),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        /* start navigation color */
        $this->start_controls_tabs('slider_nav_tabs');
        $this->start_controls_tab(
            'slider_nav_normal',
            [
                'label' => __('Normal', RT_THEME_DOMAIN),
            ]
        );

        $this->add_control(
            'style_slider_navigation_color',
            [
                'label' => __('Navigation Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next i,
                         {{WRAPPER}}  .rt-slider .owl-prev i' => 'color: {{VALUE}} !important;',
                ],
            ]
        );

        $this->add_control(
            'style_slider_navigation_bg',
            [
                'label' => __('Background Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next,
                         {{WRAPPER}} .rt-slider .owl-prev' => 'background-color: {{VALUE}} !important;',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'slider_nav_hover',
            [
                'label' => __('Hover', RT_THEME_DOMAIN),
            ]
        );

        $this->add_control(
            'style_slider_navigation_color_hover',
            [
                'label' => __('Navigation Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next:hover i,
                         {{WRAPPER}}  .rt-slider .owl-prev:hover i' => 'color: {{VALUE}} !important;',
                ],
            ]
        );

        $this->add_control(
            'style_slider_navigation_bg_hover',
            [
                'label' => __('Background Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next:hover,
                         {{WRAPPER}} .rt-slider .owl-prev:hover' => 'background-color: {{VALUE}} !important;',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

        $this->add_control(
            'style_slider_navigation_radius',
            [
                'label' => __('Border Radius', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next,
                     {{WRAPPER}} .rt-slider .owl-prev' => 'border-radius: {{SIZE}}{{UNIT}}!important;',
                ],
            ]
        );

        $this->add_responsive_control(
            'style_slider_navigation_position_top',
            [
                'label' => __('Top Indent', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next,
                     {{WRAPPER}} .rt-slider .owl-prev' => 'top: {{SIZE}}{{UNIT}}!important;',
                ],
                'condition' => [
                    'slider_nav!' => 'header',
                ],
            ]
        );

        $this->add_responsive_control(
            'style_slider_navigation_position_side',
            [
                'label' => __('Side Indent', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 300,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-prev' => 'left: {{SIZE}}{{UNIT}}!important;',
                    '{{WRAPPER}} .rt-slider .owl-next' => 'right: {{SIZE}}{{UNIT}}!important;',
                ],
                'condition' => [
                    'slider_nav!' => 'header',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'style_slider_shadow',
                'selector' => '{{WRAPPER}} .rt-slider .owl-next,
                                   {{WRAPPER}} .rt-slider .owl-prev',
            ]
        );

        $this->add_control(
            'style_slider_pagination',
            [
                'label' => __('Pagination', RT_THEME_DOMAIN),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'pagination_color',
            [
                'label' => __('Pagination Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .owl-dot span' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'pagination_hover_color',
            [
                'label' => __('Pagination Active Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .owl-dot.active span' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

    }

    /**
     * BUTTON GROUP
     */
    public function setting_button($args = array())
    {
        $button_name = !empty($args['name']) ? $args['name'] : 'button';
        $button = !empty($args['class']) ? $args['class'] : '.rt-btn';
        $button_hover = !empty($args['class']) ? $args['class'] . ':hover,' . $args['class'] . ':active,' . $args['class'] . ':focus' : '.rt-btn:hover, .rt-btn:active, .rt-btn:focus';
        $label = !empty($args['label']) ? $args['label'] : 'Button';

        $this->start_controls_section(
            'style_' . $button_name,
            [
                'label' => $label,
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->start_controls_tabs($button_name . '_tabs');

        $this->start_controls_tab(
            $button_name . '_normal',
            [
                'label' => __('Normal', RT_THEME_DOMAIN),
            ]
        );
        $this->add_control(
            $button_name . '_color',
            [
                'label' => __('Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} ' . $button => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        // $this->add_group_control(
        //     Group_Control_Typography::get_type(),
        //     [
        //         'name' => $button_name . '_type',
        //         'selector' => '{{WRAPPER}} ' . $button,
        //     ]
        // );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => $button_name . '_background',
                'selector' => '{{WRAPPER}} ' . $button,
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => $button_name . '_border',
                'selector' => '{{WRAPPER}} ' . $button,

            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => $button_name . '_shadow',
                'exclude' => [
                    'box_shadow_position',
                ],
                'selector' => '{{WRAPPER}} ' . $button,
            ]
        );

        $this->end_controls_tab();
        $this->start_controls_tab(
            $button_name . '_hover',
            [
                'label' => __('Hover', RT_THEME_DOMAIN),
            ]
        );
        $this->add_control(
            $button_name . '_hover_color',
            [
                'label' => __('Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} ' . $button_hover => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => $button_name . '_hover_background',
                'selector' => '{{WRAPPER}} ' . $button_hover,
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => $button_name . '_hover_border',
                'selector' => '{{WRAPPER}} ' . $button_hover,

            ]
        );
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => $button_name . '_hover_shadow',
                'exclude' => [
                    'box_shadow_position',
                ],
                'selector' => '{{WRAPPER}} ' . $button_hover,
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

        $this->add_responsive_control(
            $button_name . '_radius',
            [
                'label' => __('Border Radius', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 3,
                    'unit' => 'px',
                ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} ' . $button => 'border-radius: {{SIZE}}{{UNIT}}!important;',
                ],
                'separator' => 'before',
            ]
        );
        $this->add_responsive_control(
            $button_name . '_padding',
            [
                'label' => __('Padding', RT_THEME_DOMAIN),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px'],
                'allowed_dimensions' => ['left', 'top', 'right', 'bottom'],
                'selectors' => [
                    '{{WRAPPER}} ' . $button => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]

        );
        $this->add_responsive_control(
            $button_name . '_margin',
            [
                'label' => __('Margin', RT_THEME_DOMAIN),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px'],
                'allowed_dimensions' => ['left', 'top', 'right', 'bottom'],
                'selectors' => [
                    '{{WRAPPER}} ' . $button => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]

        );

        $this->add_responsive_control(
            $button_name . '_full',
            [
                'label' => __('Button Width', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} ' . $button => 'width: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();

    }

    /**
     * Pagination control
     *
     * @return void
     */
    public function setting_pagination()
    {

        $this->start_controls_section(
            'pagination',
            [
                'label' => __('Pagination', RT_THEME_DOMAIN),
            ]
        );

        $this->add_control(
            'pagination_style',
            [
                'label' => __('Pagination Style', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT,
                'default' => 'no_pagination',
                'options' => array(
                    'no_pagination' => 'No Pagination',
                    'loadmore' => 'Load More',
                ),
            ]
        );

        $this->end_controls_section();

    }

    public function setting_header_block()
    {
        $this->start_controls_section(
            'header',
            [
                'label' => __('Header', RT_THEME_DOMAIN),
            ]
        );
        $this->add_control(
            'header_title',
            [
                'label' => __('Heading', RT_THEME_DOMAIN),
                'type' => Controls_Manager::TEXT,
                'default' => __('Heading title', RT_THEME_DOMAIN),
            ]
        );
        $this->add_control(
            'header_style',
            [
                'label' => __('Style', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT,
                'default' => 'style-1',
                'options' => [
                    'style-1' => __('Style 1', RT_THEME_DOMAIN),
                    'style-2' => __('Style 2', RT_THEME_DOMAIN),
                    'style-3' => __('Style 3', RT_THEME_DOMAIN),
                    'style-4' => __('Style 4', RT_THEME_DOMAIN),
                ],
            ]
        );
        $this->end_controls_section();

        /* add style header block */
        $this->start_controls_section(
            'style_header',
            [
                'label' => __('Header', RT_THEME_DOMAIN),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        // $this->add_group_control(
        //     Group_Control_Typography::get_type(),
        //     [
        //         'name' => 'style_header_typography',
        //         'selector' => '{{WRAPPER}} .rt-header-block__title',
        //     ]
        // );

        $this->add_control(
            'style_header_color',
            [
                'label' => __('Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-header-block .rt-header-block__title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'style_header_color_line_primary',
            [
                'label' => __('Color Line Primary', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-header-block--style-1 .rt-header-block__title' => 'border-color: {{VALUE}};',
                    '{{WRAPPER}} .rt-header-block--style-2 .rt-header-block__title' => 'border-color: {{VALUE}};',
                    '{{WRAPPER}} .rt-header-block--style-3 .rt-header-block__title' => 'background-color: {{VALUE}};',
                    '{{WRAPPER}} .rt-header-block--style-4 .rt-header-block__title' => 'border-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'style_header_color_line_second',
            [
                'label' => __('Color Line Second', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-header-block--style-1 ' => 'border-color: {{VALUE}};',
                    '{{WRAPPER}} .rt-header-block--style-3 ' => 'border-color: {{VALUE}};',
                    '{{WRAPPER}} .rt-header-block--style-4::before' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        /* start navb color */
        $this->start_controls_tabs('header_block_nav_tabs');
        $this->start_controls_tab(
            'header_block_normal',
            [
                'label' => __('Normal', RT_THEME_DOMAIN),
            ]
        );
        $this->add_control(
            'header_block_color',
            [
                'label' => __('Nav Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-header-block__nav a' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_control(
            'header_block_background',
            [
                'label' => __('Nav Background Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-header-block__nav a' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'header_block_border',
            [
                'label' => __('Nav Border Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-header-block__nav a' => 'border-color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'header_block_nav_hover',
            [
                'label' => __('Hover', RT_THEME_DOMAIN),
            ]
        );
        $this->add_control(
            'header_block_color_hover',
            [
                'label' => __('Nav Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-header-block__nav a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_control(
            'header_block_background_hover',
            [
                'label' => __('Nav Background Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-header-block__nav a:hover' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'header_block_border_hover',
            [
                'label' => __('Nav Border Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-header-block__nav a:hover' => 'border-color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end nav color */

        $this->add_responsive_control(
            'header_block_radius',
            [
                'label' => __('Nav Border Radius', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 80,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .rt-header-block__nav a:hover' => 'border-radius: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'header_block_top_indent',
            [
                'label' => __('Nav Top Indent', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => -45,
                        'max' => 30,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-header-block__nav' => 'top: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'header_block_margin',
            [
                'label' => __('Margin Bottom', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-header-block' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();
    }

    /**
     * HELPER elementor loop query
     */
    public static function elementor_loop($settings = array())
    {
        /* get query argument */
        $the_query = new \WP_Query(Helper::query($settings));

        // slider wrapper
        if ($settings['carousel'] == 'yes') {
            echo HTML::before_slider(array(
                'id' => 'rt-slider-' . $settings['id'],
                'class' => ['rt-slider js-elementor-slider'],
                'items-lg' => $settings['slider_item'],
                'items-md' => $settings['slider_item_tablet'],
                'items-sm' => $settings['slider_item_mobile'],
                'nav' => ($settings['slider_nav'] != 'none' && $settings['slider_nav'] != 'header') ? true : false,
                'gap' => $settings['slider_gap'],
                'pagination' => ($settings['slider_pagination'] === 'yes') ? true : false,
                'loop' => $settings['slider_loop'],
                'autoplay' => $settings['slider_auto_play'],
            ));

        }

        // Header
        echo HTML::header_block(array(
            'class' => 'rt-header-block--' . $settings['header_style'],
            'title' => $settings['header_title'],
            'nav' => ($settings['slider_nav'] == 'header') ? true : false,
            'id' => 'header-' . $settings['id'],
        ));

        /* class wrapper */
        $classes[] = !empty($settings['class_wrapper']) ? $settings['class_wrapper'] : '';

        /* Start LOOP */
        if ($the_query->have_posts()):

            if ($settings['carousel'] == 'yes') {

                $classes[] = 'rt-slider__main owl-carousel';

                echo HTML::open(['class' => $classes]);

            } else {

                // class wrapper
                $classes[] = 'flex-loop flex flex-row';
                $classes[] = 'flex-cols-md-' . 12 / $settings['setting_column'];
                $classes[] = 'flex-cols-sm-' . 12 / $settings['setting_column_tablet'];
                $classes[] = 'flex-cols-xs-' . 12 / $settings['setting_column_mobile'];

                // masonry
                $classes[] = ($settings['layout_masonry'] == 'yes') ? 'js-elementor-masonry' : '';

                echo HTML::open(array(
                    'id' => 'block_' . $settings['id'],
                    'class' => $classes,
                    'page' => 1,
                    'max-paged' => $the_query->max_num_pages,
                ));

            }

            while ($the_query->have_posts()): $the_query->the_post();

                include locate_template($settings['template_part'] . '.php');

            endwhile;

            if ($settings['carousel'] == 'yes') {
                echo HTML::close();
            } else {
                echo HTML::close();

                /* add pagination */
                if ($the_query->max_num_pages > 1) {

                    echo HTML::pagination(array(
                        'id' => 'block_' . $settings['id'],
                        'style' => $settings['pagination_style'])
                    );

                    echo HTML::script('var block_' . $settings['id'] . ' = ' . \json_encode(array_merge($settings, ['template_part' => $settings['template_part']])));
                }

            }
            wp_reset_postdata();
        else:
            do_action('rt_post_none');
        endif;

        // slider close
        if ($settings['carousel'] == 'yes') {
            echo HTML::after_slider();
        }
    }

    
     public function element_before_loop($settings = array(), $widget_id = ''){
         // slider wrapper
        if ($settings['carousel'] == 'yes') {

            echo HTML::before_slider(array(
                'id' => 'astro-slider-'.$widget_id,
                'class' => ['rt-slider js-astro-slider'],
                'items-lg' => $settings['slider_item'],
                'items-md' => $settings['slider_item_tablet'],
                'items-sm' => $settings['slider_item_mobile'],
                'pagination' => $settings['slider_pagination'],
                'gap' => $settings['slider_gap'],
                'nav' => ($settings['slider_nav'] != 'none' && $settings['slider_nav'] != 'header') ? true : false,
                'loop' => $settings['slider_loop'],
                'autoplay' => $settings['slider_auto_play'],
            ));

        }
        

        // Header
        echo HTML::header_block(array(
            'class' => 'rt-header-block--' . $settings['header_style'],
            'title' => $settings['header_title'],
            'nav' => ($settings['slider_nav'] == 'header') ? true : false,
            'id' => 'header-' . $widget_id,
        ));

        if ($settings['carousel'] == 'yes') {
            echo HTML::open(['class' => array('rt-slider__main owl-carousel')]);
        } else {
            // class wrapper
            $classes[] = 'flex-loop flex flex-row';
            $classes[] = 'flex-cols-md-' . 12 / $settings['setting_column'];
            $classes[] = 'flex-cols-sm-' . 12 / $settings['setting_column_tablet'];
            $classes[] = 'flex-cols-xs-' . 12 / $settings['setting_column_mobile'];

            // masonry
            $classes[] = ($settings['layout_masonry'] == 'yes') ? 'js-astro-masonry' : '';

            echo HTML::open(array(
                'id' => 'block_' . $widget_id,
                'class' => $classes,
            ));

        }
    }

    public function element_after_loop($settings = array()){
               // slider close
        if ($settings['carousel'] == 'yes') {
            echo HTML::close(); //close slider main
            echo HTML::after_slider();
        } else {
            echo HTML::close();
        }
    }
    /* end class */
}
