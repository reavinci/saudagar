jQuery(document).ready(function ($) {
  /*=================================================;
    /* COUNTDOWN
    /*================================================= */
  var ajaxCountdown = function () {
    var date = new Date();
    var now_date =
      date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();

    var element = $(".js-countdown");

    element.each(function (index) {
      // get id each element countdown
      var countdown = $("#" + $(this).attr("id"));
      var get_date = countdown.getData("date");

      countdown
        .countdown(countdown.getData("date"), function (event) {
          $(this).find(".js-countdown-day").html(event.strftime("%D"));

          $(this).find(".js-countdown-hour").html(event.strftime("%H"));

          $(this).find(".js-countdown-minute").html(event.strftime("%M"));

          $(this).find(".js-countdown-second").html(event.strftime("%S"));
        })
        .addClass("is-loaded");

      var now_date_format = new Date(now_date);
      var get_date_format = new Date(get_date);
      // remove date if sale end
      if (now_date_format > get_date_format) {
        countdown.remove();
      }
    });
  };

  /*=================================================
*  AJAX CALL
// =================================================== */
  var button = $(".js-loop-load");

  button.on("click", function (event) {
    event.preventDefault();

    var block_id = $(this).data("triger-id");
    var element = $("#" + block_id);
    var settings = eval($(this).data("triger-id"));
    var current_page = element.attr("data-page");
    var max_num_pages = element.attr("data-max-paged");
    var trigger = $('[data-triger-id="' + block_id + '"]');
    var spinner = $(".js-" + block_id + "-spinner");

    if ($(this).hasClass("prev")) {
      var page_action = parseInt(current_page) - 1;
    } else {
      var page_action = parseInt(current_page) + 1;
    }

    jQuery.ajax({
      url: ajax_loop.ajaxurl,
      type: "post",
      dataType: "json",
      data: {
        action: "ajax_loop_result",
        page: page_action,
        query: settings,
        check_nonce: ajax_loop.check_nonce,
      },
      beforeSend: function (response) {
        trigger.hide();
        spinner.show();
        element.find(".flex-item").addClass("is-show js-page-1");
        element.addClass("rt-loading");
      },
      success: function (response) {
        element.removeClass("rt-loading");

        var next_page = parseInt(current_page) + 1;
        var prev_page = parseInt(current_page) - 1;

        if (response != 0) {
          // check layout masonry
          if (
            element.hasClass("js-masonry") ||
            element.hasClass("js-elementor-masonry")
          ) {
            var content = $(response);

            // add new content
            var $grid = element.append(content).masonry("appended", content);

            // layout Masonry after each image loads
            $grid.imagesLoaded().progress(function () {
              $grid.masonry("layout").masonry({ horizontalOrder: true });
            });
          } else {
            element.append(response);
          }

          // add new current page
          if ($(this).hasClass("prev")) {
            element.attr("data-page", prev_page);
          } else {
            element.attr("data-page", next_page);
          }

          // add animation
          element
            .find(".flex-item:not(.is-show)")
            .velocity("transition.expandIn", {
              duration: 300,
            })
            .addClass("is-show js-page-" + next_page);

          setTimeout(function () {
            trigger.show();
            spinner.hide();
          }, 500);

          // remove button if last page
          if (next_page >= max_num_pages) {
            trigger.remove();
          }

          // add countdown js
          if (element.find(".js-countdown")) {
            ajaxCountdown();
          }
        } else {
          console.log("failed");
        }
      },
    });
  });
});
