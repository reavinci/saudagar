<?php
/*=================================================
 * CART - SIDE PANEL
/*=================================================
 * added sidepannel on global footer
 */
function rt_woocommerce_cart_side_panel()
{
    rt_get_template_part('shop/panel-cart');
}
add_action('rt_footer', 'rt_woocommerce_cart_side_panel');

/*=================================================
 *  DISABLE ADD TO CART
/*================================================= */
function rt_woocommerce_remove_add_to_cart()
{
    if (rt_option('woocommerce_catalog', false)) {
        remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
    }

}
add_action('init', 'rt_woocommerce_remove_add_to_cart');

/*=================================================
 *  AJAX CART
/*=================================================
 * @desc add script ajax woocomerce single page */

function rt_woocommerce_ajax_cart_scripts()
{
    if (rt_option('woocommerce_ajax_mini_cart', true) && rt_is_premium()) {
        wp_enqueue_script('retheme_cart_ajax', get_template_directory_uri() . '/assets/js/add-to-cart.min.js', array('jquery'), '1.1.0', true);

        wp_localize_script('retheme_cart_ajax', 'rt_product_object',
            array(
                'ajaxurl' => admin_url('admin-ajax.php'),
                'check_nonce' => wp_create_nonce('retheme-cart-nonce'),
            )
        );
    }

}
add_action('wp_enqueue_scripts', 'rt_woocommerce_ajax_cart_scripts');

/*=================================================;
/* CART - ADDED SINGLE
/*================================================= */
function rt_woocommerce_add_cart_single_ajax()
{
    if (rt_option('woocommerce_ajax_mini_cart', true)) {
        $product_id = $_POST['product_id'];
        $variation_id = $_POST['variation_id'];
        $quantity = $_POST['quantity'];

        if (check_ajax_referer('retheme-cart-nonce', 'check_nonce')) {
            if ($variation_id) {
                WC()->cart->add_to_cart($product_id, $quantity, $variation_id);
            } else {
                WC()->cart->add_to_cart($product_id, $quantity);
            }

        }

        woocommerce_mini_cart();

        wp_die();

    }
}

add_action('wp_ajax_retheme_add_cart_single', 'rt_woocommerce_add_cart_single_ajax');
add_action('wp_ajax_nopriv_retheme_add_cart_single', 'rt_woocommerce_add_cart_single_ajax');

/*=================================================;
/* CART - UPDATE
/*================================================= */
function rt_cart_update()
{
    $product_id = $_POST['product_id'];
    $quantity = $_POST['quantity'];
    $cart_item_key = $_POST['cart_item_key'];

    if ($cart_item_key && check_ajax_referer('retheme-cart-nonce', 'check_nonce')) {
        WC()->cart->set_quantity($cart_item_key, $quantity);
    }

    wp_die();

}

add_action('wp_ajax_retheme_update_cart_result', 'rt_cart_update');
add_action('wp_ajax_nopriv_retheme_update_cart_result', 'rt_cart_update');

/*=================================================;
/* CART - DELETE RESULT
/*================================================= */
function rt_cart_remove()
{
    $product_id = $_POST['product_id'];
    $item_key = $_POST['item_key'];
    $cart_item_key = $_POST['cart_item_key'];

    if ($cart_item_key && check_ajax_referer('retheme-cart-nonce', 'check_nonce')) {
        WC()->cart->remove_cart_item($cart_item_key);
    }

    wp_die();

}
add_action('wp_ajax_retheme_delete_cart_result', 'rt_cart_remove');
add_action('wp_ajax_nopriv_retheme_delete_cart_result', 'rt_cart_remove');
