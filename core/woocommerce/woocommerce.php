<?php
/*=================================================;
/* SCRIPTS WOOCOMMERCE
/*================================================= */
function rt_woocommerce_scripts()
{
    wp_enqueue_script('retheme-variation', get_template_directory_uri() . '/assets/js/woocommerce/variation.min.js', array('jquery'), '1.3.3', true);
    wp_enqueue_script('retheme-woocommerce', get_template_directory_uri() . '/assets/js/woocommerce.min.js', array('jquery', 'retheme-variation'), '1.0.0', true);
    wp_enqueue_style('retheme-woocommerce', get_template_directory_uri() . '/assets/css/retheme-woo.min.css', '1.3.0');

}
add_action('wp_enqueue_scripts', 'rt_woocommerce_scripts', 10);

/*=================================================
/* GET ARCHIVE ALL PRODUCT
/*=================================================
 * @desc This function detect all archive on post type product like category, tag, archive default
 * @return boolean
 */
function rt_is_product_archive()
{
    if (is_tax(array('product_cat', 'product_tag', 'product_vendor')) || rt_is_woocommerce('shop')) {
        return true;
    }
}

/*=================================================
 *  WOO COMMERCE SUPPORT
/*================================================= */
function rt_woocommerce_support()
{
    add_theme_support('woocommerce');
    add_theme_support('wc-product-gallery-zoom');
    add_theme_support('wc-product-gallery-lightbox');
    add_theme_support('wc-product-gallery-slider');
}
add_action('after_setup_theme', 'rt_woocommerce_support');


/*=================================================;
/* IMAGE SIZE
/*=================================================
 * @desc Use this filter image if woocommerce archive active */

function rt_woocommerce_image_size($size)
{
    if (rt_option('woocommerce_archive_masonry', false)) {
        $size = array(
            'width' => 300,
            'height' => 999999999,
            'crop' => 1,
        );
    }

    return $size;

}
add_filter('wc_get_image_size', 'rt_woocommerce_image_size');

/*=================================================
 *  COUNT WOO
/*================================================= */
/** Show cart count and number if ajax add to cart on click */
function rt_woocommerce_header_add_to_cart_fragment($fragments)
{
    global $woocommerce;

    ob_start();

    echo '<span class="rt-header__cart-count js-cart-total">' . $woocommerce->cart->cart_contents_count . '</span>';

    $fragments['.js-cart-total'] = ob_get_clean();

    return $fragments;

}
add_filter('woocommerce_add_to_cart_fragments', 'rt_woocommerce_header_add_to_cart_fragment');

/*=================================================;
/* QTY FIELD
/*================================================= */
function rt_woocommerce_add_before_quantity()
{
    echo '<div class="rt-qty quantity_change">';
    echo '<span class="rt-qty__min minus">-</span>';
}

function rt_woocommerce_add_after_quantity()
{
    echo '<span class="rt-qty__max plus">+</span>';
    echo '</div>';
}
add_action('woocommerce_before_quantity_input_field', 'rt_woocommerce_add_before_quantity');
add_action('woocommerce_after_quantity_input_field', 'rt_woocommerce_add_after_quantity');


/*=================================================;
/* GLOBALS - REMOVE DEFAULT STYLE
/*================================================= */
function rt_woocommerce_dequeue_styles($enqueue_styles)
{
    unset($enqueue_styles['woocommerce-general']); // Remove the gloss
    return $enqueue_styles;
}
add_filter('woocommerce_enqueue_styles', 'rt_woocommerce_dequeue_styles');

/*=================================================;
/* WOOCOMMERCE - SET TEXT DOMAIN FILE
/*================================================= */
/* Replace 'textdomain' with your plugin's textdomain. e.g. 'woocommerce'.
 * File to be named, for example, yourtranslationfile-en_GB.mo
 * File to be placed, for example, wp-content/lanaguages/textdomain/yourtranslationfile-en_GB.mo
 */
function rt_woocommerce_translation_file($mofile, $domain)
{
    if ('woocommerce' === $domain && !function_exists('loco_plugin_file')) {
        $mofile = get_template_directory_uri() . '/languages/woocommerce-' . get_locale() . '.mo';
    }
    return $mofile;
}
add_filter('load_textdomain_mofile', 'rt_woocommerce_translation_file', 10, 2);

/*=================================================;
/* INCLUDE FILE
/*================================================= */
function rt_woocommerce_init()
{
    include_once dirname(__FILE__) . '/shop.php';
    include_once dirname(__FILE__) . '/product.php';
    include_once dirname(__FILE__) . '/cart.php';
    include_once dirname(__FILE__) . '/checkout.php';
    include_once dirname(__FILE__) . '/user.php';
}
add_action('woocommerce_init', 'rt_woocommerce_init');