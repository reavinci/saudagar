<?php 
/*=================================================
 * PRICE CLASS
=================================================== */
function rt_product_price_class($classes)
{

    $classes = "product_price {$classes}";

    return $classes;
}
add_filter('woocommerce_product_price_class', 'rt_product_price_class');

/*=================================================
 *   REMOVE TITLE SHOP
/*================================================= */
/** remove title shop on header page */
function rt_woocommerce_hide_page_title()
{
    return false;
}
add_filter('woocommerce_show_page_title', 'rt_woocommerce_hide_page_title');

/*=================================================;
/* PRODUCT - REMOVE HEADER
/*=================================================*/
// remove global header
// option product minimalis
function rt_product_page_header($args)
{
    if (rt_option('woocommerce_single_layout', 'buttom-gallery') == 'minimalis' && rt_is_woocommerce('product')) {
        return false;
    }
    return $args;
}
add_filter('page_header', 'rt_product_page_header');

/*=================================================;
/* PRODUCT - MOVE BREADCRUMBS
/*================================================= */
// move breadcrumbs before title
function rt_product_breadcrumbs()
{
    if (rt_option('woocommerce_single_layout', 'buttom-gallery') == 'minimalis') {
        rt_breadcrumb();
    }
}
add_action('woocommerce_single_product_summary', 'rt_product_breadcrumbs', 1);


/*=================================================
 * SHARE
/*================================================= */
/** add share button woocommerce single */
function rt_woocommerce_share()
{
    if (rt_option('woocommerce_single_share', true)) {
        rt_get_template_part('global/share');
    }

}

add_action('woocommerce_share', 'rt_woocommerce_share');

/*=================================================;
/* PRODUCT REVIEW FIELD
/*================================================= */
function rt_product_review_comment_form($review_form)
{
    $commenter = wp_get_current_commenter();

    $review_form = array(
        'fields' => array(
            'author' => '<div class="comment-wrapper-personal"><p class="comment-form-author rt-form ">' . '<label class="rt-form__label" for="author">' . esc_html__('Name', 'woocommerce') . '&nbsp;<span class="required">*</span></label> ' .
            '<input id="author" class="rt-form__input" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30" required /></p>',
            'email' => '<p class="comment-form-email rt-form "><label class="rt-form__label" for="email">' . esc_html__('Email', 'woocommerce') . '&nbsp;<span class="required">*</span></label> ' .
            '<input id="email" class="rt-form__input" name="email" type="email" value="' . esc_attr($commenter['comment_author_email']) . '" size="30" required /></p></div>',
        ),

    );

    $review_form['comment_field'] = '';

    if (wc_review_ratings_enabled()) {
        $review_form['comment_field'] = '<div class="comment-form-rating"><label for="rating">' . esc_html__('Your rating', 'woocommerce') . '</label><select name="rating" id="rating" required>
						<option value="">' . esc_html__('Rate&hellip;', 'woocommerce') . '</option>
						<option value="5">' . esc_html__('Perfect', 'woocommerce') . '</option>
						<option value="4">' . esc_html__('Good', 'woocommerce') . '</option>
						<option value="3">' . esc_html__('Average', 'woocommerce') . '</option>
						<option value="2">' . esc_html__('Not that bad', 'woocommerce') . '</option>
						<option value="1">' . esc_html__('Very poor', 'woocommerce') . '</option>
					</select></div>';
    }

    $review_form['comment_field'] .= '<p class="comment-form-comment rt-form ""><label class="rt-form__label" for="comment">' . esc_html__('Your review', 'woocommerce') . '&nbsp;<span class="required">*</span></label><textarea id="comment" class="rt-form__input" name="comment" cols="45" rows="8" required></textarea></p>';

    return $review_form;

}
add_filter('woocommerce_product_review_comment_form_args', 'rt_product_review_comment_form');

/*=================================================;
/* PRODUCT - REMOVE TITLE PAGE
/*================================================= */
function rt_product_remove_page_title($args)
{
    if (rt_is_woocommerce('product')) {
        return false;
    }
    return $args;

}
add_filter('rt_page_title', 'rt_product_remove_page_title');

/*=================================================;
/* PRODUCT - REMOVE TITLE TAB
/*=================================================
 * remove title header from tab heading
 */
function rt_product_tabs_heading()
{
    return false;
}
add_action('woocommerce_product_description_heading', 'rt_product_tabs_heading');
add_action('woocommerce_product_additional_information_heading', 'rt_product_tabs_heading');

/*=================================================;
/* PRODUCT -  LAYOUT
/*================================================= */
function rt_product_layout($classes)
{

    if (rt_is_premium()) {

        if (rt_is_woocommerce('product')) {
            $classes[] = 'product-template--' . rt_option('woocommerce_single_layout', 'left-gallery');

        }
    }

    // hidden add to cart button with css
    if (rt_option('woocommerce_catalog', false)) {
        $classes[] = 'catalog-mode';
    }

    return $classes;
}

add_filter('body_class', 'rt_product_layout');

/*=================================================;
/* MOVE CROSS SELLING
/*================================================= */
remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
add_action('woocommerce_after_cart_table', 'woocommerce_cross_sell_display');

/*=================================================;
/* PRODUCT - REMOVE META
/*================================================= */
function rt_product_remove_meta()
{
    if (rt_option('woocommerce_single_meta', true) == false) {
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
    }

}
add_action('after_setup_theme', 'rt_product_remove_meta');
