<?php

/*=================================================;
/* SHOP - ADD CLASS WRAPPER LOOP
/*================================================= */

function rt_woocommerce_class_wrapper($classes)
{
    $classes[] = 'rt-product-archive products flex flex-row flex-loop';

    /* Column */
    $column = 12 / rt_option('woocommerce_archive_options_column', 3);
    $column_md = 12 / rt_option('woocommerce_archive_options_column_tablet', 2);
    $column_sm = 12 / rt_option('woocommerce_archive_options_column_mobile', 2);

    $classes[] = 'flex-cols-md-' . $column;
    $classes[] = 'flex-cols-sm-' . $column_md;
    $classes[] = 'flex-cols-xs-' . $column_sm;

    if (rt_option('woocommerce_archive_masonry', false)) {
        $classes[] = 'js-masonry';
    }

    return $classes;

}
add_filter('rt_product_class_wrapper', 'rt_woocommerce_class_wrapper');

/*=================================================
 *  SHOP -  CHANGE LOOP PERPAGE
/*================================================= */
function rt_woocommerce_per_page($loop)
{
    $post_per_page = rt_option('woocommerce_archive_options_post_per_page', 9);

    if (rt_is_only_tablet()) {
        $post_per_page = rt_option('woocommerce_archive_options_post_per_page_tablet', 8);
    }

    if (rt_is_only_mobile()) {
        $post_per_page = rt_option('woocommerce_archive_options_post_per_page_mobile', 8);
    }
    return $post_per_page;
};

add_filter('loop_shop_per_page', 'rt_woocommerce_per_page', 10, 1);

/*=================================================;
/* SHOP -  AJAX QUERY
/*================================================= */
function rt_woocommerce_query_loop($loop)
{
    $post_per_page = (rt_is_mobile()) ? rt_option('woocommerce_archive_options_post_per_page_tablet', 8) : rt_option('woocommerce_archive_options_post_per_page', 9);

    if (rt_is_product_archive()) {
        $loop = array(
            'post_type' => 'product',
            'posts_per_page' => $post_per_page,
            'template_part' => 'woocommerce/content-product',
            'pagination' => rt_option('woocommerce_archive_options_pagination', 'number'),
        );

    }
    return $loop;
};

add_filter('rt_loop_query', 'rt_woocommerce_query_loop');

/*=================================================
 * SHOP - REMOVE WOO PAGINATION
/*================================================= */
remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);

/*=================================================;
/* SHOP - REMOVE WRAPPER GENERAL POST
/*=================================================
 * @desc Remove theme DOM wraper loop <div id="post_archive"></div>
 */
function rt_woocommerce_wrapper_post()
{
    if (rt_is_product_archive()) {
        remove_action('rt_before_loop', 'rt_before_loop', 5);
        remove_action('rt_after_loop', 'rt_after_loop', 5);
    }

}

add_action('wp', 'rt_woocommerce_wrapper_post');

/*=================================================;
/* SHOP - FILTER
/*=================================================
 * wrap woocommerce before loop element
 * @see woocommerce archive hook
 */
function rt_shop_result_count()
{
    echo '<div class="rt-shop-filter-wrapper">';
}
add_action('woocommerce_before_shop_loop', 'rt_shop_result_count', 19);

function rt_shop_catalog_ordering()
{
    echo '</div>';
}
add_action('woocommerce_before_shop_loop', 'rt_shop_catalog_ordering', 31);

/*=================================================
 * SHOP - HOOK ARCHIVE
/*=================================================
 * remove add to cart button on archive product
 */
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

/*=================================================;
/* SHOP - REMOVE DESCRIPTION
/*================================================= */
remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10);
remove_action('woocommerce_archive_description', 'woocommerce_product_archive_description', 10);

/*=================================================;
/* SHOP - ADD TO CART BUTTON ON ARCHIVE
/*================================================= */
function rt_add_to_cart_loop_button()
{
    // Yith Label
    $label = get_option('yith-wcqv-button-label');
    $label = call_user_func('__', $label, 'yith-woocommerce-quick-view');
    $label = apply_filters('yith_wcqv_button_label', esc_html($label));

    if (rt_option('woocommerce_catalog', false) == false) {
        // compatible with yith quick view
        if (!class_exists('YITH_WCQV')) {
            woocommerce_template_loop_add_to_cart();
        } else {
            echo '<a href="#" class="button yith-wcqv-button rt-product__btn" data-product_id="' . get_the_ID() . '">' . $label . '</a>';
        }
    }
}
add_action('woocommerce_before_shop_loop_item_title', 'rt_add_to_cart_loop_button', 10);
add_action('woocommerce_after_shop_loop_item_title', 'rt_add_to_cart_loop_button', 20);

/*=================================================;
/* SHOP - REMOVE PREFIX PAGE TITLE
/*================================================= */
function rt_shop_remove_prefix_title($title)
{
    if (rt_is_woocommerce('category')) {
        $title = single_cat_title('', false);
    } elseif (rt_is_woocommerce('tag')) {
        $title = single_cat_title('', false);
    }
    return $title;

}
add_filter('get_the_archive_title', 'rt_shop_remove_prefix_title');

/*=================================================;
/* SHOP - PRODUCT STYLE
/*================================================= */
function rt_woocommerce_product_style($style)
{
    $class = 'rt-product rt-product--overlay-btn';

    if ($style == 'style-2') {
        $class = 'rt-product rt-product--button';
    }

    return $class;
}
