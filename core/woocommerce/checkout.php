<?php
/*=================================================;
/* FIELD - ADD CLASS
/*=================================================*/
// add class all field woocomemrce
// reoreder and conditional checkout field
function rt_checkout_form_field_args($args, $key, $value = null)
{
    $class = 'rt-form';

    if (rt_is_woocommerce('checkout') && ($args['type'] !== 'checkbox' || $args['type'] !== 'radio')) {

        // add class field overlay on checkout page
        $args['class'][] = 'rt-form rt-form--overlay js-form-overlay';

        if ($args['type'] == 'select') {
            $args['class'][] = 'rt-form--select2';
        }

        // global field class
        $args['class'][] = $class;
        $args['label_class'][] = 'rt-form__label';
        $args['input_class'][] = 'rt-form__input';
    }

    $fields = array(
        'first_name' => 10,
        'last_name' => 20,
        'email' => 30,
        'phone' => 35,
        'district' => 90,
        'company' => 120,
    );

    foreach ($fields as $field => $value) {
        $setting = rt_option("woocommerce_checkout_field_{$field}");

        if ($args['id'] == "billing_{$field}" || $args['id'] == "shipping_{$field}") {

            if ($setting == 'optional') {
                $args['required'] = false;
            }

            if ($setting == 'hidden') {
                $args['class'][] = 'is-hidden';
                $args['required'] = false;
            }

            $args['priority'] = $value;

        }

        if ($args['type'] == 'email') {
            $args['placeholder'] = __('john.doe@email.com', RT_THEME_DOMAIN);
        }

    }

    return $args;

}
add_filter('woocommerce_form_field_args', 'rt_checkout_form_field_args', 10, 3);

/*=================================================;
/* FIELD - BILLING
/*=================================================
 * Make email optional / hidden
 */
function rt_checkout_billing_fields($fields)
{
    if (rt_option("woocommerce_checkout_field_email") == 'optional') {
        $fields["billing_email"]['required'] = false;
    }

    if (rt_option("woocommerce_checkout_field_email") == 'hidden') {
        $fields["billing_email"]['required'] = false;
        $fields["billing_email"]['class'][] = 'is-hidden';
    }

    return $fields;
}
add_filter('woocommerce_billing_fields', 'rt_checkout_billing_fields');

/*=================================================;
/* FILED - ADDRESS
/*================================================= */
function rt_checkout_default_address_fields($fields)
{

    $addresses = array(
        'country' => 70,
        'state' => 80,
        'city' => 85,
        'address_1' => 95,
        'address_2' => 100,
        'company' => 120,
        'postcode' => 130,
    );

    foreach ($addresses as $field => $value) {
        $setting = rt_option("woocommerce_checkout_field_{$field}");
        $setting_woocommerce = get_option("woocommerce_checkout_{$field}_field");

        // Optional
        if ($setting == 'optional') {
            $fields[$field]['required'] = false;

        }
        // Hidden
        if ($setting == 'hidden' || $setting_woocommerce == 'hidden') {
            $fields[$field]['required'] = false;
            $fields[$field]['class'][] = 'is-hidden';
        }

        // Priority
        $fields[$field]['priority'] = $value;

    }

    // Full name
    // replace first name to fullname
    // remove last name
    if (rt_option('woocommerce_checkout_field_name', 'fullname') == 'fullname' && rt_is_woocommerce('checkout')) {
        $fields['first_name']['label'] = __('Full Name', RT_THEME_DOMAIN);
        $fields['first_name']['class'][] = 'rt-form-fullname';
        $fields['first_name']['placeholder'] = __('John Doe', RT_THEME_DOMAIN);
        $fields['last_name']['class'][] = 'is-hidden';
    }

    // Make last name opsional
    $fields['last_name']['required'] = false;

    // Country
    // hidden country
    if (rt_option('woocommerce_checkout_country', 'hidden') == 'hidden') {
        $fields['country']['class'][] = 'is-hidden';
    }

    return $fields;

}
add_filter('woocommerce_default_address_fields', 'rt_checkout_default_address_fields');


/*=================================================;
/* FIELD - REMOVE NOTES
/*================================================= */
// remove note filed checkout
function rt_checkout_remove_notes()
{
    if (rt_option('woocommerce_checkout_field_notes', 'optional') == 'hidden') {
        add_filter('woocommerce_enable_order_notes_field', '__return_false');
    }

}
add_action('init', 'rt_checkout_remove_notes');


/*=================================================;
/* CHECKOUT - MOVE COUPON
/*================================================= */
// add link to open checkout on review order
function rt_checkout_modal_coupon()
{
    if (get_option('woocommerce_enable_coupons') === 'yes') {
        echo '<div class="rt-coupon-modal-action coupon_checkout_message">';
        echo apply_filters('woocommerce_checkout_coupon_message', esc_html__('Have a coupon?', 'woocommerce') . ' <a href="#" class="js-coupon-trigger">' . esc_html__('Click here to enter your code', 'woocommerce') . '</a>');
        echo '</div>';
    }

}
add_action('woocommerce_review_order_before_payment', 'rt_checkout_modal_coupon');

/*=================================================;
/* CHECKOUT - FULLNAME SAVE
/*================================================= */
// save full name to default woocommerce data
// move full name to first dan last name  if thank page on load
function rt_checkout_split_fullname($order_id)
{
    if (rt_option('woocommerce_checkout_field_name', 'fullname') == 'fullname') {
        $order = wc_get_order($order_id);

        $billing_first_name = get_post_meta($order_id, '_billing_first_name', true);
        $shipping_first_name = get_post_meta($order_id, '_shipping_first_name', true);

        $billing_full_name = explode(' ', $billing_first_name);
        $shipping_full_name = explode(' ', $shipping_first_name);

        // save first name
        update_post_meta($order_id, '_billing_first_name', $billing_full_name[0]);
        update_post_meta($order_id, '_shipping_first_name', $shipping_full_name[0]);

        // remove first array
        array_shift($billing_full_name);
        array_shift($shipping_full_name);

        // save last name
        update_post_meta($order_id, '_billing_last_name', implode(" ", $billing_full_name));
        update_post_meta($order_id, '_shipping_last_name', implode(" ", $shipping_full_name));
    }
}
add_action('woocommerce_thankyou', 'rt_checkout_split_fullname');
