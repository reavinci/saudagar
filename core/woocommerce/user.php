<?php 
/*=================================================;
/* USER - MODAL LOGIN
/*================================================= */
function rt_woocommerce_user_modal()
{
    rt_get_template_part('shop/form-login-modal');

}
add_action('rt_footer', 'rt_woocommerce_user_modal');


/*=================================================;
/* SHOP - OPEN MODAL IF FAILED LOGIN WOOCOMMERCE
/*================================================= */
function rt_user_woocommerce_login_failed()
{
    $referrer = $_SERVER['HTTP_REFERER'];
    $my_account_page = wc_get_page_permalink('myaccount');
    $checkout_page = wc_get_page_permalink('checkout');

    if (!empty($referrer) &&
        !strstr($referrer, 'wp-login') &&
        !strstr($referrer, 'wp-admin') &&
        !strstr($referrer, $my_account_page) &&
        !strstr($referrer, $checkout_page)) {
        wp_redirect(wc_get_page_permalink('myaccount') . '?login="failed"');
        exit;
    }
}
add_action('woocommerce_login_failed', 'rt_user_woocommerce_login_failed');

/*=================================================;
/* WOOCOMMERCE - SET MESSAGE FAILED LOGIN ON MODAL LOGIN
/*================================================= */
/* Send message error if user fail login from modal */
function rt_user_woocommerce_login_failed_message()
{
    if (!empty($_GET['login']) && $_GET['login'] == 'failed' && !is_user_logged_in()) {
        wc_add_notice(__('Login failed, please check username & password then try again', RT_THEME_DOMAIN), 'error');
    }

}
add_action('wp', 'rt_user_woocommerce_login_failed_message');
