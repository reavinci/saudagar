<?php
/*=================================================;
/* REMOVE DEFAULT WIDGETS
/*================================================= */
/**
 * Add your widget area to unset the default widgets from.
 * If your theme's first widget area is "sidebar-1", you don't need this.
 *
 * @see https://stackoverflow.com/questions/11757461/how-to-populate-widgets-on-sidebar-on-theme-activation
 *
 * @param  array $widget_areas Arguments for the sidebars_widgets widget areas.
 * @return array of arguments to update the sidebars_widgets option.
 */
function rt_impot_merlin_unset_default_widgets_args($widget_areas)
{

    $widget_areas = array(
        'retheme_sidebar' => array(),
        'retheme_footer_1' => array(),
    );

    return $widget_areas;
}
add_filter('merlin_unset_default_widgets_args', 'rt_impot_merlin_unset_default_widgets_args');


/*=================================================;
/* SIGN HOMEPAGE AND DEFAULT MENU
/*================================================= */
function rt_after_import_setup()
{
    // Assign menus to their locations.
    $main_menu = get_term_by('name', 'Main Menu', 'nav_menu');
    $second_menu = get_term_by('name', 'Second Menu', 'nav_menu');
    $thirty_menu = get_term_by('name', 'Thirty Menu', 'nav_menu');
    $topbar_menu = get_term_by('name', 'Top Bar Menu', 'nav_menu');
    $footer_menu = get_term_by('name', 'Footer Menu', 'nav_menu');

    set_theme_mod('nav_menu_locations', array(
        'primary' => $main_menu->term_id,
        'second' => $second_menu->term_id,
        'thirty' => $thirty_menu->term_id,
        'topbar' => $topbar_menu->term_id,
        'footer' => $footer_menu->term_id,
    )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title('Home');
    $blog_page_id = get_page_by_title('Blog');

    update_option('show_on_front', 'page');
    update_option('page_on_front', $front_page_id->ID);
    update_option('page_for_posts', $blog_page_id->ID);

}
// add_action('pt-ocdi/after_import', 'rt_after_import_setup');
add_action('merlin_after_all_import', 'rt_after_import_setup');

/*=================================================;
/* SET DEFAULT WOOCOMMERCE PAGE AFTER IMPORT
/*================================================= */
function rt_import_set_woocomerce_page()
{
    update_option('woocommerce_shop_page_id', get_page_by_path('produk')->ID);
    update_option('woocommerce_cart_page_id', get_page_by_path('keranjang-belanja')->ID);
    update_option('woocommerce_checkout_page_id', get_page_by_path('checkout')->ID);
    update_option('woocommerce_myaccount_page_id', get_page_by_path('akun')->ID);

    $settings = array(
        'woocommerce_specific_allowed_countries' => array('ID'),
        'woocommerce_specific_ship_to_countries' => array('ID'),
        'woocommerce_currency' => 'IDR',
        'woocommerce_currency_pos' => 'left',
    );

    foreach ($settings as $key => $setting) {
        if (empty(get_option($key))) {
            update_option($key, $setting);
        }
    }

}
add_action('merlin_after_all_import', 'rt_import_set_woocomerce_page');

/*=================================================;
/* DISABLE WOOCOMMERCE WIZARD
/*================================================= */
function rt_disable_woocommerce_wizard()
{
    $page = !empty($_GET['page'])?$_GET['page']:'';
    
    if ($page == 'merlin') {
        return false;
    }

}
add_filter('woocommerce_enable_setup_wizard', 'rt_disable_woocommerce_wizard');

/*=================================================;
/* CHOOSE IMPORT
/*================================================= */
/**
 * Check select import name condisional
 */
function rt_extra_function_after_import($selected_import_index)
{
    // Grab the selected import file.
    $selected_import_file = $GLOBALS['wizard']->import_files[$selected_import_index];
    /* $GLOBALS['wizard'] is the default Merlin class instance defined in "merlin-config.php",
    you could call it something else if you'd like. */

    // You may access the currently selected import file like so:
    if ('Market' === $selected_import_file['import_file_name']) {
        set_theme_mod('theme_style', 'default');
    }

}
// add_action('merlin_after_all_import', 'rt_extra_function_after_import');
