<?php
/*=================================================
 * PAGE - TITLE
=================================================== */
function rt_get_page_title()
{

    $title = get_the_title();

    if (is_archive()) {
        $title = get_the_archive_title();
        $desc = get_the_archive_description();
    }

    if (is_home()) {
        $title = __('Lastest Post', RT_THEME_DOMAIN);
    }

    if (is_404()) {
        $title = __('404', RT_THEME_DOMAIN);
    }

    if (is_search()) {
        $title = sprintf(__('Search Results for: %s', RT_THEME_DOMAIN), get_search_query());
    }

    if (rt_is_woocommerce('shop')) {
        $title = post_type_archive_title( '', false );
    }

    return apply_filters('rt_page_title', $title);
}

/*=================================================
 * PAGE - DESC
=================================================== */
function rt_get_page_desc()
{
    $desc = '';
    if (is_archive()) {
        $desc = get_the_archive_description();
    }
    if (rt_is_woocommerce('shop')) {
        $desc = '';
    }
    return apply_filters('rt_page_desc', $desc);
}
