<?php
/*=================================================
 *  REMOVE ATTRIBUTE IMAGE
/*================================================= */
function rt_image_remove_width_attribute($html)
{
    $classes = 'img-responsive rt-img__attachment'; // separated by spaces, e.g. 'img image-link'
    // check if there are already classes assigned to the anchor
    if (preg_match('/<img.*? class="/', $html)) {
        $html = preg_replace('/(<img.*? class=".*?)(".*?\/>)/', '$1 ' . $classes . ' $2', $html);
    } else {
        $html = preg_replace('/(<img.*?)(\/>)/', '$1 class="' . $classes . '" $2', $html);
    }

    return $html;
}
add_filter('post_thumbnail_html', 'rt_image_remove_width_attribute', 10);
add_filter('image_send_to_editor', 'rt_image_remove_width_attribute', 10);

/*=================================================
 * IMAGE PLACE HOLDER
/*================================================= */
function rt_image_placeholder($size = "")
{
    if (rt_option('image_placeholder', true)) {

        $size = !empty($size) ? $size : 'featured_medium';
        $url = get_template_directory_uri() . "/assets/img/placeholder-{$size}.png";

        echo '<img src="' . esc_url($url) . '" class="img-responsive" alt="image-placeholder">';
    }
}

function rt_image_placeholder_url($size = "")
{
    if (rt_option('image_placeholder', true)) {
        $size = !empty($size) ? $size : 'featured_medium';
        echo get_template_directory_uri() . "/assets/img/placeholder-{$size}.png";
    }
}
