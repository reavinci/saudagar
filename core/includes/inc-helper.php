<?php

/*=================================================;
/* TEMPLATE PART
/*================================================= */
function rt_get_template_part($template, $data = '')
{
    get_template_part("template-parts/{$template}", '', $data);
}

/*=================================================
 * TEMPLATE PART IN SHOTCODE
=================================================== */
function rt_shotcode_get_template_part($args)
{
    rt_get_template_part($args['location']);
}
add_shortcode('rt_template_part', 'rt_shotcode_get_template_part');

/*=================================================
 *  LIMIT STRING
/*================================================= */
/*
 * @the_excerpt paragraf
 * @excerpt_length limit number
 * return string limite
 */
function rt_limited_string($the_excerpt, $excerpt_length)
{
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt));
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if (count($words) > $excerpt_length):
        array_pop($words);
        $the_excerpt = implode(' ', $words);
        $the_excerpt .= '...';
    endif;

    return $the_excerpt;
}

/*=================================================;
/* SET CLASS ROW
/*================================================= */
function rt_set_class_raw($filter = '', $classes = array())
{
    if (!empty($filter)) {
        $class_output = join(' ', apply_filters($filter, array_unique($classes)));
    } else {
        $class_output = join(' ', array_unique($classes));
    }

    return 'class="' . $class_output . '" data-class="' . $filter . '"';
}

/*=================================================
 *  SET CLASS
/*================================================= */
function rt_set_class($filter = '', $classes = array())
{
    echo rt_set_class_raw($filter, $classes);
}

/*=================================================;
/* PASSED URL GET METHOD
/*================================================= */
function rt_request_get($url)
{
    return !empty($_GET[$url]) ? $_GET[$url] : '';
}
/*=================================================;
/* PASSED URL GET METHOD
/*================================================= */
function rt_request_post($url)
{
    return !empty($_POST[$url]) ? $_POST[$url] : '';
}

/*=================================================
 * GET IMAGE ALL SIZE
=================================================== */
function rt_get_image_sizes()
{

    $image_size['featured_medium'] = 'featured_medium';
    $image_size['featured_medium_nocrop'] = 'featured_medium_nocrop';

    foreach (get_intermediate_image_sizes() as $key => $size) {
        $image_size[$size] = $size;
    }
    return $image_size;
}

/*=================================================;
/* ANIMATE REVERSE
/*=================================================
 *
 * @param [type] $animate
 * @return string
 * @example fadeIn this function return fadeOut
 */
function rt_animate_reverse($animate)
{
    return str_replace('In', 'Out', $animate);
}
/**
 * Is_ajax - Returns true when the page is loaded via ajax.
 *
 * @return bool
 */
function rt_is_ajax()
{
    return function_exists('wp_doing_ajax') ? wp_doing_ajax() : Constants::is_defined('DOING_AJAX');
}
