<?php
/*==============================================
 * RT OPTION
==============================================
 *  @desc this function get option from customizer
 * if metabox not null this function return data form metabox
 * name customizer, option and metabox must same
 * role: filter > metabox > option > customizer
 */
function rt_option($setting, $default = '')
{
    // check default options
    if (!empty(rt_get_field($setting)) && rt_get_field($setting) !== 'default') {

        $option = rt_get_field($setting);

    } elseif (!empty(rt_get_field($setting, 'option'))) {

        $option = rt_get_field($setting, 'option');

    } else {
        $option = get_theme_mod($setting, $default);
    }

    return apply_filters($setting, $option);

}
/*=================================================
 * HEADER ELEMENT OPTION
=================================================== */
/*
 * get element form header customizer
 * merge default and option form database
 */
function rt_option_header()
{
    $options = rt_option('header_builder_option', array(
        'topbar_left_alignment' => 'left',
        'topbar_center_alignment' => 'center',
        'topbar_right_alignment' => 'right',
        'topbar_left_display' => 'normal',
        'topbar_center_display' => 'normal',
        'topbar_right_display' => 'normal',
        'middle_left_alignment' => 'left',
        'middle_center_alignment' => 'center',
        'middle_right_alignment' => 'right',
        'middle_left_display' => 'normal',
        'middle_center_display' => 'normal',
        'middle_right_display' => 'normal',
        'main_left_alignment' => 'left',
        'main_center_alignment' => 'center',
        'main_right_alignment' => 'right',
        'main_left_display' => 'normal',
        'main_center_display' => 'normal',
        'main_right_display' => 'normal',
        'sticky_left_alignment' => 'left',
        'sticky_center_alignment' => 'center',
        'sticky_right_alignment' => 'right',
        'sticky_left_display' => 'normal',
        'sticky_center_display' => 'normal',
        'sticky_right_display' => 'normal',
        'mobile_left_alignment' => 'left',
        'mobile_center_alignment' => 'center',
        'mobile_right_alignment' => 'right',
        'mobile_left_display' => 'normal',
        'mobile_center_display' => 'normal',
        'mobile_right_display' => 'normal',
    ));

    return wp_parse_args($options, rt_var('header-default'));
}

/*=================================================;
/* GET FIELD ACF
/*================================================= */
/** This function replace default get field acf */

function rt_get_field($field, $post_id = false, $format_value = false)
{

    if (!class_exists('acf')) {
        return false;
    }

    $value = get_field($field, $post_id, $format_value);

    /**
     * Some values are saved as empty string or 0 for fields (e.g true_false fields).
     * So we used is_null instead of is_empty to check if post meta is set or not.
     */
    if (is_null($value)) {
        return false;

    }

    return $value;
}

/*=================================================;
/* LOCAL SERVER
/*================================================= */
/**
 * Check if user is only running the product’s on localhost
 */
function rt_is_local()
{
    $site = $_SERVER['SERVER_NAME'];

    $local = array(
        'localhost',
        '127.0.0.1',
        '10.0.0.0/8',
        '172.16.0.0/12',
        '192.168.0.0/16',
        '*.dev',
        '.*local',
        'dev.*',
        'staging.*',
    );

    if (in_array($site, $local)) {
        return false;
    }

}

/*=================================================;
/* HANDLE THEME
/*================================================= */
/**
 * Check if user is only running the product’s Premium Version code
 *
 * @return true on localhost or user have valid license
 */
function rt_is_premium()
{
    if (rt_is_local() || rt_is_dev()) {
        return true;
    } else {
        return (get_option(rt_var('product-slug', '_status')) == 'active') ? true : false;
    }

}
/**
 * code running premium active or not
 *
 * @return void
 */
function rt_is_premium_plan()
{
    return true;
}

function rt_is_free()
{
    if (rt_is_premium()) {
        return false;
    } else {
        return true;
    }
}

/**
 * Check if the user is on the free plan of the product and not want to active premium.
 */
function rt_is_free_plan()
{
    if (rt_is_premium_plan()) {
        return false;
    } else {
        return true;
    }

}

/**
 * code not running because, code for future plan
 *
 * @return void
 */
function rt_is_feature()
{
    return false;
}

/*
 * Check developer feature
 */
function rt_is_dev()
{
    if (rt_option('retheme_dev', false)) {
        $dev = true;
    } else {
        $dev = false;
    }
    return apply_filters('retheme_dev', $dev);
}

/*=================================================;
/* MOBILE - DETECT
/*================================================= */
// mobile and tablet
function rt_is_mobile()
{
    $detect = new Mobile_Detect;

    return ($detect->isMobile()) ? true : false;
}
// only mobile
function rt_is_only_mobile()
{
    $detect = new Mobile_Detect;

    if ($detect->isMobile() && !$detect->isTablet()) {
        return true;
    }

}
// only tablet
function rt_is_only_tablet()
{
    $detect = new Mobile_Detect;

    if ($detect->isTablet()) {
        return true;
    }

}

/*=================================================;
/* GUTENBERG - CHECK
/*================================================= */
function rt_is_gutenberg_active()
{
    $gutenberg = false;
    $block_editor = false;

    if (has_filter('replace_editor', 'gutenberg_init')) {
        // Gutenberg is installed and activated.
        $gutenberg = true;
    }

    if (version_compare($GLOBALS['wp_version'], '5.0-beta', '>')) {
        // Block editor.
        $block_editor = true;
    }

    if (!$gutenberg && !$block_editor) {
        return false;
    }

    include_once ABSPATH . 'wp-admin/includes/plugin.php';

    if (!is_plugin_active('classic-editor/classic-editor.php')) {
        return true;
    }

    $use_block_editor = (get_option('classic-editor-replace') === 'no-replace');

    return $use_block_editor;
}
