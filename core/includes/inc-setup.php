<?php
/**
 * retheme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package retheme
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function rt_setup()
{
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on retheme, use a find and replace
     * to change 'retheme' to the name of your theme in all the template files.
     */
    load_theme_textdomain(RT_THEME_DOMAIN, get_template_directory() . '/languages');

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    //Turn on wide images
    add_theme_support('align-wide');

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu() in one location.
    $menu['primary'] = 'Menu Primary';
    $menu['mobile'] = 'Menu Mobile';

    if (rt_is_premium()) {
        $menu['second'] = 'Menu Second';
        $menu['thirty'] = 'Menu Thirty';
        $menu['topbar'] = 'Menu Topbar';
        $menu['footer'] = 'Menu Footer';

    }

    register_nav_menus($menu);

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('post-thumbnails');
    add_theme_support('html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    /**
     * Add theme support selective refresh for customizer
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Add custom image size
     */
    add_image_size('featured_medium', 400, 225, true);
    add_image_size('featured_medium_nocrop', 400, 999999);

    /**
     * Add theme support vendor breadcrumbs
     * @see https://themehybrid.com/docs/breadcrumb-trail
     */
    add_theme_support('breadcrumb-trail');

    add_theme_support('title-tag');

    if (!isset($content_width)) {
        $content_width = 1170;
    }

    if (rt_is_premium()) {
        do_action('rt_after_setup_theme');
    }

}
add_action('after_setup_theme', 'rt_setup');

/*=================================================;
/* THEME - BRAND META
/*================================================= */
/**
 * Add theme color brand on search board mobile browser
 */
function rt_theme_color(){

    $color = rt_option('brand_color_browser');

    if(!empty($color)){
        echo '<meta name="theme-color" content="'.$color.'">';
    }
}
add_action( 'wp_head', 'rt_theme_color');

/*=================================================;
/* THEME - CHECK VERSION
/*================================================= */

function rt_check_theme_version()
{
    $current_version = wp_get_theme()->get('Version'); 
    $old_version = get_option(RT_THEME_SLUG . '_theme_old_version');

    if ($old_version !== $current_version) {
        // update not to run twice
        update_option(RT_THEME_SLUG . '_theme_old_version', $current_version);
        
    }
}

add_action('after_setup_theme', 'rt_check_theme_version');


/*=================================================;
/* BODY CLASS
/*================================================= */
function rt_theme_body_class($classes)
{
    if(rt_option('global_style', 'default') == 'card'){
        $style = rt_option('global_style', 'default');

        $classes[] = "template-style-{$style}";
    }
    
    if (rt_is_premium()) {
        $classes[] = 'pro';
    }

    if (rt_option('gutenberg-support', false)) {
        $classes[] = 'gutenberg-support';
    }

    // add class device on body
    if (rt_is_only_mobile()) {
        $classes[] = 'is-mobile';
    }elseif (rt_is_only_tablet()) {
        $classes[] = 'is-tablet';
    }else{
        $classes[] = 'is-desktop';
    }

    return $classes;
}

add_filter('body_class', 'rt_theme_body_class');

/*=================================================;
/* REQUIRED PLUGIN
/*================================================= */
/**
 * Register the required plugins for this theme.
 */
function rt_theme_required_plugins()
{
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $repo_plugins = array(
        // This is an example of how to include a plugin from the WordPress Plugin Repository.
        array(
            'name' => 'Elementor',
            'slug' => 'elementor',
        ),

        array(
            'name' => 'WooCommerce',
            'slug' => 'woocommerce',
        ),
        array(
            'name' => 'Contact Form 7',
            'slug' => 'contact-form-7',
        ),

        array(
            'name' => 'Ongkos Kirim',
            'slug' => 'ongkoskirim-id',
        ),

        array(
            'name' => 'Font Awesome',
            'source' => 'https://webforia.id/wp-content/uploads/webforia/font-awesome.zip',
        ),

        array(
            'name' => 'Variation Swatches',
            'slug' => 'variation-swatches-for-woocommerce',
        ),

    );

    $premium_plugins = array();

    if (rt_is_premium()) {
        $premium_plugins = array(
            array(
                'name' => 'Astro Element',
                'slug' => 'astro-element',
                'source' => 'https://webforia.id/wp-content/uploads/webforia/astro-element.zip',
            ),
            array(
                'name' => 'Webforia Shop Booster',
                'slug' => 'webforia-shop-booster',
                'source' => 'https://webforia.id/wp-content/uploads/webforia/webforia-shop-booster.zip',
            ),
            array(
                'name' => 'Webforia Whatsapp Chat',
                'slug' => 'webforia-whatsapp-chat',
                'source' => 'https://webforia.id/wp-content/uploads/webforia/webforia-whatsapp-chat.zip',
            ),

        );
    }

    $plugins = wp_parse_args($premium_plugins, $repo_plugins);

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
        'id' => 'tgmpa', // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '', // Default absolute path to bundled plugins.
        'menu' => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug' => 'theme-panel', // Parent menu slug.
        'capability' => 'edit_theme_options', // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices' => true, // Show admin notices or not.
        'dismissable' => true, // If false, a user cannot dismiss the nag message.
        'dismiss_msg' => true, // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false, // Automatically activate plugins after installation or not.
        'message' => '', // Message to output right before the plugins table.
    );

    tgmpa($plugins, $config);

}

add_action('tgmpa_register', 'rt_theme_required_plugins');

/*=================================================;
/* DEMO IMPORT
/*================================================= */
function rt_theme_demo_import()
{
    $coming_soon = array();

    $import = array(

        array(
            'import_file_name' => 'Saudagar Default',
            'categories' => array('WooCommerce'),
            'import_file_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/saudagar-demo-default.xml',
            'import_widget_file_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/saudagar-demo-default.wie',
            'import_customizer_file_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/saudagar-demo-default.dat',
            'import_preview_image_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/saudagar-demo-default.png',
            'preview_url' => 'https://demo.webforia.id/wp/saudagar',
        ),

    );

    return $import;

}

add_filter('merlin_import_files', 'rt_theme_demo_import');

/*=================================================;
/* DEFAUT VARIABLE
/*================================================= */
function rt_var($value, $arg = '')
{
    $default = apply_filters('rt_var', array(
        'product-name' => RT_THEME_NAME,
        'product-slug' => RT_THEME_SLUG,
        'product-version' => RT_THEME_VERSION,
        'product-docs' => RT_THEME_DOC,
        'product-url' => RT_THEME_URL,
        'product-group' => 'https://web.facebook.com/groups/265676900996871',
        'product-support' => 'https://web.facebook.com/groups/265676900996871',
        'product-contact' => 'https://webforia.id/kontak/',
        'font-primary' => 'Roboto',
        'font-weight' => '500',
        'font-second' => 'Roboto',
        'color-primary' => '#222222',
        'color-second' => '#333333',
        'font-size-body' => '14px',
        'line-height-body' => '1.5',
        'search-style' => 'dropdown',
        'extend' => array('astro-element', 'webforia-shop-booster', 'webforia-whatsapp-chat'),
    )
    );

    if ($arg) {
        $var = !empty($default[$value]) ? $default[$value] . $arg : '';
    } else {
        $var = !empty($default[$value]) ? $default[$value] : '';
    }

    return $var;

}
