<?php
/*=================================================;
/* MENU
/*================================================= */
/**
 * Add css class to menu item
 * @category Menu
 * @param [type] $classes
 * @param [type] $item
 * @return void
 */
function rt_menu_class($classes, $item)
{
    if ($item->current) {
        $classes[] = "is-active";
    }

    $classes[] = "rt-menu__item";

    return $classes;

}
add_filter('nav_menu_css_class', 'rt_menu_class', 10, 2);

/**
 * add class sub menu
 * @category menu
 * @param [type] $menu
 * @return void
 */
function rt_submenu_class($menu)
{
    $menu = preg_replace('/ class="sub-menu"/', '/ class="sub-menu rt-menu__submenu" /', $menu);
    return $menu;
}

add_filter('wp_nav_menu', 'rt_submenu_class');


/*=================================================
* SET DEFAULT MENU ITEM
=================================================== */
function rt_menu_fallback( $args )
{
    if ( ! current_user_can( 'manage_options' ) )
    {
        return;
    }

    // see wp-includes/nav-menu-template.php for available arguments
    extract( $args );

    $link = $link_before
        . '<a href="' .admin_url( 'nav-menus.php' ) . '">' . wp_sprintf(__('%s Edit Menu %s', RT_THEME_DOMAIN), $before, $after).'</a>'
        . $link_after;

    // We have a list
    if ( FALSE !== stripos( $items_wrap, '<ul' )
        or FALSE !== stripos( $items_wrap, '<ol' )
    )
    {
        $link = "<li class='rt-menu__item is-active'>$link</li>";
        $link .= wp_sprintf("<li class='rt-menu__item'><a>%s</a></li>", __('Sample Menu', RT_THEME_DOMAIN));
        $link .= wp_sprintf("<li class='rt-menu__item'><a>%s</a></li>", __('Sample Menu', RT_THEME_DOMAIN));
        $link .= wp_sprintf("<li class='rt-menu__item'><a>%s</a></li>", __('Sample Menu', RT_THEME_DOMAIN));
        $link .= wp_sprintf("<li class='rt-menu__item'><a>%s</a></li>", __('Sample Menu', RT_THEME_DOMAIN));
    }

    $output = sprintf( $items_wrap, $menu_id, $menu_class, $link );
    if ( ! empty ( $container ) )
    {
        $output  = "<$container class='$container_class' id='$container_id'>$output</$container>";
    }

    if ( $echo )
    {
        echo $output;
    }

    return $output;
}