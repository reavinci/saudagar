<?php

/*=================================================
 * TOOLBAL - COMPONENT CONDITIONAL
=================================================== */
function rt_wsb_is_toolbar()
{
    $location = '';

    if (!rt_is_woocommerce('checkout') && !rt_is_woocommerce('cart') && !is_single()) {
        if (rt_option('toolbar_shop', true) || rt_option('toolbar_filter', true) || rt_option('toolbar_account', true) || rt_option('toolbar_cart', true)) {
            $location = true;
        }
    }

    return rt_option('wbs_toolbar_location', $location);
}

/*=================================================;
/* SCRIPTS
/*================================================= */
function rt_wbs_scripts()
{
   if(is_singular( 'product' )){
       wp_enqueue_script('wbs-product', get_template_directory_uri() . '/assets/js/wbs-product.min.js', array('retheme'), '1.2.0', true);
   }
}

add_action('wp_enqueue_scripts', 'rt_wbs_scripts', 10);


include_once dirname(__FILE__) . '/class-sticky-product.php';
include_once dirname(__FILE__) . '/class-toolbar.php';