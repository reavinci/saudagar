<?php
namespace ShopBooster;

use Retheme\Customizer_Base;

class WSB_Toolbar
{
    public function __construct()
    {
        add_action('rt_footer', [$this, 'render'], 20);
        add_action('rt_footer', [$this, 'add_filter_panel'], 25);
        add_filter('body_class', [$this, 'add_body_class']);
        add_action('widgets_init', array($this, 'register_widget'));

        $this->customizer();
    }

    public function add_body_class($classes)
    {
        if (rt_wsb_is_toolbar()) {
            $classes[] = 'shop-booster-toolbar';
        }

        return $classes;
    }

    public function register_widget()
    {
        register_sidebar(array(
            'name' => __('WooCommerce Filter', 'webforia-shop-booster'),
            'id' => 'retheme_filter_sidebar',
            'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--aside %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
            'after_title' => '</h3></div>',
        ));

    }

    public function add_filter_panel()
    {
        if (rt_option('toolbar_filter', true) && rt_wsb_is_toolbar()) {
            rt_get_template_part('shop-booster/filter-panel');
        }

    }

    public function customizer()
    {
        $section = 'woocommerce_toolbar_section';

        $customizer = new Customizer_Base;

        $customizer->add_section('shop_booster', array(
            'woocommerce_toolbar' => array(__('Tools Bar', 'webforia-shop-booster')),
        ));

        $customizer->add_field(array(
            'type' => 'toggle',
            'settings' => 'toolbar_shop',
            'label' => __('Shop', 'webforia-shop-booster'),
            'section' => $section,
            'default' => true,
        ));

        $customizer->add_field(array(
            'type' => 'toggle',
            'settings' => 'toolbar_filter',
            'label' => __('Filter', 'webforia-shop-booster'),
            'section' => $section,
            'default' => true,
        ));

        $customizer->add_field(array(
            'type' => 'toggle',
            'settings' => 'toolbar_account',
            'label' => __('Account', 'webforia-shop-booster'),
            'section' => $section,
            'default' => true,
        ));

        $customizer->add_field(array(
            'type' => 'toggle',
            'settings' => 'toolbar_cart',
            'label' => __('Cart', 'webforia-shop-booster'),
            'section' => $section,
            'default' => true,
        ));
    }

    public function render()
    {
        if (rt_wsb_is_toolbar()) {
            rt_get_template_part('shop-booster/shop-toolbar');
        }

    }

}

new WSB_Toolbar;
