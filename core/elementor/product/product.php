<?php

namespace Retheme\Elementor;

use Elementor;
use Elementor\Controls_Manager;
use Retheme\Elementor_Base;
use Retheme\Helper;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Products extends Elementor_Base
{
    public function get_name()
    {
        return 'retheme-product';
    }

    public function get_title()
    {
        return __('Products', RT_THEME_DOMAIN);
    }

    public function get_icon()
    {
        return 'ate-icon ate-product';

    }

    public function get_categories()
    {
        return ['retheme-elements'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_option();
        
        $this->style_general();

        $this->setting_carousel();

    }

    public function setting_query()
    {
        $this->start_controls_section(
            'setting_query',
            [
                'label' => __('Query', RT_THEME_DOMAIN),
            ]
        );

        $this->add_control(
            'post_type',
            [
                'label' => __('Post Type', RT_THEME_DOMAIN),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => 'product',
            ]
        );

        $this->add_control(
            'posts_per_page',
            [
                'label' => __('Posts Number', RT_THEME_DOMAIN),
                'type' => Controls_Manager::NUMBER,
                'default' => 6,
            ]
        );

        $this->add_control(
            'advanced',
            [
                'label' => __('Advanced', RT_THEME_DOMAIN),
                'type' => Controls_Manager::HEADING,
            ]
        );
        $this->add_control(
            'query_by',
            [
                'label' => __('Query posts by', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT,
                'default' => 'lastest',
                'options' => [
                    'lastest' => __('Lastest Posts', RT_THEME_DOMAIN),
                    'featured' => __('Featured Products', RT_THEME_DOMAIN),
                    'category' => __('Category', RT_THEME_DOMAIN),
                    'manually' => __('Manually Id', RT_THEME_DOMAIN),
                ],
            ]
        );

        $this->add_control(
            'category',
            [
                'label' => __('Category', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'default' => 'lastest',
                'options' => Helper::get_terms('product_cat'),
                'condition' => [
                    'query_by' => 'category',
                ],
            ]
        );
        $this->add_control(
            'post_id',
            [
                'label' => __('Select Product', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_posts('product'),
                'condition' => [
                    'query_by' => 'manually',
                ],

            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'ID' => 'Post Id',
                    'author' => 'Post Author',
                    'title' => 'Title',
                    'date' => 'Date',
                    'modified' => 'Last Modified Date',
                    'parent' => 'Parent Id',
                    'rand' => 'Random',
                    'comment_count' => 'Comment Count',
                    'menu_order' => __('Menu Order', RT_THEME_DOMAIN),
                    'total_sales' => __('Most Selling', RT_THEME_DOMAIN),
                    'wp_post_views_count' => __('Most Viewer', RT_THEME_DOMAIN),
                    'comment_count' => __('Most Review', RT_THEME_DOMAIN),
                ],
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT,
                'default' => 'DESC',
                'options' => [
                    'ASC' => __('ASC', RT_THEME_DOMAIN),
                    'DESC' => __('DESC', RT_THEME_DOMAIN),
                ],
            ]
        );

        $this->add_control(
            'offset',
            [
                'label' => __('Offset', RT_THEME_DOMAIN),
                'type' => Controls_Manager::NUMBER,
                'default' => 0,
                'condition' => [
                    'posts_post_type!' => 'by_id',
                ],
                'description' => __('Use this setting to skip over posts (e.g. \'2\' to skip over 2 posts).', RT_THEME_DOMAIN),
            ]
        );
        $this->end_controls_section();
    }

    protected function setting_option()
    {
        $this->start_controls_section(
            'setting_product',
            [
                'label' => __('Options', RT_THEME_DOMAIN),
            ]
        );

        if(rt_is_premium()){
            $this->add_control(
            'layout_masonry',
            [
                'label' => __('Masonry', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'your-plugin'),
                'label_off' => __('Off', 'your-plugin'),
                'return_value' => 'no',
            ]
        );

        }
        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => __(1, RT_THEME_DOMAIN),
                    2 => __(2, RT_THEME_DOMAIN),
                    3 => __(3, RT_THEME_DOMAIN),
                    4 => __(4, RT_THEME_DOMAIN),
                    6 => __(6, RT_THEME_DOMAIN),
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );

        if (rt_is_premium()) {
            $this->add_control(
                'pagination_style',
                [
                    'label' => __('Pagination Style', RT_THEME_DOMAIN),
                    'type' => Controls_Manager::SELECT,
                    'default' => 'no_pagination',
                    'options' => array(
                        'no_pagination' => 'No Pagination',
                        'loadmore' => 'Load More',
                    ),
                ]
            );
        }

        $this->end_controls_section();
    }

    public function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', RT_THEME_DOMAIN),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .flex-loop' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .flex-loop > .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                    padding-right: calc({{SIZE}}{{UNIT}}/2);
                                                    margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        $args = array(
            'id' => $this->get_id(),
            'class_wrapper' => 'products woocommerce',
            'template_part' => 'woocommerce/content-product',
        );

        echo $this->elementor_loop(wp_parse_args($args, $settings));
    }
}
