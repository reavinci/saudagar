<?php
namespace Retheme\Elementor;

use Retheme\Elementor_Base;
use Retheme\Helper;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Posts extends Elementor_Base
{
    protected $post_type = 'post';
    protected $post_taxonomy = 'category';

    public function get_name()
    {
        return 'retheme-post';
    }

    public function get_title()
    {
        return __('Posts Grid', RT_THEME_DOMAIN);
    }

    public function get_icon()
    {
        return 'ate-icon ate-post';
    }

    public function get_categories()
    {
        return ['retheme-elements'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_options(); //protected

        $this->style_general(); //protected
        $this->style_body();
        $this->style_title();
        $this->style_badges();
        $this->style_meta();
        $this->style_excerpt();

        $this->setting_button(array(
            'name' => 'readmore',
            'class' => '.rt-post__readmore',
            'label' => 'Read More',
        ));

        $this->setting_button(array(
            'name' => 'loadmore',
            'label' => 'Load More',
            'class' => '.rt-pagination__button',
        ));

        $this->setting_carousel();
    }

    /**
     *  query post
     * @return [query section]
     */
    public function setting_query($args = array())
    {
        $this->start_controls_section(
            'setting_query',
            [
                'label' => __('Query', RT_THEME_DOMAIN),
            ]
        );

        /**
         *  Number perpage not show if widget smart-tiles
         */
        if (empty($args['limit_perpage'])) {
            $this->add_control(
                'posts_per_page',
                [
                    'label' => __('Posts Number', RT_THEME_DOMAIN),
                    'type' => Controls_Manager::NUMBER,
                    'default' => 6,
                ]
            );
            $this->add_control(
                'advanced',
                [
                    'label' => __('Advanced', RT_THEME_DOMAIN),
                    'type' => Controls_Manager::HEADING,
                ]
            );
        } else {
            $this->add_control(
                'advanced',
                [
                    'label' => __('Advanced', RT_THEME_DOMAIN),
                    'type' => Controls_Manager::HEADING,
                    'separator' => 'before',
                ]
            );
        }

        $this->add_control(
            'query_by',
            [
                'label' => __('Query posts by', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT,
                'default' => 'lastest',
                'options' => [
                    'lastest' => __('Lastest Posts', RT_THEME_DOMAIN),
                    'category' => __('Category', RT_THEME_DOMAIN),
                    'manually' => __('Manually', RT_THEME_DOMAIN),
                ],
            ]
        );

        $this->add_control(
            'category',
            [
                'label' => __('Categories', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_terms($this->post_taxonomy),
                'condition' => [
                    'query_by' => 'category',
                ],
            ]
        );
        $this->add_control(
            'post_id',
            [
                'label' => __('Select Post', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_posts($this->post_type),
                'condition' => [
                    'query_by' => 'manually',
                ],

            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'ID' => 'Post Id',
                    'author' => 'Post Author',
                    'title' => 'Title',
                    'date' => 'Date',
                    'modified' => 'Last Modified Date',
                    'parent' => 'Parent Id',
                    'rand' => 'Random',
                    'comment_count' => 'Comment Count',
                    'menu_order' => __('Menu Order', RT_THEME_DOMAIN),
                    'wp_post_views_count' => __('Most Viewer', RT_THEME_DOMAIN),
                    'comment_count' => __('Most Review', RT_THEME_DOMAIN),
                ],
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT,
                'default' => 'DESC',
                'options' => [
                    'ASC' => __('ASC', RT_THEME_DOMAIN),
                    'DESC' => __('DESC', RT_THEME_DOMAIN),
                ],
            ]
        );

        $this->add_control(
            'offset',
            [
                'label' => __('Offset', RT_THEME_DOMAIN),
                'type' => Controls_Manager::NUMBER,
                'default' => 0,
                'description' => __('Use this setting to skip over posts (e.g. \'2\' to skip over 2 posts).', RT_THEME_DOMAIN),
            ]
        );

        $this->end_controls_section();
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', RT_THEME_DOMAIN),
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => __(1, RT_THEME_DOMAIN),
                    2 => __(2, RT_THEME_DOMAIN),
                    3 => __(3, RT_THEME_DOMAIN),
                    4 => __(4, RT_THEME_DOMAIN),
                    6 => __(6, RT_THEME_DOMAIN),
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );

        $this->add_control(
            'layout_masonry',
            [
                'label' => __('Masonry', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', RT_THEME_DOMAIN),
                'label_on' => __('On', RT_THEME_DOMAIN),
                'return_value' => 'yes',
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'excerpt',
            [
                'label' => __('Excerpt Number', RT_THEME_DOMAIN),
                'type' => Controls_Manager::NUMBER,
                'default' => __('18', RT_THEME_DOMAIN),
            ]
        );

        $this->add_control(
            'readmore',
            [
                'label' => __('Read More', RT_THEME_DOMAIN),
                'type' => Controls_Manager::TEXT,
                'default' => __('Read More', RT_THEME_DOMAIN),
            ]
        );

        $this->add_control(
            'meta_category',
            [
                'label' => __('Category', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', RT_THEME_DOMAIN),
                'label_off' => __('Off', RT_THEME_DOMAIN),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_date',
            [
                'label' => __('Date', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', RT_THEME_DOMAIN),
                'label_off' => __('Off', RT_THEME_DOMAIN),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_author',
            [
                'label' => __('Author', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', RT_THEME_DOMAIN),
                'label_off' => __('Off', RT_THEME_DOMAIN),
                'return_value' => 'yes',
            ]
        );

        // $this->add_control(
        //     'meta_view',
        //     [
        //         'label' => __('View Count', RT_THEME_DOMAIN),
        //         'type' => Controls_Manager::SWITCHER,
        //         'default' => 'no',
        //         'label_on' => __('On', RT_THEME_DOMAIN),
        //         'label_off' => __('Off', RT_THEME_DOMAIN),
        //         'return_value' => 'yes',
        //     ]
        // );

        $this->add_control(
            'meta_comment',
            [
                'label' => __('Comment Count', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', RT_THEME_DOMAIN),
                'label_off' => __('Off', RT_THEME_DOMAIN),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'pagination_style',
            [
                'label' => __('Pagination Style', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SELECT,
                'default' => 'no_pagination',
                'options' => array(
                    'no_pagination' => 'No Pagination',
                    'loadmore' => 'Load More',
                ),
            ]
        );


        $this->end_controls_section();
    }



    protected function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', RT_THEME_DOMAIN),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .flex-loop' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .flex-loop > .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                    padding-right: calc({{SIZE}}{{UNIT}}/2);
                                                    margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_responsive_control(
            'general_alignment',
            [
                'label' => __('Layout Alignment', RT_THEME_DOMAIN),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', RT_THEME_DOMAIN),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', RT_THEME_DOMAIN),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', RT_THEME_DOMAIN),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-post__title,
                    {{WRAPPER}} .rt-badges,
                    {{WRAPPER}} .rt-post__meta,
                    {{WRAPPER}} .rt-post__body' => 'text-align: {{VALUE}};',
                ],

            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'general_border',
                'selector' => '{{WRAPPER}} .rt-post',
            ]
        );

        $this->add_responsive_control(
            'general_radius',
            [
                'label' => __('Border Radius', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 80,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .rt-post' => 'border-radius: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();
    }

    public function style_image()
    {
        $this->start_controls_section(
            'style_image',
            [
                'label' => __('Image', RT_THEME_DOMAIN),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_responsive_control(
            'image_width',
            [
                'label' => __('Image Width', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 80,
                        'max' => 200,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-post__thumbnail' => 'width: {{SIZE}}{{UNIT}};',
                ],

            ]
        );
        $this->end_controls_section();

    }

    public function style_body()
    {

        $this->start_controls_section(
            'style_body',
            [
                'label' => __('Body', RT_THEME_DOMAIN),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'body_background',
            [
                'label' => __('Background Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'body_padding',
            [
                'label' => __('Padding', RT_THEME_DOMAIN),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .rt-post__body' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();

    }

    public function style_title()
    {
        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', RT_THEME_DOMAIN),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('title_tabs');
        $this->start_controls_tab(
            'title_normal',
            [
                'label' => __('Normal', RT_THEME_DOMAIN),
            ]
        );
        $this->add_control(
            'title_color',
            [
                'label' => __('Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__title a' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'title_hover',
            [
                'label' => __('Hover', RT_THEME_DOMAIN),
            ]
        );
        $this->add_control(
            'title_color_hover',
            [
                'label' => __('Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__title a:hover' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'selector' => '{{WRAPPER}} .rt-post__title',
            ]
        );

        $this->add_responsive_control(
            'title_margin',
            [
                'label' => __('Title Spacing', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-post__title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    public function style_badges()
    {
        $this->start_controls_section(
            'style_badges',
            [
                'label' => __('Badges', RT_THEME_DOMAIN),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('badges_tabs');
        $this->start_controls_tab(
            'badges_normal',
            [
                'label' => __('Normal', RT_THEME_DOMAIN),
            ]
        );
        $this->add_control(
            'badges_color',
            [
                'label' => __('Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-badges a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'badges_background',
            [
                'label' => __('Background Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-badges a' => 'background-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'badges_hover',
            [
                'label' => __('Hover', RT_THEME_DOMAIN),
            ]
        );
        $this->add_control(
            'badges_color_hover',
            [
                'label' => __('Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-badges a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'badges_background_hover',
            [
                'label' => __('Background Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-badges a:hover' => 'background-color: {{VALUE}} !important;',
                ],
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

        $this->add_responsive_control(
            'badges_padding',
            [
                'label' => __('Padding', RT_THEME_DOMAIN),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .rt-badges a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'separator' => 'before',
            ]

        );

        $this->add_responsive_control(
            'badges_radius',
            [
                'label' => __('Badges Radius', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 3,
                    'unit' => 'px',
                ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .rt-badges a' => 'border-radius: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_meta()
    {
        $this->start_controls_section(
            'style_meta',
            [
                'label' => __('Meta', RT_THEME_DOMAIN),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'meta_color',
            [
                'label' => __('Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__meta-item' => 'color: {{VALUE}};',
                ],
            ]
        );

        /* start title color */
        $this->start_controls_tabs('meta_tabs');
        $this->start_controls_tab(
            'meta_normal',
            [
                'label' => __('Normal', RT_THEME_DOMAIN),
            ]
        );

        $this->add_control(
            'meta_link',
            [
                'label' => __('Primary Link', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__meta a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'meta_hover',
            [
                'label' => __('Hover', RT_THEME_DOMAIN),
            ]
        );
        $this->add_control(
            'meta_link_hover',
            [
                'label' => __('Primary Link', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__meta a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */

        // $this->add_group_control(
        //     Group_Control_Typography::get_type(),
        //     [
        //         'name' => 'meta_typography',
        //         'selector' => '{{WRAPPER}} .rt-post__meta-item',
        //         'separator' => 'before',
        //     ]
        // );

        $this->add_control(
            'meta_color_divender',
            [
                'label' => __('Color Divender', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__meta-item::before' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'meta_margin',
            [
                'label' => __('Meta Spacing', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-post__meta' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    public function style_excerpt()
    {
        $this->start_controls_section(
            'style_excerpt',
            [
                'label' => __('Excerpt', RT_THEME_DOMAIN),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'excerpt_color',
            [
                'label' => __('Color', RT_THEME_DOMAIN),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'excerpt_typography',
                'selector' => '{{WRAPPER}} .rt-post__content',
            ]
        );

        $this->add_responsive_control(
            'excerpt_margin',
            [
                'label' => __('Excerpt Spacing', RT_THEME_DOMAIN),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-post__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();
       
        $args = array(
            'post_type' => 'post',
            'id' => $this->get_id(),
            'template_part' => 'core/elementor/post/post-view',
        );


        echo $this->elementor_loop(wp_parse_args($args, $settings));

    }
    /* end class */
}
