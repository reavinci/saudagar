<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Sidebar extends Customizer_Base
{
    public function __construct()
    {
        $this->set_section();

        $this->add_option();
        $this->add_layout();
    }

    public function set_section()
    {
        $this->add_section('', array(
            'sidebar_option' => array(esc_attr__('Sidebar', RT_THEME_DOMAIN)),
            'sidebar_layout' => array(esc_attr__('Sidebar - Layout', RT_THEME_DOMAIN)),
            'sidebar_Element' => array(esc_attr__('Sidebar - Element', RT_THEME_DOMAIN)),
        ));
    }

    public function add_option()
    {
        if (rt_is_premium()) {
            $this->add_field(array(
                'type' => 'toggle',
                'settings' => 'sidebar_sticky',
                'label' => __('Enable Sticky', RT_THEME_DOMAIN),
                'section' => 'sidebar_option_section',
                'default' => true,
            ));
        }
    }
    public function add_layout()
    {
        $section = 'sidebar_option_section';
        $settings = 'sidebar_layout';

        $this->add_field(array(
            'type' => 'radio-buttonset',
            'settings' => $settings . '_home',
            'label' => __('Sidebar On Home Page', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => 'right',
            'choices' => array(
                'none' => __('No Sidebar', RT_THEME_DOMAIN),
                'left' => __('Left', RT_THEME_DOMAIN),
                'right' => __('Right', RT_THEME_DOMAIN),
            ),
        ));

        $this->add_field(array(
            'type' => 'radio-buttonset',
            'settings' => $settings . '_archive',
            'label' => __('Sidebar On Archive Page', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => 'right',
            'choices' => array(
                'none' => __('No Sidebar', RT_THEME_DOMAIN),
                'left' => __('Left', RT_THEME_DOMAIN),
                'right' => __('Right', RT_THEME_DOMAIN),
            ),
        ));

        $this->add_field(array(
            'type' => 'radio-buttonset',
            'settings' => $settings . '_single',
            'label' => __('Sidebar On Single Page', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => 'right',
            'choices' => array(
                'none' => __('No Sidebar', RT_THEME_DOMAIN),
                'left' => __('Left', RT_THEME_DOMAIN),
                'right' => __('Right', RT_THEME_DOMAIN),
            ),
        ));

        $this->add_field(array(
            'type' => 'radio-buttonset',
            'settings' => $settings . '_page',
            'label' => __('Sidebar On Regular Page', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => 'right',
            'choices' => array(
                'none' => __('No Sidebar', RT_THEME_DOMAIN),
                'left' => __('Left', RT_THEME_DOMAIN),
                'right' => __('Right', RT_THEME_DOMAIN),
            ),
        ));

        if (rt_is_woocommerce()) {
            $this->add_field(array(
                'type' => 'radio-buttonset',
                'settings' => $settings . '_woocommerce_archive',
                'label' => __('Sidebar On Shop', RT_THEME_DOMAIN),
                'section' => $section,
                'default' => 'right',
                'choices' => array(
                    'none' => __('No Sidebar', RT_THEME_DOMAIN),
                    'left' => __('Left', RT_THEME_DOMAIN),
                    'right' => __('Right', RT_THEME_DOMAIN),
                ),
            ));

            $this->add_field(array(
                'type' => 'radio-buttonset',
                'settings' => $settings . '_woocommerce_single',
                'label' => __('Sidebar On Product', RT_THEME_DOMAIN),
                'section' => $section,
                'default' => 'none',
                'choices' => array(
                    'none' => __('No Sidebar', RT_THEME_DOMAIN),
                    'left' => __('Left', RT_THEME_DOMAIN),
                    'right' => __('Right', RT_THEME_DOMAIN),
                ),
            ));

        }
    }

    // end class
}

new Sidebar;
