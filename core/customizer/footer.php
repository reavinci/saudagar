<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Footer extends Customizer_Base
{

    public function __construct()
    {
        $this->set_panel();
        $this->set_section();

        $this->add_option_widget();

        $this->add_option_bottom();

    }

    public function set_panel()
    {
        $this->add_panel('footer_panel', array(
            'title' => __('Footer', RT_THEME_DOMAIN),
        ));
    }

    public function set_section()
    {
        $this->add_section('footer_panel', array(
            'footer_widget' => array(esc_attr__('Widget Area', RT_THEME_DOMAIN)),
            'footer_bottom' => array(esc_attr__('Bottom Footer', RT_THEME_DOMAIN)),
            'footer_categories' => array(esc_attr__('Categories', RT_THEME_DOMAIN)),
            'footer_tag' => array(esc_attr__('Tags', RT_THEME_DOMAIN)),
            'footer_button' => array(esc_attr__('Button', RT_THEME_DOMAIN)),
            'footer_form' => array(esc_attr__('Form', RT_THEME_DOMAIN)),
        ));
    }

    public function add_option_widget()
    {

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'footer_widget',
            'label' => __('Enable Widget Footer', RT_THEME_DOMAIN),
            'section' => 'footer_widget_section',
            'default' => true,
        ));

        // developer enable footer builder required ACF PRO

        if (rt_is_premium()) {
            /**
             * get list from elementor library
             */
            $this->add_field(array(
                'type' => 'radio-image',
                'section' => 'footer_widget_section',
                'settings' => 'footer_widget_layout',
                'label' => __('Layout', RT_THEME_DOMAIN),
                'default' => 'footer-1',
                'choices' => array(
                    'footer-1' => get_template_directory_uri() . '/core/customizer/assets/img/footer-1.svg',
                    'footer-2' => get_template_directory_uri() . '/core/customizer/assets/img/footer-2.svg',
                    'footer-3' => get_template_directory_uri() . '/core/customizer/assets/img/footer-3.svg',
                    // 'footer-elementor' => get_template_directory_uri() . '/core/customizer/assets/img/footer-builder.svg',
                ),
            ));
        } else {
            $this->add_field(array(
                'type' => 'radio-image',
                'section' => 'footer_widget_section',
                'settings' => 'footer_widget_layout',
                'label' => __('Layout', RT_THEME_DOMAIN),
                'default' => 'footer-1',
                'choices' => array(
                    'footer-1' => get_template_directory_uri() . '/core/customizer/assets/img/footer-1.svg',
                    'footer-2' => get_template_directory_uri() . '/core/customizer/assets/img/footer-2.svg',
                    'footer-3' => get_template_directory_uri() . '/core/customizer/assets/img/footer-3.svg',
                ),
            ));

        }
        // $this->add_field(array(
        //     'type' => 'select',
        //     'section' => 'footer_widget_section',
        //     'settings' => 'footer_widget_layout_elementor',
        //     'label' => __('Elementor Library', RT_THEME_DOMAIN),
        //     'multiple' => 1,
        //     'choices' => \Retheme\Helper::get_posts('elementor_library'),
        //     'active_callback' => array(
        //         array(
        //             'setting' => 'footer_widget_layout',
        //             'operator' => '==',
        //             'value' => array('footer-elementor'),
        //         ),
        //     ),

        // ));

        $this->add_field_responsive(array(
            'type' => 'typography',
            'settings' => 'footer_widget_heading_typography',
            'label' => __('Heading Typography', RT_THEME_DOMAIN),
            'section' => 'footer_widget_section',
            'default' => array(
                'variant' => rt_var('font-weight'),
                'font-size' => '',
                'line-height' => '',
                'text-transform' => 'none',
            ),
            'output' => array(
                array(
                    'element' => '.rt-widget--footer .rt-widget__title',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field_color(array(
            'label' => __('Heading Color', RT_THEME_DOMAIN),
            'settings' => 'footer_widget_heading_color',
            'section' => 'footer_widget_section',
            'element' => '.rt-widget--footer .rt-widget__title',
        ));

        $this->add_field_link(array(
            'settings' => 'footer_option_widget_link',
            'section' => 'footer_widget_section',
            'element' => '.rt-widget--footer a',
            'pseudo' => 'hover',
            'suffix' => '!important',
        ));

        $this->add_field_color(array(
            'settings' => 'footer_option_widget_color',
            'section' => 'footer_widget_section',
            'element' => '.rt-widget--footer, .rt-widget--footer .textwidget',
        ));

        $this->add_field_color(array(
            'label' => 'Color Accents',
            'settings' => 'footer_option_widget_color_accent',
            'section' => 'footer_widget_section',
            'element' => '.widget_recent_entries.rt-widget--footer .post-date',
        ));

        $this->add_field_background(array(
            'settings' => 'footer_option_widget_background',
            'section' => 'footer_widget_section',
            'element' => '.page-footer__widget',
        ));

        $this->add_field_border_color(array(
            'settings' => 'footer_option_widget_border_color',
            'section' => 'footer_widget_section',
            'element' => '.page-footer__widget',
        ));

    }

    public function add_option_bottom()
    {
        $section = 'footer_bottom_section';

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'footer_bottom',
            'label' => __('Enable Footer Bottom', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'sortable',
            'settings' => 'footer_bottom_element',
            'label' => __('Element', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => array(
                'html-1',
            ),
            'choices' => array(
                'bottom-menu' => __('Bottom Menu', RT_THEME_DOMAIN),
                'social' => __('Social Media', RT_THEME_DOMAIN),
                'spacer-1' => __('Spacer 1', RT_THEME_DOMAIN),
                'spacer-2' => __('Spacer 2', RT_THEME_DOMAIN),
                'html-1' => __('Copyright', RT_THEME_DOMAIN),
                'image-1' => __('Image', RT_THEME_DOMAIN),
            ),

        ));

        $this->add_field(array(
            'type' => 'radio-buttonset',
            'settings' => 'footer_bottom_position',
            'label' => __('Position', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => 'space-between',
            'choices' => array(
                'space-between' => __('Justify', RT_THEME_DOMAIN),
                'left' => __('Left', RT_THEME_DOMAIN),
                'right' => __('Right', RT_THEME_DOMAIN),
                'center' => __('Center', RT_THEME_DOMAIN),
            ),
            'transport' => 'auto',
            'output' => array(
                array(
                    'element' => '.page-footer__bottom .page-container',
                    'property' => 'justify-content',
                ),
            ),

        ));

        $this->add_field(array(
            'label' => 'Copyright (Support HTML tag)',
            'settings' => "footer_bottom_text",
            'description' => __('You can use {{year}} to get the current year', RT_THEME_DOMAIN),
            'section' => $section,
            "default" => "@Copyright " . get_bloginfo('name') . ". All Rights Reserved",
            'type' => 'textarea',
        ));

        $this->add_field(array(
            'type' => 'image',
            'settings' => "footer_bottom_img_1",
            'section' => $section,
            'label' => __('Image', RT_THEME_DOMAIN),
            'description' => __('You can upload image like payment logo, secure logo etc', RT_THEME_DOMAIN),
        ));

        $this->add_field_color(array(
            'settings' => 'footer_option_bottom_color',
            'section' => $section,
            'element' => '.page-footer__bottom',
        ));

        $this->add_field_link(array(
            'settings' => 'footer_option_bottom_link',
            'section' => $section,
            'element' => '.page-footer__bottom a,
                    .page-footer__bottom .rt-menu--bar-simple li a',
            'pseudo' => 'hover',
        ));

        $this->add_field_background(array(
            'settings' => 'footer_option_bottom_background',
            'section' => $section,
            'element' => '.page-footer__bottom',
        ));

        $this->add_field_border_color(array(
            'settings' => 'footer_option_bottom_border_color',
            'section' => $section,
            'element' => '.page-footer__bottom',
        ));

    }

// end class
}

new Footer;
