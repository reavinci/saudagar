<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Brand extends Customizer_Base
{

    public function __construct()
    {

        $this->add_brand_logo();

    }

    public function add_brand_logo()
    {

        $this->add_header(array(
            'label' => __('Desktop Logo', RT_THEME_DOMAIN),
            'settings' => 'brand_logo',
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'image',
            'settings' => 'brand_logo_primary',
            'label' => __('Default Logo', RT_THEME_DOMAIN),
            'tooltip' => __('This logo will be used as your default logo', RT_THEME_DOMAIN),
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'slider',
            'settings' => 'brand_logo_primary_size',
            'label' => __('Default Logo Width', RT_THEME_DOMAIN),
            'section' => 'title_tagline',
            'choices' => array(
                'min' => '10',
                'max' => '300',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header__main .rt-logo, .rt-header__middle .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));

        if (rt_is_premium()) {
            $this->add_field(array(
                'type' => 'image',
                'settings' => 'brand_logo_sticky',
                'label' => __('Sticky Logo', RT_THEME_DOMAIN),
                'tooltip' => __('This logo will be used when the header is in a sticky state.', RT_THEME_DOMAIN),
                'section' => 'title_tagline',
            ));

            $this->add_field(array(
                'type' => 'slider',
                'settings' => 'brand_logo_sticky_size',
                'label' => __('Sticky Logo Width', RT_THEME_DOMAIN),
                'section' => 'title_tagline',
                'choices' => array(
                    'min' => '10',
                    'max' => '300',
                    'step' => '1',
                ),
                'output' => array(
                    array(
                        'element' => '.rt-header__sticky .rt-logo',
                        'property' => 'width',
                        'units' => 'px',
                    ),

                ),
                'transport' => 'auto',
            ));
        }

        if (rt_is_feature()) {
            $this->add_field(array(
                'type' => 'image',
                'settings' => 'brand_logo_overlay',
                'label' => __('Overlay Logo Light', RT_THEME_DOMAIN),
                'tooltip' => __('This logo will be used when the transparent header is enabled.', RT_THEME_DOMAIN),
                'section' => 'title_tagline',
            ));

            $this->add_field(array(
                'type' => 'slider',
                'settings' => 'brand_logo_overlay_size',
                'label' => __('Overlay Logo Width', RT_THEME_DOMAIN),
                'section' => 'title_tagline',
                'choices' => array(
                    'min' => '10',
                    'max' => '300',
                    'step' => '1',
                ),
                'output' => array(
                    array(
                        'element' => '.rt-header.is-overlay .rt-header__main .rt-logo',
                        'property' => 'width',
                        'units' => 'px',
                    ),

                ),
                'transport' => 'auto',
            ));
        }

        $this->add_header(array(
            'label' => __('Mobile Logo', RT_THEME_DOMAIN),
            'settings' => 'brand_logo_mobile',
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'image',
            'settings' => 'brand_logo_mobile',
            'label' => __('Mobile Logo', RT_THEME_DOMAIN),
            'tooltip' => __('This logo will be used on mobile devices', RT_THEME_DOMAIN),
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'slider',
            'settings' => 'brand_logo_mobile_size',
            'label' => __('Mobile Logo Width', RT_THEME_DOMAIN),
            'section' => 'title_tagline',
            'choices' => array(
                'min' => '10',
                'max' => '200',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header-mobile__main .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));

        if (rt_is_premium()) {
            $this->add_field(array(
                'type' => 'image',
                'settings' => 'brand_logo_mobile_sticky',
                'label' => __('Sticky Logo', RT_THEME_DOMAIN),
                'tooltip' => __('This logo will be used when the header is in a sticky state on mobile devices.', RT_THEME_DOMAIN),
                'section' => 'title_tagline',
            ));

            $this->add_field(array(
                'type' => 'slider',
                'settings' => 'brand_logo_mobile_size',
                'label' => __('Mobile Logo Width', RT_THEME_DOMAIN),
                'section' => 'title_tagline',
                'choices' => array(
                    'min' => '10',
                    'max' => '200',
                    'step' => '1',
                ),
                'output' => array(
                    array(
                        'element' => '.rt-header-mobile__main .rt-logo',
                        'property' => 'width',
                        'units' => 'px',
                    ),

                ),
                'transport' => 'auto',
            ));

        }

        if (rt_is_feature()) {
            $this->add_field(array(
                'type' => 'image',
                'settings' => 'brand_logo_mobile_overlay',
                'label' => __('Overlay Logo Light', RT_THEME_DOMAIN),
                'tooltip' => __('This logo will be used when the transparent header is enabled on mobile devices.', RT_THEME_DOMAIN),
                'section' => 'title_tagline',
            ));

            $this->add_field(array(
                'type' => 'slider',
                'settings' => 'brand_logo_mobile_overlay_size',
                'label' => __('Overlay Logo Width', RT_THEME_DOMAIN),
                'section' => 'title_tagline',
                'choices' => array(
                    'min' => '10',
                    'max' => '200',
                    'step' => '1',
                ),
                'output' => array(
                    array(
                        'element' => '.rt-header-mobile.is-overlay .rt-header-mobile__main .rt-logo',
                        'property' => 'width',
                        'units' => 'px',
                    ),

                ),
                'transport' => 'auto',
            ));

        }

        if (rt_is_premium()) {
            $this->add_header(array(
                'label' => __('Checkout Logo', RT_THEME_DOMAIN),
                'settings' => 'focus_logo',
                'section' => 'title_tagline',
            ));

            $this->add_field(array(
                'type' => 'image',
                'settings' => 'focus_logo_primary',
                'label' => __('Checkout Logo', RT_THEME_DOMAIN),
                'tooltip' => __('This logo will be used on checkout template', RT_THEME_DOMAIN),
                'section' => 'title_tagline',
            ));

            $this->add_field(array(
                'type' => 'slider',
                'settings' => 'focus_logo_primary_size',
                'label' => __('Checkout Logo Width', RT_THEME_DOMAIN),
                'section' => 'title_tagline',
                'choices' => array(
                    'min' => '10',
                    'max' => '300',
                    'step' => '1',
                ),
                'output' => array(
                    array(
                        'element' => '.ptf-header .rt-logo',
                        'property' => 'width',
                        'units' => 'px',
                    ),

                ),
                'transport' => 'auto',
            ));

        }

         $this->add_header(array(
            'label' => __('Color Browser', RT_THEME_DOMAIN),
            'settings' => 'brand_color_browser',
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'color',
            'description' => 'Warna Browser situs adalah warna yang Anda lihat di tab browser, bilah bookmark, dan ketika menggunakan browser mobile.',
            'settings' => 'brand_color_browser',
            'label' => 'Color Browser',
            'section' => 'title_tagline',
        ));

        $this->add_header(array(
            'label' => __('Pavicon', RT_THEME_DOMAIN),
            'settings' => 'brand_pavicon',
            'section' => 'title_tagline',
        ));

    }
    



// end class
}

new Brand;
