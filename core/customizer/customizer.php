<?php
namespace Retheme\Customizer;

class Customizer
{

    public function __construct()
    {
        add_action('customize_controls_enqueue_scripts', [$this, 'retheme_customizer_control']);
        add_action('customize_preview_init', [$this, 'register_customizer_preview']);
        add_action('after_setup_theme', [$this, 'include_part']);
    }

    public function include_part()
    {
        /**
         * Filter customizer section
         */
        $customizers = apply_filters('rt_customizer_settings', array(
            'general',
            'header',
            'blog',
            'sidebar',
            'footer',
            'brand',
            'page',
            'search',
            'connect',
            'woocommerce',
        ));

        if (is_array($customizers)) {
            /** custom control */
            include dirname(__FILE__) . '/control/builder/builder.php';

            foreach ($customizers as $key => $customizer) {
                include dirname(__FILE__) . "/{$customizer}.php";
            }

        }

    }


    public function retheme_customizer_control()
    {
        wp_enqueue_style('retheme_customizer_panel', get_template_directory_uri() . '/core/customizer/assets/css/customizer-panel.css', false, '1.0.0');
        wp_enqueue_script('retheme_customizer_panel', get_template_directory_uri() . '/core/customizer/assets/js/customizer-panel.js', array('jquery'), false, true);
        wp_enqueue_script('retheme_customizer_control', get_template_directory_uri() . '/core/customizer/assets/js/customizer-control.js', array('jquery'), false, true);
    }

    public function register_customizer_preview()
    {
        wp_enqueue_style('retheme_customizer_preview', get_template_directory_uri() . '/core/customizer/assets/css/customizer-preview.css', false, false);
        wp_enqueue_script('retheme_customizer_preview', get_template_directory_uri() . '/core/customizer/assets/js/customizer-preview.js', array('jquery'), false, false);
    }
    // end class
}

new Customizer;
