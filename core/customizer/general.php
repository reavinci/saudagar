<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class General extends Customizer_Base
{

    public function __construct()
    {
        $this->set_panel();

        $this->set_section();


        $this->add_typography();
        $this->add_color();

        $this->add_breadcrumbs();
        $this->add_topup();
        $this->add_slider();

        $this->add_button_primary();

        if (class_exists('woocommerce')) {
            $this->add_button_second();
            $this->add_button_action();
            $this->add_button_whatsapp();
        }

        $this->add_pagination_number();
        $this->add_pagination_loadmore();

    }

    public function set_panel()
    {
        $this->add_panel('general_panel', array(
            'title' => __('Global', RT_THEME_DOMAIN),
        ));
    }

    public function set_section()
    {
        $this->add_section('general_panel', array(
            'general_style' => array(esc_attr__('Style', RT_THEME_DOMAIN)),
            'general_typography' => array(esc_attr__('Typography', RT_THEME_DOMAIN)),
            'general_color' => array(esc_attr__('Background & Color', RT_THEME_DOMAIN)),
            'general_breadcrumbs' => array(esc_attr__('Breadcrumbs', RT_THEME_DOMAIN)),
            'general_topup' => array(esc_attr__('Top Up', RT_THEME_DOMAIN)),
            'general_pagination' => array(esc_attr__('Pagination', RT_THEME_DOMAIN)),
            'general_button' => array(esc_attr__('Button', RT_THEME_DOMAIN)),
            'general_slider' => array(esc_attr__('Slider', RT_THEME_DOMAIN)),

        ));
    }

    public function add_style()
    {
        $section = 'general_style_section';

        $this->add_field(array(
            'type' => 'select',
            'section' => $section,
            'settings' => 'global_style',
            'label' => __('Website Style', RT_THEME_DOMAIN),
            'default' => 'default',
            'multiple' => 1,
            'choices' => [
                'default' => 'Default',
                'card' => 'Card',
            ],
        ));

    }
    public function add_typography()
    {

        $this->add_field(array(
            'label' => __('All Heading', RT_THEME_DOMAIN),
            'type' => 'typography',
            'settings' => 'typography_heading',
            'section' => 'general_typography_section',
            'default' => array(
                'font-family' => rt_var('font-primary'),
                'color' => rt_var('color-primary'),
                'variant' => rt_var('font-weight'),
                'text-transform' => 'none',
            ),
            'output' => array(
                array(
                    'element' => 'h1, h2, h3, h4, h5, h6,
                                .woocommerce ul.cart_list li .product-title,
                                .woocommerce ul.product_list_widget li .product-title,
                                .woocommerce ul.product_list_widget li a,
                                label,
                                .woocommerce div.product .woocommerce-tabs ul.tabs li',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field_responsive(array(
            'type' => 'typography',
            'settings' => 'typography_regular',
            'label' => __('Body', RT_THEME_DOMAIN),
            'section' => 'general_typography_section',
            'default' => array(
                'font-size' => rt_var('font-size-body'),
                'line-height' => rt_var('line-height-body'),
                'font-family' => rt_var('font-second'),
                'color' => rt_var('second-second'),
                'text-transform' => 'none',
            ),
            'output' => array(
                array(
                    'element' => 'html',
                ),
            ),
            'transport' => 'auto',
        ));

    }

    public function add_color()
    {
        $section = 'general_color_section';

        $this->add_header(array(
            'label' => 'Global',
            'settings' => 'global_color',
            'section' => $section,
            'class' => 'header_main',
        ));
        $this->add_field(array(
            'label' => 'Brand Color',
            'settings' => 'global_brand_color',
            'type' => 'multicolor',
            'section' => $section,
            'choices' => [
                'primary' => __('Primary', RT_THEME_DOMAIN),
                'secondary' => __('Secondary', RT_THEME_DOMAIN),

            ],
            'default' => [
                'primary' => '',
                'secondary' => '',
            ],
            'output' => array(
                array(
                    'choice' => 'primary',
                    'element' => '.rt-comment-list__reply:hover,
                                 .woocommerce-MyAccount-navigation li a:hover,
                                 .woocommerce-MyAccount-navigation li.is-active a:hover,
                                 .rt-pagination .page-numbers:hover, .rt-pagination__item:hover,
                                 .rt-pagination .page-numbers.current, .rt-pagination__item.current,
                                 .rt-header .rt-menu--horizontal .rt-menu__item a::before,
                                 .rt-badges a, .rt-post .rt-badges a, .rt-single__badges a,
                                 .rt-btn--primary,
                                 .rt-gotop:hover,
                                 .woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
                                 .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
                                 .product-category:hover .woocommerce-loop-category__title',
                    'property' => 'background-color',
                ),
                array(
                    'choice' => 'primary',
                    'element' => '.rt-pagination .page-numbers:hover, .rt-pagination__item:hover,
                                  .rt-pagination .page-numbers.current, .rt-pagination__item.current,
                                  .rt-btn--border:hover, .rt-btn--border:focus,
                                  .rt-header-block__nav a:hover,
                                  .rt-product--frame-1:hover',
                    'property' => 'border-color',
                ),
                array(
                    'choice' => 'primary',
                    'element' => '.rt-header .rt-header__html a:hover,
                                    .rt-header .rt-header__html a:active,
                                    .rt-header .rt-header__html a:focus,

                                    .rt-header .rt-header__element i:hover,
                                    .rt-header .rt-header__element i:active,
                                    .rt-header .rt-header__element i:focus,

                                    .rt-header .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > a,
                                    .rt-header .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > .rt-menu__arrow i,

                                    .rt-header .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-parent > a,
                                    .rt-header .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-parent > .rt-menu__arrow i,

                                    .rt-header .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > a,
                                    .rt-header .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > .rt-menu__arrow.is-active i,

                                    .rt-header .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > a,
                                    .rt-header .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > .rt-menu__arrow.is-active i,
                                    .rt-btn--border:hover, .rt-btn--border:focus,
                                    .rt-header-block__nav a:hover',
                    'property' => 'color',
                ),

                array(
                    'choice' => 'secondary',
                    'element' => $this->selector('.rt-badges a, .rt-post .rt-badges a, .rt-single__badges a, .rt-gotop, .rt-btn--primary'),
                    'property' => 'background-color',
                ),

                // css var
                array(
                    'choice' => 'primary',
                    'element' => ':root',
                    'property' => '--theme-color-brand',
                ),
                array(
                    'choice' => 'secondary',
                    'element' => ':root',
                    'property' => '--theme-color-brand-second',
                ),

            ),
            'transport' => 'auto',
        ));

        $this->add_field(array(
            'label' => 'Link',
            'settings' => 'color_link',
            'type' => 'multicolor',
            'section' => $section,
            'choices' => [
                'normal' => __('Normal', RT_THEME_DOMAIN),
                'hover' => __('Hover', RT_THEME_DOMAIN),

            ],
            'default' => [
                'normal' => '',
                'hover' => '',
            ],
            'output' => array(
                array(
                    'element' => 'a,
                                .woocommerce-checkout
                                .showlogin',
                    'property' => 'color',
                    'choice' => 'normal',
                ),
                array(
                    'element' => $this->selector('a, .rt-breadcrumbs a'),
                    'property' => 'color',
                    'choice' => 'hover',
                ),

                // link important
                array(
                    'element' => '.rt-widget a:hover, .rt-post__title a:hover, .rt-post__readmore:hover',
                    'property' => 'color',
                    'choice' => 'hover',
                    'suffix' => '!important',
                ),

                // css var
                array(
                    'choice' => 'normal',
                    'element' => ':root',
                    'property' => '--theme-link',
                ),
                array(
                    'choice' => 'second',
                    'element' => ':root',
                    'property' => '--theme-link-hover',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field(array(
            'label' => __('Grays Scheme', RT_THEME_DOMAIN),
            'settings' => 'global_gray_scheme',
            'section' => $section,
            'type' => 'multicolor',
            'choices' => [
                'primary' => __('Primary', RT_THEME_DOMAIN),
                'secondary' => __('Secondary', RT_THEME_DOMAIN),

            ],
            'default' => [
                'primary' => '',
                'secondary' => '',
            ],
            'output' => array(
                array(
                    'choice' => 'primary',
                    'element' => '.page-header,
                                 .rt-qty span,
                                 .woocommerce-checkout-review-order,
                                 .rt-modal__header,
                                 .rt-cart__header,
                                 .rt-cart__footer,
                                 .cart-collaterals,
                                 .shop_table thead, .woocommerce-product-attributes thead,
                                 .rt-panel__header,
                                 .rt-tab__title,
                                 .woocommerce-Reviews #review_form,
                                 .rt-header__topbar,
                                 .woocommerce-MyAccount-navigation li.is-active a,
                                 .rt-mini-cart__empty i,
                                 .rt-accordion__title,
                                 .rt-header-block__nav a.is-disable,
                                 .single-product-summary .wbs-user-prices',
                    'property' => 'background-color',

                ),

                // border
                array(
                    'choice' => 'secondary',
                    'element' => '.page-header,
                                .rt-widget--aside,
                                .rt-widget__header,
                                .page-numbers,
                                .rt-header-mobile__main,
                                .rt-header__main,
                                .rt-header__middle,
                                .rt-header__topbar,
                                .widget_archive li, .widget_categories li, .widget_list li, .widget_meta li, .widget_nav_menu li, .widget_pages li, .widget_product_categories li, .widget_rss li, .woocommerce-widget-layered-nav li,
                                .product_meta,
                                .product_meta, .woocommerce-tabs,
                                .rt-socmed--border .rt-socmed__item,
                                .product_meta, .woocommerce-tabs,
                                .fieldset, select,
                                #add_payment_method table.cart td.actions .coupon .input-text, .woocommerce .input-text, .woocommerce .search-field, .woocommerce-cart table.cart td.actions .coupon .input-text, .woocommerce-checkout table.cart td.actions .coupon .input-text,
                                .rt-qty span,
                                .rt-cart,
                                .cart-collaterals,
                                .cart-collaterals .shop_table,
                                .shop_table tr, .woocommerce-product-attributes tr,
                                .rt-cart__header,
                                .rt-cart__footer,
                                .rt-form__input, .rt-search__input,
                                .woocommerce-checkout-review-order,
                                .shop_table, .woocommerce-product-attributes,
                                .shop_table tr, .woocommerce-product-attributes tr,
                                .ptf-header,
                                .ptf-footer,
                                .rt-header-checkout, .rt-list--bordered li, .rt-modal__header, .rt-panel__header,
                                .rt-coupon-modal-action,
                                .rt-widget--aside .rt-widget__header,
                                .woocommerce-MyAccount-navigation ul,
                                .woocommerce-MyAccount-navigation li,
                                .rt-panel,
                                .rt-tab__title,
                                .rt-tab__body,
                                .rt-tab__nav li:last-child .rt-tab__title,
                                .rt-pagination .page-numbers, .rt-pagination__item,
                                .woocommerce div.product .woocommerce-tabs ul.tabs::before,
                                .woocommerce-Reviews #review_form,
                                .woocommerce .woocommerce-Reviews input, .woocommerce .woocommerce-Reviews textarea,
                                .woocommerce .widget_shopping_cart .total, .woocommerce.widget_shopping_cart .total,
                                .rt-btn--border,
                                .woocommerce a.remove,
                                .rt-accordion__title,
                                .rt-header-block__nav a,
                                .single-product-summary .wbs-user-prices,
                                .rt-product--frame-1',
                    'property' => 'border-color',

                ),
                array(
                    'element' => '.select2-container--default .select2-selection--single',
                    'property' => 'border-color',
                    'suffix' => '!important',
                ),
                array(
                    'element' => '.rt-qty span:hover,
                                .rt-socmed--border .rt-socmed__item:hover',
                    'property' => 'background-color',
                ),

                // css var
                array(
                    'choice' => 'primary',
                    'element' => ':root',
                    'property' => '--theme-gray-light',
                ),
                array(
                    'choice' => 'secondary',
                    'element' => ':root',
                    'property' => '--theme-border-color',
                ),
            ),
            'transport' => 'auto',
        ));

        // $this->add_field_background(array(
        //     'label' => 'Background Color',
        //     'settings' => 'global_background_color',
        //     'section' => $section,
        //     'element' => '.page-wrapper',
        // ));

    }

    public function add_breadcrumbs()
    {
        $section = 'general_breadcrumbs_section';

        $this->add_header(array(
            'label' => 'Breadcrumbs',
            'settings' => 'page_breadcrumbs',
            'section' => $section,
            'class' => 'breadcrumbs',
        ));

        $this->add_field_link(array(
            'settings' => 'page_breadcrumbs_link',
            'section' => $section,
            'pseudo' => 'hover',
            'choices' => [
                'normal' => __('Normal', RT_THEME_DOMAIN),
                'hover' => __('Hover', RT_THEME_DOMAIN),
                'active' => __('Active', RT_THEME_DOMAIN),

            ],
            'default' => [
                'normal' => '',
                'hover' => '',
                'active' => '',
            ],
            'output' => array(
                array(
                    'element' => '.page-header .rt-breadcrumbs a, .page-header .rt-breadcrumbs li::after',
                    'property' => 'color',
                    'choice' => 'normal',
                ),
                array(
                    'element' => '.page-header .rt-breadcrumbs a:hover',
                    'property' => 'color',
                    'choice' => 'hover',
                ),

                array(
                    'element' => '.page-header .rt-breadcrumbs',
                    'property' => 'color',
                    'choice' => 'active',
                ),

            ),
        ));

    }

    public function add_topup()
    {

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'nav_gotop',
            'label' => __('Enable Top Up', RT_THEME_DOMAIN),
            'section' => 'general_topup_section',
            'default' => true,
        ));

        $this->add_field_color(array(
            'settings' => 'topup_color',
            'section' => 'general_topup_section',
            'element' => '.rt-gotop',
            'pseudo' => 'hover',
        ));

        $this->add_field_background(array(
            'settings' => 'topup_background',
            'section' => 'general_topup_section',
            'element' => '.rt-gotop',
            'pseudo' => 'hover',
        ));

        $this->add_field_border_radius(array(
            'settings' => 'topup_border_radius',
            'section' => 'general_topup_section',
            'element' => '.rt-gotop',
        ));
    }

    public function add_pagination_number()
    {

        if (rt_is_premium()) {
            $this->add_header(array(
                'label' => 'Number',
                'settings' => 'pagination_number',
                'section' => 'general_pagination_section',
                'class' => 'general_pagination',
            ));

            $this->add_field_color(array(
                'settings' => 'pagination_number_color',
                'section' => 'general_pagination_section',
                'pseudo' => 'hover',
                'output' => array(
                    array(
                        'element' => '.rt-pagination .page-numbers',
                        'property' => 'color',
                        'choice' => 'normal',
                    ),
                    array(
                        'element' => '.rt-pagination .page-numbers:hover, .rt-pagination .page-numbers.current',
                        'property' => 'color',
                        'choice' => 'hover',
                    ),

                ),

            ));

            $this->add_field_background(array(
                'settings' => 'general_pagination_number_background',
                'section' => 'general_pagination_section',
                'class' => 'general_pagination',

                'pseudo' => 'hover',
                'output' => array(
                    array(
                        'element' => '.rt-pagination .page-numbers, .rt-pagination__item',
                        'property' => 'background-color',
                        'choice' => 'normal',
                    ),
                    array(
                        'element' => '.rt-pagination .page-numbers:hover, .rt-pagination .page-numbers.current',
                        'property' => 'background-color',
                        'choice' => 'hover',
                    ),

                ),
            ));

            $this->add_field_border_color(array(
                'settings' => 'pagination_number_border_color',
                'section' => 'general_pagination_section',

                'pseudo' => 'hover',
                'output' => array(
                    array(
                        'element' => '.rt-pagination .page-numbers, .rt-pagination__item',
                        'property' => 'border-color',
                        'choice' => 'normal',
                    ),
                    array(
                        'element' => '.rt-pagination .page-numbers:hover, .rt-pagination .page-numbers.current',
                        'property' => 'border-color',
                        'choice' => 'hover',
                    ),

                ),
            ));

            $this->add_field_border_radius(array(
                'settings' => 'pagination_number_border_radius',
                'section' => 'general_pagination_section',
                'class' => 'general_pagination',
                'element' => '.rt-pagination .page-numbers',
            ));

        }
    }

    public function add_pagination_loadmore()
    {
        if (rt_is_premium()) {
            $this->add_header(array(
                'label' => 'Load More',
                'settings' => 'pagination_loadmore',
                'section' => 'general_pagination_section',
                'class' => 'general_pagination_loadmore',
            ));

            $this->add_field_button(array(
                'settings' => 'pagination_loadmore',
                'section' => 'general_pagination_section',
                'class' => 'general_pagination_loadmore',
                'element' => '.rt-pagination--loadmore .rt-pagination__button,
                         .at-pagination--loadmore .at-pagination__button',
            ));

        }
    }

    /**
     * style button for theme button, ninja form, and contact form 7
     * @return void
     */
    public function add_button_primary()
    {
        $this->add_header(array(
            'label' => 'Button Primary',
            'settings' => 'button_primary',
            'section' => 'general_button_section',
            'class' => 'button_primary',
        ));

        $this->add_field_button(array(
            'settings' => 'button_primary',
            'section' => 'general_button_section',
            'class' => 'button_primary',
            'element' => '.rt-btn--primary,
                        .woocommerce .button,
                        .woocommerce-widget-layered-nav-dropdown__submit,
                        input.rt-btn.rt-btn--primary,
                        .woocommerce-Reviews input.rt-btn.rt-btn--primary',
        ));

    }

    /**
     * style button for theme button, ninja form, and contact form 7
     * @return void
     */
    public function add_button_second()
    {
        $this->add_header(array(
            'label' => 'Button Second',
            'settings' => 'button_second',
            'section' => 'general_button_section',
            'class' => 'button_second',
        ));

        $this->add_field_button(array(
            'settings' => 'button_second',
            'section' => 'general_button_section',
            'class' => 'button_second',
            'element' => '.rt-btn--second
                        .woocommerce-cart .coupon .button,
                        .woocommerce-mini-cart__buttons .button:first-child',
        ));
    }

    public function add_button_action()
    {
        if (rt_is_premium()) {
            $this->add_header(array(
                'label' => 'Button Action',
                'settings' => 'button_action',
                'section' => 'general_button_section',
                'class' => 'button_action',
            ));

            $this->add_field_button(array(
                'settings' => 'button_action',
                'section' => 'general_button_section',
                'class' => 'button_action',
                'element' => '.rt-btn--action,
                        .woocommerce button.button.single_add_to_cart_button,
                        .woocommerce .wsb-sticky-product__action .button,
                        .woocommerce .checkout-button,
                        .woocommerce #place_order,
                        .woocommerce-mini-cart__buttons .checkout',
            ));

        }
    }

    public function add_button_whatsapp()
    {
        $this->add_header(array(
            'label' => 'Button Whatsapp',
            'settings' => 'button_whatsapp',
            'section' => 'general_button_section',
            'class' => 'button_whatsapp',
        ));

        $this->add_field_button(array(
            'settings' => 'button_whatsapp',
            'section' => 'general_button_section',
            'class' => 'button_whatsapp',
            'element' => '.rt-btn--whatsapp',
        ));

    }

    public function add_slider()
    {
        $section = 'general_slider_section';

        $this->add_header(array(
            'label' => 'Navigation',
            'settings' => 'general_slider',
            'section' => $section,
        ));
        $this->add_field_color(array(
            'settings' => "general_slider_nav_color",
            'section' => $section,
            'element' => '.rt-slider .owl-next i, .rt-slider .owl-prev i',
            'pseudo' => 'hover',
            'suffix' => '!important',
        ));
        $this->add_field_background(array(
            'settings' => "general_slider_nav_background",
            'section' => $section,
            'element' => '.rt-slider .owl-next, .rt-slider .owl-prev',
            'pseudo' => 'hover',
            'suffix' => '!important',
        ));
        $this->add_field_border_radius(array(
            'label' => __('Border Radius', RT_THEME_DOMAIN),
            'settings' => "general_slider_nav_border_color",
            'section' => $section,
            'element' => '.rt-slider .owl-next, .rt-slider .owl-prev',
        ));

        $this->add_header(array(
            'label' => 'Pagination',
            'settings' => 'general_slider_pagination',
            'section' => $section,
        ));

        $this->add_field(array(
            'label' => __('Background Color', RT_THEME_DOMAIN),
            'settings' => 'general_slider_pagination',
            'section' => $section,
            'type' => 'multicolor',
            'choices' => [
                'normal' => __('Normal', RT_THEME_DOMAIN),
                'hover' => __('Hover', RT_THEME_DOMAIN),

            ],
            'default' => [
                'normal' => '',
                'hover' => '',
            ],
            'output' => array(
                array(
                    'element' => '.rt-slider .owl-dots .owl-dot span',
                    'property' => 'background-color',
                    'choice' => 'normal',
                ),
                array(
                    'element' => '.rt-slider .owl-dots .owl-dot.active span',
                    'property' => 'background-color',
                    'choice' => 'hover',
                ),

            ),
            'transport' => 'auto',
        ));
    }

// end class
}

new General;
