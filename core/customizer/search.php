<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Search extends Customizer_Base
{

    public function __construct()
    {

        $this->set_section();

        $this->add_search_option();
    }

    public function set_section()
    {
        $this->add_section('', array(
            'search_option' => array(esc_attr__('Search ', RT_THEME_DOMAIN)),
        ));
    }

    public function add_search_option()
    {
        $section = 'search_option_section';
        $settings = 'search_options';

        $this->add_field(array(
            'type' => 'select',
            'section' => $section,
            'settings' => 'search_options_result',
            'label' => __('Post Types Result', RT_THEME_DOMAIN),
            'default' => 'post',
            'multiple' => 1,
            'choices' => apply_filters('rt_post_type_search_result', array(
                'post' => 'Post',
                'product' => 'Product',
            )),
        ));

        $this->add_field(array(
            'type' => 'text',
            'section' => $section,
            'settings' => 'search_options_text',
            'label' => __('Placeholder', RT_THEME_DOMAIN),
            'default' => 'Type Something and enter',
        ));

        if ( rt_is_premium()) {
            $this->add_field_color(array(
                'settings' => 'search_options_color',
                'section' => $section,
                'element' => '.rt-header .rt-search-main .rt-search__input,
						 .rt-header .rt-search-main .rt-search__icon',
            ));

            $this->add_field_background(array(
                'settings' => 'search_options_background',
                'section' => $section,
                'element' => '.rt-header .rt-search-main',
            ));

            $this->add_field_border_color(array(
                'settings' => 'search_options_border',
                'section' => $section,
                'element' => '.rt-header .rt-search-main',
            ));

        }
    }

// end class
}

new Search;
