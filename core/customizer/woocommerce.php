<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class WooCommerce extends Customizer_Base
{

    public function __construct()
    {

        add_action('customize_register', array($this, 'remove_default_field'));

        $this->set_section();

        $this->add_general_option();
        $this->add_archive_option();

        $this->add_single_option();
        $this->add_single_related();

        $this->add_sale_badge();
        $this->add_star_rating();
        $this->style_product();
        $this->style_product_single();

        $this->add_checkout();


    }

    public function set_section()
    {
        $this->add_section('woocommerce', array(
            'woocommerce_general' => array(__('General', RT_THEME_DOMAIN)),
            'woocommerce_archive' => array(__('Archive', RT_THEME_DOMAIN)),
            'woocommerce_single' => array(__('Product', RT_THEME_DOMAIN)),
            'woocommerce_catalog' => array(__('Catalog', RT_THEME_DOMAIN)),
            'woocommerce_badge' => array(__('Badge Sale', RT_THEME_DOMAIN)),
            'woocommerce_rating' => array(__('Star Rating', RT_THEME_DOMAIN)),
            'woocommerce_cart' => array(__('Cart', RT_THEME_DOMAIN)),
            'woocommerce_checkout' => array(__('Checkout', RT_THEME_DOMAIN)),
            'woocommerce_my_account' => array(__('My Account', RT_THEME_DOMAIN)),
        ));

    }

    /**
     * Remove default customizer woocommerce
     *
     * @param [type] $wp_customize customizer control
     * @return void
     */
    public function remove_default_field($wp_customize)
    {
        $wp_customize->add_control('woocommerce_catalog_columns')->theme_supports = false;
        $wp_customize->add_control('woocommerce_catalog_rows')->theme_supports = false;
    }

    public function add_general_option()
    {

        $section = 'woocommerce_general_section';
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'woocommerce_ajax_mini_cart',
            'label' => __('Ajax Mini Cart', RT_THEME_DOMAIN),
            'description' => 'Enable/Disable panel cart after add to cart button on click',
            'section' => 'woocommerce_general_section',
            'default' => true,
        ));

        // remove add to cart button
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'woocommerce_catalog',
            'label' => __('Enable Catalog', RT_THEME_DOMAIN),
            'description' => 'Hide Add to Cart button from homepage and all other pages',
            'section' => 'woocommerce_general_section',
            'default' => false,
        ));

    }

    public function add_archive_option()
    {
        $section = 'woocommerce_archive_section';
        $settings = 'woocommerce_archive_options';

        $this->add_header(array(
            'label' => 'Options',
            'settings' => 'woocommerce_archive',
            'section' => 'woocommerce_archive_section',
        ));

        if (rt_is_premium()) {

            $this->add_field(array(
                'type' => 'select',
                'section' => 'woocommerce_archive_section',
                'settings' => 'woocommerce_archive_style',
                'label' => __('Style', RT_THEME_DOMAIN),
                'multiple' => 1,
                'choices' => array(
                    'style-1' => __('Style 1', RT_THEME_DOMAIN),
                    'style-2' => __('Style 2', RT_THEME_DOMAIN),
                ),

            ));

            $this->add_field(array(
                'type' => 'toggle',
                'settings' => 'woocommerce_archive_masonry',
                'label' => __('Masonry', RT_THEME_DOMAIN),
                'section' => 'woocommerce_archive_section',
                'default' => false,
            ));

        }

        $this->add_field_responsive(array(
            'type' => 'select',
            'section' => 'woocommerce_archive_section',
            'settings' => 'woocommerce_archive_options_column',
            'label' => __('Column', RT_THEME_DOMAIN),
            'default' => 3,
            'multiple' => 1,
            'choices' => array(
                1 => __('1 Column', RT_THEME_DOMAIN),
                2 => __('2 Column', RT_THEME_DOMAIN),
                3 => __('3 Column', RT_THEME_DOMAIN),
                4 => __('4 Column', RT_THEME_DOMAIN),
                6 => __('6 Column', RT_THEME_DOMAIN),
            ),

        ));

        $this->add_field_responsive(array(
            'type' => 'slider',
            'settings' => $settings . '_gap',
            'label' => __('Column Gap', RT_THEME_DOMAIN),
            'section' => 'woocommerce_archive_section',
            'default' => 30,
            'choices' => array(
                'min' => 0,
                'max' => 30,
            ),
            'output' => array(
                array(
                    'element' => '.rt-product-archive',
                    'property' => 'margin-left',
                    'value_pattern' => 'calc(-$px/2)',
                ),
                array(
                    'element' => '.rt-product-archive',
                    'property' => 'margin-right',
                    'value_pattern' => 'calc(-$px/2)',
                ),
                array(
                    'element' => '.rt-product-archive .flex-item',
                    'property' => 'padding-left',
                    'value_pattern' => 'calc($px/2)',
                ),
                array(
                    'element' => '.rt-product-archive .flex-item',
                    'property' => 'padding-right',
                    'value_pattern' => 'calc($px/2)',
                ),
                array(
                    'element' => '.rt-product-archive .flex-item',
                    'units' => 'px',
                    'property' => 'margin-bottom',
                ),
            ),
            'transport' => 'auto',

        ));

        $this->add_field_responsive(array(
            'type' => 'number',
            'section' => $section,
            'settings' => 'woocommerce_archive_options_post_per_page',
            'label' => __('Post Per Page', RT_THEME_DOMAIN),
            'default' => 9,
            'choices' => array(
                'min' => 1,
                'max' => 20,
            ),

        ));

        if (rt_is_premium()) {
            $this->add_field(array(
                'type' => 'select',
                'settings' => $settings . '_pagination',
                'label' => __('Pagination', RT_THEME_DOMAIN),
                'section' => $section,
                'default' => 'number',
                'multiple' => 1,
                'choices' => array(
                    'none' => __('None', RT_THEME_DOMAIN),
                    'number' => __('Number', RT_THEME_DOMAIN),
                    'loadmore' => __('Load More', RT_THEME_DOMAIN),
                ),
            ));
        }

    }

    public function add_single_option()
    {
        $section = 'woocommerce_single_section';

        $this->add_header(array(
            'label' => 'Options',
            'settings' => 'woocommerce_single_options',
            'section' => $section,
            'class' => 'woocommerce_single',
        ));

        if (rt_is_premium()) {
            $this->add_field(array(
                'type' => 'select',
                'settings' => 'woocommerce_single_layout',
                'label' => __('Layout', RT_THEME_DOMAIN),
                'section' => $section,
                'default' => 'buttom-gallery',
                'choices' => array(
                    'left-gallery' => __('Left Gallery', RT_THEME_DOMAIN),
                    'minimalis' => __('Minimalis', RT_THEME_DOMAIN),
                    'buttom-gallery' => __('Buttom Gallery', RT_THEME_DOMAIN),
                ),
            ));

        }
        $this->add_field(array(
            'type' => 'toggle',
            'label' => 'Product Meta',
            'settings' => 'woocommerce_single_meta',
            'section' => $section,
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'label' => 'Share Button',
            'settings' => 'woocommerce_single_share',
            'section' => $section,
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'label' => 'Related Product',
            'settings' => 'woocommerce_single_product_related',
            'section' => $section,
            'default' => true,
        ));

    }

    public function add_single_related()
    {
        $section = 'woocommerce_single_section';
        $settings = 'woocommerce_single_related';

        $this->add_header(array(
            'label' => 'Related Product',
            'settings' => 'woocommerce_single_related',
            'section' => $section,
            'active_callback' => array(
                array(
                    'setting' => 'woocommerce_single_product_related',
                    'operator' => '==',
                    'value' => true,
                ),
            ),
        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => $settings . '_count',
            'label' => __('Related Count', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => 4,
            'multiple' => 1,
            'choices' => array(
                2 => __('2', RT_THEME_DOMAIN),
                3 => __('3', RT_THEME_DOMAIN),
                4 => __('4', RT_THEME_DOMAIN),
                6 => __('6', RT_THEME_DOMAIN),
            ),
            'active_callback' => array(
                array(
                    'setting' => 'woocommerce_single_product_related',
                    'operator' => '==',
                    'value' => true,
                ),
            ),
        ));

        $this->add_field_responsive(array(
            'type' => 'select',
            'settings' => $settings . '_show',
            'label' => __('Slider Show', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => 4,
            'multiple' => 1,
            'choices' => array(
                1 => __('1', RT_THEME_DOMAIN),
                2 => __('2', RT_THEME_DOMAIN),
                3 => __('3', RT_THEME_DOMAIN),
                4 => __('4', RT_THEME_DOMAIN),
                6 => __('6', RT_THEME_DOMAIN),
            ),
            'active_callback' => array(
                array(
                    'setting' => 'woocommerce_single_product_related',
                    'operator' => '==',
                    'value' => true,
                ),
            ),
        ));
    }

/**
 * sale badge in image thumbnail
 *
 * @return void
 */
    public function add_sale_badge()
    {
        if (rt_is_premium()) {
            $this->add_field_color(array(
                'settings' => 'woocommerce_sale',
                'section' => 'woocommerce_badge_section',
                'element' => '.woocommerce span.onsale',
            ));

            $this->add_field_background(array(
                'settings' => 'woocommerce_sale_background',
                'section' => 'woocommerce_badge_section',
                'element' => '.woocommerce span.onsale',
            ));

            $this->add_field_border_color(array(
                'settings' => 'woocommerce_sale_border_color',
                'section' => 'woocommerce_badge_section',
                'element' => '.woocommerce span.onsale',
            ));

        }
    }

    public function add_star_rating()
    {
        if (rt_is_premium()) {
            $this->add_field_color(array(
                'settings' => 'woocommerce_rating',
                'section' => 'woocommerce_rating_section',
                'element' => '.woocommerce .rt-product .star-rating span::before,
                        .woocommerce .rt-product .star-rating::before,
                        .woocommerce .products .product_item .star-rating,
                        .woocommerce.products .product_item .star-rating,
                        .single-product.woocommerce .star-rating span,
                        .star-rating',
            ));

        }
    }

    public function style_product()
    {
        $section = 'woocommerce_archive_section';

        $this->add_header(array(
            'label' => 'General Style',
            'settings' => 'style_shop_box',
            'section' => $section,
        ));

        $this->add_field_background(array(
            'settings' => 'style_shop_box_background',
            'section' => $section,
            'element' => '.rt-product',
        ));
        $this->add_field(array(
            'settings' => 'style_shop_box_border_width',
            'type' => 'slider',
            'label' => __('Border Width', RT_THEME_DOMAIN),
            'section' => $section,
            'choices' => array(
                'min' => '0',
                'max' => '10',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-product',
                    'property' => 'border',
                    'value_pattern' => '$px solid transparent',
                ),

            ),
            'transport' => 'auto',
        ));
        $this->add_field_border_color(array(
            'settings' => 'style_shop_box_border_color',
            'section' => $section,
            'element' => '.rt-product',
            'suffix' => '!important',
            'pseudo' => 'hover',
        ));
        $this->add_field_border_radius(array(
            'settings' => 'style_shop_box_border_radius',
            'section' => $section,
            'element' => '.rt-product',
        ));

        $this->add_field(array(
            'type' => 'radio-buttonset',
            'settings' => 'style_shop_box_align',
            'label' => __('Text Align', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => 'center',
            'choices' => array(
                'left' => __('Left', RT_THEME_DOMAIN),
                'center' => __('Center', RT_THEME_DOMAIN),
                'right' => __('Right', RT_THEME_DOMAIN),
            ),
            'output' => array(
                array(
                    'element' => '.rt-product__body',
                    'property' => 'text-align',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field_padding(array(
            'settings' => 'style_shop_box_padding',
            'section' => $section,
            'element' => '.rt-product',
        ));

        $this->add_field_padding(array(
            'label' => __('Padding Body', RT_THEME_DOMAIN),
            'settings' => 'style_shop_body_padding',
            'section' => $section,
            'element' => '.rt-product__body',
        ));

        //Title
        $this->add_header(array(
            'label' => 'Title',
            'settings' => 'style_shop_text',
            'section' => $section,
        ));

        $this->add_field(array(
            'type' => 'typography',
            'label' => __('Title', RT_THEME_DOMAIN),
            'settings' => 'style_shop_title_typography',
            'section' => $section,
            'default' => [
                'font-family' => '',
                'variant' => '',
                'font-size' => '',
                'letter-spacing' => '',
                'text-transform' => '',
            ],
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.rt-product .rt-product__title,
                                    .woocommerce-loop-category__title',
                ],
            ],
        ));

        $this->add_field_color(array(
            'settings' => 'style_shop_title_color',
            'section' => $section,
            'element' => '.rt-product .rt-product__title a,
                                    .woocommerce-loop-category__title a',
            'pseudo' => 'hover',
        ));

        $this->add_field(array(
            'settings' => 'style_shop_title_spacing',
            'type' => 'slider',
            'label' => __('Spacing', RT_THEME_DOMAIN),
            'section' => $section,
            'choices' => array(
                'min' => '1',
                'max' => '30',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-product .rt-product__title',
                    'property' => 'margin-bottom',
                    'units' => 'px',
                ),
            ),
            'transport' => 'auto',
        ));

        //Price
        $this->add_header(array(
            'label' => 'Price',
            'settings' => 'style_shop_price',
            'section' => $section,
        ));

        $this->add_field(array(
            'type' => 'typography',
            'label' => __('Price', RT_THEME_DOMAIN),
            'settings' => 'style_shop_price_typography',
            'section' => $section,
            'default' => [
                'font-family' => '',
                'variant' => '',
                'font-size' => '',
                'letter-spacing' => '',
                'text-transform' => '',
                'color' => '',
            ],
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.rt-product .price',
                    'suffix' => '!important',
                ],
            ],
        ));

        $this->add_field(array(
            'type' => 'typography',
            'label' => __('Price Sale', RT_THEME_DOMAIN),
            'settings' => 'style_shop_price_sale_typography',
            'section' => $section,
            'default' => [
                'font-family' => '',
                'variant' => '',
                'font-size' => '',
                'letter-spacing' => '',
                'text-transform' => '',
                'color' => '',
            ],
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.rt-product del .price, .rt-product .price del',
                    'suffix' => '!important',
                ],
            ],
        ));

        //rating
        $this->add_header(array(
            'label' => 'Rating',
            'settings' => 'style_shop_rating',
            'section' => $section,
        ));

        $this->add_field(array(
            'settings' => 'style_shop_rating_spacing',
            'type' => 'slider',
            'label' => __('Spacing', RT_THEME_DOMAIN),
            'section' => $section,
            'choices' => array(
                'min' => '1',
                'max' => '30',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.woocommerce .products .rt-product .star-rating,
                                .woocommerce.products .rt-product .star-rating',
                    'property' => 'margin-bottom',
                    'units' => 'px',
                ),
            ),
            'transport' => 'auto',
        ));

        //button
        $this->add_header(array(
            'label' => 'Add To Cart Button',
            'settings' => 'style_shop_add_to_cart',
            'section' => $section,

        ));

        $this->add_field_button(array(
            'settings' => 'style_shop_add_to_cart',
            'section' => $section,
            'element' => ".rt-product .button,
                         .woocommerce .rt-product.rt-product--overlay-btn .rt-product__thumbnail a.button",
        ));

    }

    public function style_product_single()
    {
        $section = 'woocommerce_single_section';
        //Price
        $this->add_header(array(
            'label' => 'Title',
            'settings' => 'style_product_title_typography',
            'section' => $section,
        ));
        $this->add_field(array(
            'type' => 'typography',
            'label' => __('Title', RT_THEME_DOMAIN),
            'settings' => 'style_product_title_typography',
            'section' => $section,
            'default' => [
                'font-family' => '',
                'variant' => '',
                'font-size' => '',
                'letter-spacing' => '',
                'text-transform' => '',
                'color' => '',
            ],
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.single-product-summary .product_title',
                ],
            ],
        ));
        //Price
        $this->add_header(array(
            'label' => 'Price',
            'settings' => 'style_product_price',
            'section' => $section,
        ));

        $this->add_field(array(
            'type' => 'typography',
            'label' => __('Price', RT_THEME_DOMAIN),
            'settings' => 'style_product_price_typography',
            'section' => $section,
            'default' => [
                'font-family' => '',
                'variant' => '',
                'font-size' => '',
                'letter-spacing' => '',
                'text-transform' => '',
                'color' => '',
            ],
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.single-product-summary > p.price',
                    'suffix' => '!important',
                ],
            ],
        ));

        $this->add_field(array(
            'type' => 'typography',
            'label' => __('Price Sale', RT_THEME_DOMAIN),
            'settings' => 'style_product_price_sale_typography',
            'section' => $section,
            'default' => [
                'font-family' => '',
                'variant' => '',
                'font-size' => '',
                'letter-spacing' => '',
                'text-transform' => '',
                'color' => '',
            ],
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.single-product-summary > .price del',
                    'suffix' => '!important',
                ],
            ],
        ));
    }

    public function add_cart()
    {
        $section = 'woocommerce_cart_section';

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_cart_behavior',
            'section' => $section,
            'label' => __('Add to cart behaviour', RT_THEME_DOMAIN),
            'default' => 'addtocart_panel',
            'priority' => 1,
            'choices' => array(
                'ajax_addtocart_panel' => 'Ajax Add To Cart',
                'redirect_checkout' => 'Redirect to checkout Page',
                'catalog' => 'Catalog (without button)',
                'redirect_whatsapp' => 'WhatsApp Catalog',
            ),
        ));
    }

    public function add_checkout()
    {
        $section = 'woocommerce_checkout';

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_name',
            'section' => $section,
            'label' => __('Name', RT_THEME_DOMAIN),
            'default' => 'fullname',
            'priority' => 1,
            'choices' => array(
                'fullname' => 'Full Name',
                'dual-name' => 'First Name & Last Name',
            ),
        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_email',
            'section' => $section,
            'label' => __('Email', RT_THEME_DOMAIN),
            'default' => 'required',
            'priority' => 1,
            'choices' => array(
                'hidden' => 'Hidden',
                'optional' => 'Optional',
                'required' => 'Required',
            ),
        ));
        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_postcode',
            'section' => $section,
            'label' => __('Post Code', RT_THEME_DOMAIN),
            'default' => 'hidden',
            'priority' => 1,
            'choices' => array(
                'hidden' => 'Hidden',
                'optional' => 'Optional',
                'required' => 'Required',
            ),
        ));
        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_notes',
            'section' => $section,
            'label' => __('Notes', RT_THEME_DOMAIN),
            'default' => 'optional',
            'priority' => 2,
            'choices' => array(
                'hidden' => 'Hidden',
                'optional' => 'Optional',
            ),
        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_country',
            'section' => $section,
            'label' => __('Country', RT_THEME_DOMAIN),
            'priority' => 7,
            'default' => 'hidden',
            'choices' => array(
                'hidden' => 'Hidden',
                'required' => 'Required',
            ),
        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_state',
            'section' => $section,
            'label' => __('Province', RT_THEME_DOMAIN),
            'priority' => 7,
            'default' => 'required',
            'choices' => array(
                'hidden' => 'Hidden',
                'optional' => 'Optional',
                'required' => 'Required',
            ),
        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_city',
            'section' => $section,
            'label' => __('City', RT_THEME_DOMAIN),
            'priority' => 8,
            'default' => 'required',
            'choices' => array(
                'hidden' => 'Hidden',
                'optional' => 'Optional',
                'required' => 'Required',
            ),
        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_address_1',
            'section' => $section,
            'label' => __('Address line 1 field', RT_THEME_DOMAIN),
            'priority' => 9,
            'choices' => array(
                'hidden' => 'Hidden',
                'optional' => 'Optional',
                'required' => 'Required',
            ),
        ));

    }
// end class
}

new WooCommerce;
