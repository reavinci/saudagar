<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;
use Retheme\Helper;

class Connect extends Customizer_Base
{

    public function __construct()
    {
        $this->set_panel();
        $this->set_section();

        $this->add_socmed();
        $this->add_share();

    }

    public function set_panel()
    {
        $this->add_panel('connect_panel', array(
            'title' => __('Connect', RT_THEME_DOMAIN),
        ));
    }

    public function set_section()
    {
        $this->add_section('connect_panel', array(
            'social' => array(esc_attr__('Social Media', RT_THEME_DOMAIN)),
            'share' => array(esc_attr__('Share', RT_THEME_DOMAIN)),
        ));
    }

    public function add_socmed()
    {
        $section = 'social_section';

        $this->add_field(array(
            'type' => 'repeater',
            'settings' => 'social_item',
            'label' => __('Social Media', RT_THEME_DOMAIN),
            'section' => $section,
            'row_label' => array(
                'type' => 'field',
                'value' => __('Your Social Media', RT_THEME_DOMAIN),
                'field' => 'link_text',
            ),

            'fields' => array(
                'link_text' => array(
                    'type' => 'select',
                    'label' => __('Social Media', RT_THEME_DOMAIN),
                    'default' => 'facebook',
                    'choices' => Helper::get_social_media(),
                ),
                'link_url' => array(
                    'type' => 'text',
                    'label' => __('Link URL', RT_THEME_DOMAIN),
                ),
            ),
        ));

    }

    public function add_share()
    {
        $section = 'share_section';

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'connect_share_facebook',
            'label' => __('Facebook', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => true,
        ));
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'connect_share_twitter',
            'label' => __('Twitter', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'connect_share_pinterest',
            'label' => __('Pinterest', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'connect_share_email',
            'label' => __('Email', RT_THEME_DOMAIN),
            'section' => $section,
            'default' => true,
        ));

    }

// end class
}

new Connect;
