<?php

namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Header extends Customizer_Base
{
    public function __construct()
    {
        $this->set_panel();
        $this->set_section();

        $this->add_header_builder();

        /** desktop options */
        $this->add_options_topbar();
        $this->add_options_middle();
        $this->add_options_main();
        $this->add_options_sticky();

        /** Main menu */
        $this->add_main_menu();
        $this->add_main_submenu();

        /** Mobile menu */
        $this->add_mobile_header();
        $this->add_mobile_header_sticky();
        $this->add_drawer_menu();
        $this->add_drawer_search();

        /** Header Element */
        $this->add_element_social();
        $this->add_element_search_form();
        $this->add_element_button();
        $this->add_element_html();

        // icon
        $this->add_cart_icon();
        $this->add_user_icon();
        $this->add_search_icon();

    }

    public function set_panel()
    {
        $this->add_panel('header_panel', array(
            'title' => __('Header', RT_THEME_DOMAIN),
        ));
    }

    public function set_section()
    {

        $this->add_section('header_panel', array(
            'header_desktop' => array(__('Desktop Options', RT_THEME_DOMAIN)),
            'header_mobile' => array(__('Mobile Options', RT_THEME_DOMAIN)),
            'header_overlay' => array(__('Header Overlay', RT_THEME_DOMAIN)),

            'header_main_menu' => array(__('Main Menu', RT_THEME_DOMAIN)),
            'header_mobile_menu' => array(__('Mobile Menu', RT_THEME_DOMAIN)),

            'header_socmed' => array(__('Social Media', RT_THEME_DOMAIN)),
            'header_button' => array(__('Button', RT_THEME_DOMAIN)),
            'header_html' => array(__('HTML', RT_THEME_DOMAIN)),
            'header_search' => array(__('Search Form', RT_THEME_DOMAIN)),

            'header_cart_icon' => array(__('Cart Icon', RT_THEME_DOMAIN)),
            'header_user_icon' => array(__('User Icon', RT_THEME_DOMAIN)),
            'header_search_icon' => array(__('Search Icon', RT_THEME_DOMAIN)),

            'header_builder' => array(__('Builder', RT_THEME_DOMAIN)),

        ));
    }

    public function get_header_element($partial = '')
    {
        $elements = get_theme_mod("header_builder_option")[$partial];

        if (is_array($elements)) {
            foreach ($elements as $key => $element) {
                rt_get_template_part("header/element/" . $element);
            }
        }
    }

    public function add_header_builder()
    {
        if (rt_is_premium()) {
            $elements = apply_filters('rt_builder_element', array(
                'logo' => 'Logo',
                'main-menu' => 'Main Menu',
                'second-menu' => 'Second Menu',
                'thirdty-menu' => 'Thirdty Menu',
                'top-menu' => 'Top Menu',
                'cart-icon' => 'Cart Icon',
                'user-icon' => 'User Icon',
                'search-icon' => 'Search Icon',
                'search-form' => 'Search Form',
                'social' => 'Social Media',
                'divender-1' => '|',
                'divender-2' => '|',
                'divender-3' => '|',
                'divender-4' => '|',
                'divender-5' => '|',
                'button-1' => 'Button 1',
                'button-2' => 'Button 2',
                'button-3' => 'Button 3',
                'html-1' => 'HTML 1',
                'html-2' => 'HTML 2',
                'html-3' => 'HTML 3',
                'html-4' => 'HTML 4',
                'html-5' => 'HTML 5',
            ));

            $mobile_elements = apply_filters('rt_builder_mobile_element', array(
                'logo-mobile' => 'Logo',
                'toggle-menu' => 'Toggle Menu',
                'cart-icon' => 'Cart Icon',
                'user-icon' => 'User Icon',
                'search-icon' => 'Search Icon',
                'search-form' => 'Search Form',
                'divender-1' => '|',
                'divender-2' => '|',
                'divender-3' => '|',
                'divender-4' => '|',
                'divender-5' => '|',
                'button-1' => 'Button 1',
                'button-2' => 'Button 2',
                'button-3' => 'Button 3',
                'html-1' => 'HTML 1',
                'html-2' => 'HTML 2',
                'html-3' => 'HTML 3',
                'html-4' => 'HTML 4',
                'html-5' => 'HTML 5',
            ));

            $drawer_elements = apply_filters('rt_builder_drawer', array(
                'main-menu' => 'Main Menu',
                'search-form' => 'Search Form',
                'social' => 'Social Media',
                'button-1' => 'Button 1',
                'button-2' => 'Button 2',
                'button-3' => 'Button 3',
                'html-1' => 'HTML 1',
                'html-2' => 'HTML 2',
                'html-3' => 'HTML 3',
                'html-4' => 'HTML 4',
                'html-5' => 'HTML 5',
            ));

        }

        if (rt_is_free()) {
            $elements = apply_filters('rt_builder_element', array(
                'logo' => 'Logo',
                'main-menu' => 'Main Menu',
                'cart-icon' => 'Cart Icon',
                'user-icon' => 'User Icon',
                'search-icon' => 'Search Icon',
                'social' => 'Social Media',
                'html-1' => 'HTML 1',
                'html-2' => 'HTML 2',
            ));

            $mobile_elements = apply_filters('rt_builder_mobile_element', array(
                'logo-mobile' => 'Logo',
                'toggle-menu' => 'Toggle Menu',
                'cart-icon' => 'Cart Icon',
                'user-icon' => 'User Icon',
                'search-icon' => 'Search Icon',
                'html-1' => 'HTML 1',
                'html-2' => 'HTML 2',
            ));

            $drawer_elements = apply_filters('rt_builder_drawer', array(
                'main-menu' => 'Main Menu',
                'search-form' => 'Search Form',
                'social' => 'Social Media',
                'html-1' => 'HTML 1',
                'html-2' => 'HTML 2',
            ));
        }

        $this->add_field(array(
            'label' => 'Main Bar',
            'type' => 'builder',
            'settings' => 'header_builder_option',
            'section' => 'header_builder_section',
            'default' => array(
                'topbar_left_alignment' => 'left',
                'topbar_center_alignment' => 'center',
                'topbar_right_alignment' => 'right',
                'topbar_left_display' => 'normal',
                'topbar_center_display' => 'normal',
                'topbar_right_display' => 'normal',
                'middle_left_alignment' => 'left',
                'middle_center_alignment' => 'center',
                'middle_right_alignment' => 'right',
                'middle_left_display' => 'normal',
                'middle_center_display' => 'normal',
                'middle_right_display' => 'normal',
                'main_left_alignment' => 'left',
                'main_center_alignment' => 'center',
                'main_right_alignment' => 'right',
                'main_left_display' => 'normal',
                'main_center_display' => 'normal',
                'main_right_display' => 'normal',
                'sticky_left_alignment' => 'left',
                'sticky_center_alignment' => 'center',
                'sticky_right_alignment' => 'right',
                'sticky_left_display' => 'normal',
                'sticky_center_display' => 'normal',
                'sticky_right_display' => 'normal',
                'mobile_left_alignment' => 'left',
                'mobile_center_alignment' => 'center',
                'mobile_right_alignment' => 'right',
                'mobile_left_display' => 'normal',
                'mobile_center_display' => 'normal',
                'mobile_right_display' => 'normal',

            ),

            'choices' => array(
                'normal_elements' => $elements,
                'sticky_elements' => $elements,
                'mobile_elements' => $mobile_elements,
                'drawer_elements' => $drawer_elements,

            ),
            'partial_refresh' => array(

                'header_topbar_left' => array(
                    'selector' => '#header-topbar-left',
                    'render_callback' => function () {
                        return $this->get_header_element('topbar_left_element');
                    },
                ),

                'header_topbar_center' => array(
                    'selector' => '#header-topbar-center',
                    'render_callback' => function () {
                        return $this->get_header_element('topbar_center_element');
                    },
                ),

                'header_topbar_right' => array(
                    'selector' => '#header-topbar-right',
                    'render_callback' => function () {
                        return $this->get_header_element('topbar_right_element');
                    },
                ),

                'header_middle_left' => array(
                    'selector' => '#header-middle-left',
                    'render_callback' => function () {
                        return $this->get_header_element('middle_left_element');
                    },
                ),

                'header_middle_center' => array(
                    'selector' => '#header-middle-center',
                    'render_callback' => function () {
                        return $this->get_header_element('middle_center_element');
                    },
                ),

                'header_middle_right' => array(
                    'selector' => '#header-middle-right',
                    'render_callback' => function () {
                        return $this->get_header_element('middle_right_element');
                    },
                ),

                'header_main_left' => array(
                    'selector' => '#header-main-left',
                    'render_callback' => function () {
                        return $this->get_header_element('main_left_element');
                    },
                ),

                'header_main_center' => array(
                    'selector' => '#header-main-center',
                    'render_callback' => function () {
                        return $this->get_header_element('main_center_element');
                    },
                ),

                'header_main_right' => array(
                    'selector' => '#header-main-right',
                    'render_callback' => function () {
                        return $this->get_header_element('main_right_element');
                    },
                ),

                'header_sticky_left' => array(
                    'selector' => '#header-sticky-left',
                    'render_callback' => function () {
                        return $this->get_header_element('sticky_left_element');
                    },
                ),

                'header_sticky_center' => array(
                    'selector' => '#header-sticky-center',
                    'render_callback' => function () {
                        return $this->get_header_element('sticky_center_element');
                    },
                ),

                'header_sticky_right' => array(
                    'selector' => '#header-sticky-right',
                    'render_callback' => function () {
                        return $this->get_header_element('sticky_right_element');
                    },
                ),

                'header_mobile_left' => array(
                    'selector' => '#header-mobile-left',
                    'render_callback' => function () {
                        return $this->get_header_element('mobile_left_element');
                    },
                ),

                'header_mobile_center' => array(
                    'selector' => '#header-mobile-center',
                    'render_callback' => function () {
                        return $this->get_header_element('mobile_center_element');
                    },
                ),

                'header_mobile_right' => array(
                    'selector' => '#header-mobile-right',
                    'render_callback' => function () {
                        return $this->get_header_element('mobile_right_element');
                    },
                ),

                'header_drawer' => array(
                    'selector' => '#header-mobile-drawer',
                    'render_callback' => function () {
                        return $this->get_header_element('drawer_element');
                    },
                ),

            ),

        ));
    }

    public function add_options_main()
    {
        $this->add_header(array(
            'label' => 'Main Bar',
            'settings' => 'header_main',
            'section' => 'header_desktop_section',
            'class' => 'header_main',
        ));

        $this->add_field(array(
            'settings' => 'header_main_height',
            'type' => 'slider',
            'label' => __('Height', RT_THEME_DOMAIN),
            'section' => 'header_desktop_section',
            'class' => 'header_main',
            'default' => '60',
            'choices' => array(
                'min' => '45',
                'max' => '150',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header__main',
                    'property' => 'height',
                    'units' => 'px',
                ),
                array(
                    'element' => '.rt-header__main .rt-menu--horizontal',
                    'property' => 'height',
                    'units' => 'px',
                ),
                array(
                    'element' => '.rt-header .rt-search--overlay',
                    'property' => 'height',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));
        $this->add_field_link(array(
            'settings' => 'header_main_link',
            'label' => __('Link', RT_THEME_DOMAIN),
            'section' => 'header_desktop_section',
            'pseudo' => 'hover',
            'output' => array(
                array(
                    'element' => '.rt-header__main .rt-header__html a,
                                        .rt-header__main .rt-header__element i,
                                        .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item > a,
                                        .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item > .rt-menu__arrow i,

                                        .rt-header__sticky .rt-header__html a,
                                        .rt-header__sticky .rt-header__element i,
                                        .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item > a,
                                        .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item > .rt-menu__arrow i',
                    'property' => 'color',
                    'choice' => 'normal',
                ),
                array(
                    'element' => '.rt-header__main .rt-header__html a:hover,
                                    .rt-header__main .rt-header__element i:hover,
                                    .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > a,
                                    .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > .rt-menu__arrow i,

                                    .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-parent > a,
                                    .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-parent > .rt-menu__arrow i,

                                    .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > a,
                                    .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > .rt-menu__arrow.is-active i,

                                    .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > a,
                                    .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > .rt-menu__arrow.is-active i,

                                    .rt-header__sticky .rt-header__html a:hover,
                                    .rt-header__sticky .rt-header__element i:hover,
                                    .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > a,
                                    .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > .rt-menu__arrow i,

                                    .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-parent > a,
                                    .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-parent > .rt-menu__arrow i,

                                    .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > a,
                                    .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > .rt-menu__arrow.is-active i,

                                    .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > a,
                                    .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > .rt-menu__arrow.is-active i',
                    'property' => 'color',
                    'choice' => 'hover',
                ),

            ),
            'transport' => 'auto',
        ));

        $this->add_field_background(array(
            'settings' => 'header_main_background',
            'section' => 'header_desktop_section',
            'class' => 'header_main',
            'element' => '.rt-header__main, .rt-header__sticky',
        ));

        $this->add_field_border_color(array(
            'settings' => 'header_main_border_color',
            'section' => 'header_desktop_section',
            'class' => 'header_main',
            'element' => '.rt-header__main, .rt-header__sticky',
        ));

        $this->add_field_background(array(
            'label' => 'Color Divender',
            'settings' => 'header_main_color_divender',
            'section' => 'header_desktop_section',
            'class' => 'header_main',
            'element' => '.rt-header__main .rt-header__divender, .rt-header__sticky .rt-header__divender',
        ));
    }

    public function add_options_sticky()
    {
        if (rt_is_premium()) {

            $this->add_header(array(
                'label' => 'Sticky Bar',
                'settings' => 'header_sticky',
                'section' => 'header_desktop_section',
                'class' => 'header_sticky',
                'tooltip' => 'Header melayang saat halaman website di geser ke bawah',
            ));

            $this->add_field(array(
                'type' => 'toggle',
                'settings' => 'header_sticky',
                'label' => __('Enable Sticky Header', RT_THEME_DOMAIN),
                'section' => 'header_desktop_section', 'class' => 'header_main',
                'class' => 'header_sticky',
                'default' => true,
            ));

            $this->add_field(array(
                'settings' => 'header_sticky_height',
                'type' => 'slider',
                'label' => __('Height', RT_THEME_DOMAIN),
                'section' => 'header_desktop_section',
                'class' => 'header_sticky',
                'default' => '60',
                'choices' => array(
                    'min' => '45',
                    'max' => '150',
                    'step' => '1',
                ),
                'output' => array(
                    array(
                        'element' => '.rt-header__sticky',
                        'property' => 'height',
                        'units' => 'px',
                    ),
                    array(
                        'element' => '.rt-header__sticky .rt-menu--horizontal',
                        'property' => 'height',
                        'units' => 'px',
                    ),
                    array(
                        'element' => '.rt-header.is-sticky .rt-search--overlay',
                        'property' => 'height',
                        'units' => 'px',
                    ),
                ),
                'transport' => 'auto',
            ));

            $this->add_field_link(array(
                'label' => __('Link', RT_THEME_DOMAIN),
                'settings' => 'header_sticky_link',
                'section' => 'header_desktop_section',
                'pseudo' => 'hover',
                'output' => array(
                    array(
                        'element' => '.rt-header__sticky .rt-header__html a,
                                        .rt-header__sticky .rt-header__element i,
                                        .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item > a,
                                        .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item > .rt-menu__arrow i',
                        'property' => 'color',
                        'choice' => 'normal',
                    ),
                    array(
                        'element' => '.rt-header__sticky .rt-header__html a:hover,
                                            .rt-header__sticky .rt-header__element i:hover,
                                            .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > a,
                                            .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > .rt-menu__arrow i,

                                            .rt-header__sticky .rt-menu__main > li.rt-menu__item.current-menu-parent > a,
                                            .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-parent > .rt-menu__arrow i,

                                            .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > a,
                                            .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > .rt-menu__arrow.is-active i,

                                            .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > a,
                                            .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > .rt-menu__arrow.is-active i',

                        'property' => 'color',
                        'choice' => 'hover',
                    ),

                ),
            ));

            $this->add_field_background(array(
                'settings' => 'header_sticky_background',
                'section' => 'header_desktop_section',
                'class' => 'header_sticky',
                'element' => '.rt-header__sticky',
            ));

            $this->add_field_border_color(array(
                'settings' => 'header_sticky_border_color',
                'section' => 'header_desktop_section',
                'class' => 'header_sticky',
                'element' => '.rt-header__sticky',
            ));

            $this->add_field_background(array(
                'label' => 'Color Divender',
                'settings' => 'header_sticky_color_divender',
                'section' => 'header_desktop_section',
                'class' => 'header_sticky',
                'element' => '.rt-header__sticky .rt-header__divender',
            ));
        }
    }

    public function add_options_topbar()
    {
        if (rt_is_premium()) {

            $this->add_header(array(
                'label' => 'Top Bar',
                'settings' => 'header_topbar',
                'section' => 'header_desktop_section',
                'class' => 'header_topbar',
            ));
            $this->add_field(array(
                'type' => 'toggle',
                'settings' => 'header_topbar',
                'label' => __('Enable Top Header', RT_THEME_DOMAIN),
                'section' => 'header_desktop_section',
                'class' => 'header_topbar',
                'default' => false,
                'partial_refresh' => array(
                    'header_topbar' => array(
                        'selector' => '#header-topbar',
                        'render_callback' => function () {
                            rt_get_template_part('header/header-topbar');
                        },
                    ),
                ),

            ));

            $this->add_field(array(
                'settings' => 'header_topbar_height',
                'type' => 'slider',
                'label' => __('Height', RT_THEME_DOMAIN),
                'section' => 'header_desktop_section',
                'class' => 'header_topbar',
                'default' => '37',
                'choices' => array(
                    'min' => '30',
                    'max' => '100',
                    'step' => '1',
                ),
                'output' => array(
                    array(
                        'element' => '.rt-header__topbar, .rt-header__topbar .rt-menu',
                        'property' => 'height',
                        'units' => 'px',
                    ),
                ),
                'transport' => 'auto',
            ));

            $this->add_field_color(array(
                'settings' => 'header_topbar_color',
                'section' => 'header_desktop_section',
                'class' => 'header_topbar',
                'element' => '.rt-header__topbar .rt-header__element',
            ));

            $this->add_field_link(array(
                'settings' => 'header_topbar_link',
                'section' => 'header_desktop_section',
                'class' => 'header_topbar',
                'element' => '.rt-header__topbar .rt-header__element a,
                            .rt-header__topbar .rt-menu--horizontal .rt-menu__item > a,
                            .rt-header__topbar .rt-menu--horizontal .rt-menu__main > li.rt-menu__item > .rt-menu__arrow i',
                'pseudo' => 'hover',
            ));
            $this->add_field_background(array(
                'settings' => 'header_topbar_background',
                'section' => 'header_desktop_section',
                'class' => 'header_topbar',
                'element' => '.rt-header__topbar',
            ));

            $this->add_field_border_color(array(
                'settings' => 'header_topbar_border_color',
                'section' => 'header_desktop_section',
                'class' => 'header_topbar',
                'element' => '.rt-header__topbar',
            ));

            $this->add_field_background(array(
                'label' => 'Color Divender',
                'settings' => 'header_topbar_color_divender',
                'section' => 'header_desktop_section',
                'class' => 'header_topbar',
                'element' => '.rt-header__topbar .rt-header__divender',
            ));
        }
    }

    public function add_options_middle()
    {
        if (rt_is_premium()) {
            $this->add_header(array(
                'label' => 'Middle Bar',
                'settings' => 'header_middle',
                'section' => 'header_desktop_section',
                'class' => 'header_middle',
            ));

            $this->add_field(array(
                'type' => 'toggle',
                'settings' => 'header_middle',
                'label' => __('Enable Middle Header', RT_THEME_DOMAIN),
                'section' => 'header_desktop_section',
                'class' => 'header_middle',
                'default' => false,
                'partial_refresh' => array(
                    'header_middle' => array(
                        'selector' => '#header-middle',
                        'render_callback' => function () {
                            rt_get_template_part('header/header-middle');
                        },
                    ),
                ),
            ));

            $this->add_field(array(
                'settings' => 'header_middle_height',
                'type' => 'slider',
                'label' => __('Height', RT_THEME_DOMAIN),
                'section' => 'header_desktop_section',
                'class' => 'header_middle',
                'default' => '120',
                'choices' => array(
                    'min' => '30',
                    'max' => '200',
                    'step' => '1',
                ),
                'output' => array(
                    array(
                        'element' => '.rt-header__middle',
                        'property' => 'height',
                        'units' => 'px',
                    ),

                    array(
                        'element' => '.rt-header__middle .rt-menu--horizontal',
                        'property' => 'height',
                        'units' => 'px',
                    ),

                ),
                'transport' => 'auto',
            ));

            $this->add_field_color(array(
                'settings' => 'header_middle_color',
                'section' => 'header_desktop_section',
                'class' => 'header_middle',
                'element' => '.rt-header__middle',
            ));

            $this->add_field_link(array(
                'settings' => 'header_middle_link',
                'label' => __('Link', RT_THEME_DOMAIN),
                'section' => 'header_desktop_section',
                'pseudo' => 'hover',
                'output' => array(
                    array(
                        'element' => '.rt-header__middle .rt-header__html a,
                                        .rt-header__middle .rt-header__element i,
                                        .rt-header__middle .rt-menu--horizontal .rt-menu__main > li.rt-menu__item > a,
                                        .rt-header__middle .rt-menu--horizontal .rt-menu__main > li.rt-menu__item > .rt-menu__arrow i',
                        'property' => 'color',
                        'choice' => 'normal',
                    ),
                    array(
                        'element' => '.rt-header__middle .rt-header__html a:hover,
                                            .rt-header__middle .rt-header__element i:hover,
                                            .rt-header__middle .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > a,
                                            .rt-header__middle .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > .rt-menu__arrow i,

                                            .rt-header__middle .rt-menu__main > li.rt-menu__item.current-menu-parent > a,
                                            .rt-header__middle .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-parent > .rt-menu__arrow i,

                                            .rt-header__middle .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > a,
                                            .rt-header__middle .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > .rt-menu__arrow.is-active i,

                                            .rt-header__middle .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > a,
                                            .rt-header__middle .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > .rt-menu__arrow.is-active i',

                        'property' => 'color',
                        'choice' => 'hover',
                    ),

                ),
            ));

            $this->add_field_background(array(
                'settings' => 'header_middle_background',
                'section' => 'header_desktop_section',
                'class' => 'header_middle',
                'element' => '.rt-header__middle',
            ));

            $this->add_field_border_color(array(
                'settings' => 'header_middle_border_color',
                'section' => 'header_desktop_section',
                'class' => 'header_middle',
                'element' => '.rt-header__middle',
            ));

            $this->add_field_background(array(
                'label' => 'Color Divender',
                'settings' => 'header_middle_color_divender',
                'section' => 'header_desktop_section',
                'class' => 'header_middle',
                'element' => '.rt-header__middle .rt-header__divender',
            ));
        }
    }

    public function add_options_overlay()
    {
        $section = 'header_overlay_section';

        $this->add_header(array(
            'label' => 'Header Overlay',
            'settings' => 'header_overlay',
            'section' => $section,
            'class' => 'header_main',
        ));

        $this->add_field(array(
            'settings' => 'header_overlay_height',
            'type' => 'slider',
            'label' => __('Height', RT_THEME_DOMAIN),
            'section' => $section,
            'class' => 'header_main',
            'default' => '60',
            'choices' => array(
                'min' => '45',
                'max' => '150',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header.is-overlay .rt-header__main',
                    'property' => 'height',
                    'units' => 'px',
                ),
                array(
                    'element' => '.rt-menu--horizontal',
                    'property' => 'height',
                    'units' => 'px',
                ),

                array(
                    'element' => '.rt-header.is-overlay .rt-header .rt-search--overlay',
                    'property' => 'height',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));

        $this->add_field(array(
            'settings' => 'header_overlay_link',
            'section' => $section,
            'class' => 'header_main',
            'type' => 'color',
            'choices' => array(
                'alpha' => true,
            ),
            'label' => __('Link', RT_THEME_DOMAIN),
            'output' => array(
                array(
                    'element' => '.rt-header.is-overlay .rt-header__main .rt-header__html a,
	                   .rt-header.is-overlay .rt-header__main .rt-header__element i,
	                   .rt-header.is-overlay .rt-header__main .rt-menu__main > li.rt-menu__item > a,
                       .rt-header.is-overlay .rt-header__main .rt-menu__main > li.rt-menu__item > .rt-menu__arrow i',
                    'property' => 'color',
                ),
                array(
                    'element' => '.rt-header-mobile.is-overlay .rt-menu-toggle span,
                       .rt-header-mobile.is-overlay .rt-menu-toggle span::after,
                       .rt-header-mobile.is-overlay .rt-menu-toggle span::before',
                    'property' => 'background-color',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field(array(
            'settings' => 'header_overlay_link_hover',
            'section' => $section,
            'class' => 'header_main',
            'type' => 'color',
            'choices' => array(
                'alpha' => true,
            ),
            'label' => __('Link :Hover', RT_THEME_DOMAIN),
            'output' => array(
                array(
                    'element' => '.rt-header.is-overlay .rt-header__main .rt-header__html a:hover,
	                   .rt-header.is-overlay .rt-header__main .rt-header__element i:hover,
	                   .rt-header.is-overlay .rt-header__main .rt-menu__main > li.rt-menu__item.is-active > a,
                       .rt-header.is-overlay .rt-header__main .rt-menu__main > li.rt-menu__item.is-active > .rt-menu__arrow i,
                       .rt-header.is-overlay .rt-header__main .rt-menu__main  > li.rt-menu__item.current-menu-item a',
                    'property' => 'color',
                ),
                array(
                    'element' => '.rt-header-mobile.is-overlay .rt-menu-toggle.is-active span,
                       .rt-header-mobile.is-overlay .rt-menu-toggle.is-active span::after,
                       .rt-header-mobile.is-overlay .rt-menu-toggle.is-active span::before',
                    'property' => 'background-color',
                ),
            ),
            'transport' => 'auto',

        ));

        $this->add_field_background(array(
            'settings' => 'header_overlay_background',
            'section' => $section,
            'class' => 'header_main',
            'element' => '.rt-header.is-overlay .rt-header__main,
                            .rt-header-mobile.is-overlay .rt-header-mobile__main',
        ));

        $this->add_field_border_color(array(
            'settings' => 'header_overlay_border_color',
            'section' => $section,
            'class' => 'header_main',
            'element' => '.rt-header.is-overlay .rt-header__main',
        ));

        $this->add_field_background(array(
            'label' => 'Color Divender',
            'settings' => 'header_overlay_color_divender',
            'section' => $section,
            'class' => 'header_main',
            'element' => '.rt-header.is-overlay .rt-header__main .rt-header__divender',
        ));

        $this->add_field_background(array(
            'label' => 'Background Effect',
            'settings' => 'header_overlay_color_effect',
            'section' => $section,
            'class' => 'header_main',
            'element' => '.rt-header.is-overlay .rt-menu--horizontal .rt-menu__item a::before',
        ));
    }

    public function add_layout_prebuilder()
    {

        /**
         * get list from elementor library
         */
        $library = \Retheme\Helper::get_posts('elementor_library');
        array_unshift($library, 'Select Header Library');

        $this->add_field(array(
            'type' => 'select',
            'section' => 'header_layout_section',
            'settings' => 'header_layout_builder_elementor',
            'label' => __('Elementor Library', RT_THEME_DOMAIN),
            'multiple' => 1,
            'choices' => $library,
            'active_callback' => array(
                array(
                    'setting' => 'header_layout_builder',
                    'operator' => '==',
                    'value' => array('header-builder'),
                ),
            ),

        ));
    }

    public function add_main_menu()
    {
        $section = 'header_main_menu_section';

        $this->add_header(array(
            'label' => 'Menu',
            'settings' => 'header_main_menu',
            'section' => $section,
            'class' => 'header_menu',
        ));

        $this->add_field(array(
            'type' => 'typography',
            'settings' => 'header_main_menu_typography',
            'label' => __('Typography', RT_THEME_DOMAIN),
            'section' => $section,
            'class' => 'header_menu',
            'default' => array(
                'font-family' => get_theme_mod('general_heading_typography', rt_var('font-primary')),
                'variant' => '',
                'font-size' => '',
                'text-transform' => 'none',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header .rt-menu--horizontal .rt-menu__item>a',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field(array(
            'settings' => 'header_main_menu_menu_bg',
            'type' => 'color',
            'choices' => array(
                'alpha' => true,
            ),
            'label' => __('Background Effect', RT_THEME_DOMAIN),
            'section' => $section,
            'class' => 'header_menu',
            'output' => array(
                array(
                    'element' => '.rt-header .rt-menu--horizontal .rt-menu__item a::before',
                    'property' => 'background-color',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field(array(
            'settings' => 'header_main_menu_menu_gap',
            'type' => 'slider',
            'label' => __('Menu Gap', RT_THEME_DOMAIN),
            'section' => $section,
            'choices' => array(
                'min' => '5',
                'max' => '30',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header .rt-menu.rt-menu--horizontal:not(#topbar-menu) .rt-menu__item a',
                    'property' => 'padding-left',
                    'units' => 'px',
                ),
                array(
                    'element' => '.rt-header .rt-menu.rt-menu--horizontal:not(#topbar-menu) .rt-menu__item a',
                    'property' => 'padding-right',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));
    }

    public function add_main_submenu()
    {
        $section = 'header_main_menu_section';

        $this->add_header(array(
            'label' => 'Sub Menu',
            'settings' => 'header_main_submenu',
            'section' => $section,
            'class' => 'header_submenu',
        ));
        $this->add_field(array(
            'type' => 'typography',
            'settings' => 'header_main_submenu_typography',
            'label' => __('Typography', RT_THEME_DOMAIN),
            'section' => $section,
            'class' => 'header_submenu',
            'default' => array(
                'font-family' => get_theme_mod('general_heading_typography', rt_var('font-primary')),
                'variant' => '',
                'font-size' => '',
                'text-transform' => 'none',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header .rt-menu--horizontal .rt-menu__submenu .rt-menu__item a',
                ),
            ),
            'transport' => 'auto',

        ));

        if (rt_is_premium()) {
            $this->add_field_background(array(
                'settings' => 'header_main_submenu_background',
                'section' => $section,
                'class' => 'header_submenu',
                'element' => '.rt-menu--horizontal .rt-menu__submenu',
            ));

            $this->add_field_link(array(
                'settings' => 'header_main_submenu_link',
                'section' => $section,
                'pseudo' => 'hover',
                'element' => '.rt-header .rt-menu--horizontal .rt-menu__submenu .rt-menu__item a,
                           .rt-header .rt-menu--horizontal .rt-menu__item > .rt-menu__arrow.is-active i',

            ));

            $this->add_field_animation(array(
                'settings' => 'header_main_submenu_animation',
                'section' => $section,
                'class' => 'header_submenu',
            ));
        }
    }

    public function add_mobile_header()
    {
        $section = 'header_mobile_section';

        $this->add_header(array(
            'label' => 'Header',
            'settings' => 'header_mobile_menu',
            'section' => $section,
            'class' => 'header_mobile',

        ));

        $this->add_field_background(array(
            'settings' => 'header_mobile_menu_background',
            'section' => $section,
            'class' => 'header_mobile',
            'element' => '.rt-header-mobile__main',
        ));

        $this->add_field_border_color(array(
            'settings' => 'header_mobile_menu_border_color',
            'section' => $section,
            'class' => 'header_mobile',
            'element' => '.rt-header-mobile__main',
        ));

        $this->add_field_color(array(
            'settings' => 'header_mobile_menu_menu_link',
            'section' => $section,
            'class' => 'header_mobile',
            'element' => '.rt-header-mobile__main i, .single-product .rt-header-mobile h5.rt-header__element',
            'pseudo' => 'hover',
        ));

        $this->add_field_background(array(
            'label' => 'Link Bar Color',
            'settings' => 'header_mobile_menu_menu_bar',
            'section' => $section,
            'class' => 'header_mobile',
            'element' => '.rt-header-mobile__main .rt-menu-toggle span,
                    .rt-header-mobile__main .rt-menu-toggle span:after,
                    .rt-header-mobile__main .rt-menu-toggle span:before',
        ));

        $this->add_field_background(array(
            'label' => 'Color Divender',
            'settings' => 'header_mobile_menu_color_divender',
            'section' => $section,
            'class' => 'header_mobile',
            'element' => '.rt-header-mobile__main .rt-header__divender',
        ));
    }

    public function add_mobile_header_sticky()
    {
        if (rt_is_premium()) {
            $section = 'header_mobile_section';

            $this->add_header(array(
                'label' => 'Sticky Header',
                'settings' => 'header_mobile_sticky',
                'section' => $section,
                'class' => 'header_sticky_mobile',
            ));

            $this->add_field(array(
                'type' => 'toggle',
                'settings' => 'header_mobile_sticky',
                'label' => __('Enable Sticky Header', RT_THEME_DOMAIN),
                'section' => $section,
                'class' => 'header_sticky_mobile',
                'default' => true,
            ));

            $this->add_field(array(
                'type' => 'toggle',
                'settings' => 'header_mobile_sticky',
                'label' => __('Enable Sticky Mobile Menu', RT_THEME_DOMAIN),
                'section' => $section,
                'tooltip' => 'Header melayang saat halaman website di geser ke bawah',
                'default' => true,
            ));

            $this->add_field_background(array(
                'settings' => 'header_mobile_sticky_background',
                'section' => $section,
                'class' => 'header_sticky_mobile',
                'element' => '.rt-header-mobile.is-sticky .rt-header-mobile__main',
            ));

            $this->add_field_border_color(array(
                'settings' => 'header_mobile_sticky_border_color',
                'section' => $section,
                'class' => 'header_sticky_mobile',
                'element' => '.rt-header-mobile.is-sticky .rt-header-mobile__main',
            ));

            $this->add_field_color(array(
                'settings' => 'header_mobile_sticky_menu_link',
                'section' => $section,
                'class' => 'header_sticky_mobile',
                'element' => '.rt-header-mobile.is-sticky .rt-header-mobile__main i',
                'pseudo' => 'hover',
            ));

            $this->add_field_background(array(
                'label' => 'Link Bar Color',
                'settings' => 'header_mobile_sticky_menu_bar',
                'section' => $section,
                'class' => 'header_sticky_mobile',
                'element' => '.rt-header-mobile.is-sticky .rt-menu-toggle span,
                    .rt-header-mobile.is-sticky .rt-menu-toggle span:after,
                    .rt-header-mobile.is-sticky .rt-menu-toggle span:before',
            ));

            $this->add_field_background(array(
                'label' => 'Color Divender',
                'settings' => 'header_mobile_sticky_menu_color_divender',
                'section' => $section,
                'class' => 'header_sticky_mobile',
                'element' => '.rt-header-mobile.is-sticky .rt-header__divender',
            ));
        }
    }

    public function add_drawer_menu()
    {
        if (rt_is_premium()) {
            $section = 'header_mobile_menu_section';
            $settings = 'header_drawer_menu';

            $this->add_field(array(
                'type' => 'select',
                'settings' => 'header_drawer_menu_style',
                'section' => $section,
                'tooltip' => 'Atur jenis menu yang ditampilkan',
                'label' => __('Style', 'admin_domain'),
                'default' => 'dropdown',
                'choices' => array(
                    'dropdown' => 'Dropdown',
                    'sidepanel' => 'Side Panel',
                ),

            ));

            $this->add_field(array(
                'type' => 'select',
                'settings' => 'header_drawer_menu_schema',
                'section' => $section,
                'label' => __('Color Schema', 'admin_domain'),
                'default' => 'Dark',
                'choices' => array(
                    'dark' => 'Dark',
                    'light' => 'Light',
                    'custom' => 'Custom',
                ),

            ));
            $this->add_header(array(
                'label' => 'Header Panel',
                'settings' => 'header_drawer_menu_header_panel',
                'section' => $section,
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_style',
                        'operator' => '==',
                        'value' => 'sidepanel',
                    ),
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),
            ));

            $this->add_field_color(array(
                'settings' => 'header_drawer_menu_header_panel_color',
                'section' => $section,
                'element' => '.rt-main-canvas-menu--custom .rt-sidepanel__header,
                .rt-main-canvas-menu--custom .rt-sidepanel__title,
                             .rt-main-canvas-menu--custom .rt-sidepanel__close',
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_style',
                        'operator' => '==',
                        'value' => 'sidepanel',
                    ),
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),
            ));

            $this->add_field_background(array(
                'settings' => 'header_drawer_menu_header_panel_background',
                'section' => $section,
                'element' => '.rt-main-canvas-menu--custom .rt-sidepanel__header',
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_style',
                        'operator' => '==',
                        'value' => 'sidepanel',
                    ),
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),
            ));

            $this->add_header(array(
                'label' => 'Menu',
                'settings' => 'header_drawer_menu_typography',
                'section' => $section,
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),
            ));

            $this->add_field_background(array(
                'settings' => 'header_drawer_menu_background',
                'section' => $section,
                'element' => '.rt-main-canvas-menu--custom,
                                .rt-main-canvas-menu--custom .rt-sidepanel__inner',
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),
            ));

            $this->add_field(array(
                'type' => 'typography',
                'label' => __('Typography', RT_THEME_DOMAIN),
                'settings' => 'header_drawer_menu_typography',
                'section' => $section,
                'default' => [
                    'font-family' => '',
                    'variant' => '',
                    'font-size' => '',
                    'letter-spacing' => '',
                    'text-transform' => '',
                ],
                'transport' => 'auto',
                'output' => [
                    [
                        'element' => '.rt-main-canvas-menu--custom .rt-menu--vertical .rt-menu__item a',
                    ],
                ],
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),
            ));

            $this->add_field_color(array(
                'settings' => 'header_drawer_menu_color',
                'section' => $section,
                'element' => '.rt-main-canvas-menu--custom .rt-header__html,
                             .rt-main-canvas-menu--custom .rt-menu--vertical .rt-menu__item a,
                             .rt-main-canvas-menu--custom .rt-sidepanel__title,
                             .rt-main-canvas-menu--custom .rt-sidepanel__close',
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),
            ));
            $this->add_field_border_color(array(
                'settings' => 'header_drawer_menu_border_color',
                'section' => $section,
                'element' => '.rt-main-canvas-menu--custom .rt-menu--vertical .rt-menu__item a,
                             .rt-main-canvas-menu--custom .rt-header__element',
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),
            ));

            $this->add_header(array(
                'label' => 'Sub Menu',
                'settings' => 'header_drawer_submenu',
                'section' => $section,
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),
            ));

            $this->add_field_background(array(
                'settings' => 'header_drawer_submenu_background',
                'section' => $section,
                'element' => '.rt-main-canvas-menu--custom .rt-menu--vertical .rt-menu__submenu',
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),
            ));
            $this->add_field_color(array(
                'settings' => 'header_drawer_submenu_color',
                'section' => $section,
                'element' => '.rt-main-canvas-menu--custom .rt-menu--vertical .rt-menu__submenu .rt-menu__item a',
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),
            ));

            $this->add_header(array(
                'label' => 'Arrow',
                'settings' => 'header_drawer_arrow',
                'section' => $section,
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),

            ));

            $this->add_field_background(array(
                'settings' => 'header_drawer_arrow_background',
                'section' => $section,
                'element' => '.rt-main-canvas-menu--custom .rt-menu--vertical .rt-menu__arrow',
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),
            ));
            $this->add_field_color(array(
                'settings' => 'header_drawer_arrow_color',
                'section' => $section,
                'element' => '.rt-main-canvas-menu--custom .rt-menu--vertical .rt-menu__arrow',
                'active_callback' => array(
                    array(
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ),
                ),
            ));

        }
    }

    public function add_drawer_search()
    {
        if (rt_is_premium()) {
            $section = 'header_search_section';
            $settings = 'header_drawer';
            $element = '.rt-header-mobile .rt-search__input, .rt-main-canvas-menu .rt-search__input';

            $this->add_header(array(
                'label' => 'Search Form (Mobile)',
                'settings' => $settings,
                'section' => $section,
                'class' => 'menu_drawer_search',
            ));

            $this->add_field_color(array(
                'settings' => $settings . '_color',
                'section' => $section,
                'class' => 'menu_drawer_search',
                'element' => '.rt-header-mobile .rt-search__input,
                          .rt-header-mobile .rt-search__btn i,
                          .rt-main-canvas-menu .rt-search__input,
                          .rt-main-canvas-menu .rt-search__btn i',
            ));

            $this->add_field_background(array(
                'settings' => $settings . '_background',
                'section' => $section,
                'class' => 'menu_drawer_search',
                'element' => $element,
            ));

            $this->add_field_border_color(array(
                'settings' => $settings . '_border_color',
                'section' => $section,
                'class' => 'menu_drawer_search',
                'element' => $element,
            ));

            $this->add_field_border_radius(array(
                'settings' => $settings . '_border_radius',
                'section' => $section,
                'class' => 'menu_drawer_search',
                'element' => $element,
            ));

            $this->add_field(array(
                'type' => 'dimension',
                'settings' => 'header_drawer_search',
                'label' => __('Width (px/%)', RT_THEME_DOMAIN),
                'section' => $section,
                'class' => 'menu_drawer_search',
                'default' => '100%',
                'output' => array(
                    array(
                        'element' => '.rt-header-mobile .rt-header__search-form,
                                        .rt-main-canvas-menu .rt-search__input .rt-header__search-form',
                        'property' => 'width',
                    ),

                ),

                'transport' => 'auto',
            ));
        }
    }

    public function add_element_social()
    {if (rt_is_premium()) {
        $section = 'header_socmed_section';
        $settings = 'header_social';

        $this->add_header(array(
            'label' => 'Social Media',
            'settings' => $settings,
            'section' => $section,
            'class' => 'header_socmed',
        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => $settings . '_style',
            'label' => __('Style', RT_THEME_DOMAIN),
            'section' => $section,
            'class' => 'header_socmed',
            'default' => 'simple',
            'multiple' => 1,
            'choices' => array(
                'simple' => __('Simple', RT_THEME_DOMAIN),
                'brand' => __('Brand', RT_THEME_DOMAIN),
                'border' => __('Border', RT_THEME_DOMAIN),
            ),
        ));

        $this->add_field_color(array(
            'label' => __('Color', RT_THEME_DOMAIN),
            'type' => 'multicolor',
            'settings' => $settings . '_color',
            'section' => $section,
            'pseudo' => 'hover',
            'output' => array(

                array(
                    'element' => '.header_social .rt-socmed .rt-socmed__item',
                    'property' => 'color',
                    'suffix' => '!important',
                    'choice' => 'normal',
                ),
                array(
                    'element' => '.header_social .rt-socmed .rt-socmed__item',
                    'property' => 'color',
                    'suffix' => '!important',
                    'choice' => 'hover',
                ),
            ),
        ));

        $this->add_field_background(array(
            'label' => __('Background', RT_THEME_DOMAIN),
            'settings' => $settings . '_background',
            'section' => $section,
            'pseudo' => 'hover',
            'output' => array(
                array(
                    'element' => '.header_social .rt-socmed .rt-socmed__item',
                    'property' => 'background-color',
                    'suffix' => '!important',
                    'choice' => 'normal',
                ),
                array(
                    'element' => '.header_social .rt-socmed .rt-socmed__item',
                    'property' => 'background-color',
                    'suffix' => '!important',
                    'choice' => 'hover',
                ),
            ),
        ));

        $this->add_field_border_color(array(
            'label' => __('Border Color', RT_THEME_DOMAIN),
            'settings' => $settings . '_border_color',
            'section' => $section,
            'pseudo' => 'hover',
            'output' => array(
                array(
                    'element' => '.header_social .rt-socmed .rt-socmed__item',
                    'property' => 'border-color',
                    'suffix' => '!important',
                    'choice' => 'normal',
                ),
                array(
                    'element' => '.header_social .rt-socmed .rt-socmed__item',
                    'property' => 'border-color',
                    'suffix' => '!important',
                    'choice' => 'hover',
                ),
            ),
        ));

        $this->add_field_border_radius(array(
            'label' => __('Border Radius', RT_THEME_DOMAIN),
            'settings' => $settings . '_border_color_hover',
            'section' => $section,
            'element' => '.header_social .rt-socmed .rt-socmed__item',

        ));
    }
    }

    public function add_element_button()
    {
        if (rt_is_premium()) {

            for ($index = 1; $index <= 3; $index++) {

                $section = 'header_button_section';

                $this->add_header(array(
                    'label' => "Button {$index}",
                    'settings' => "header_button_{$index}",
                    'section' => $section,
                ));

                $this->add_field(array(
                    'type' => 'text',
                    'settings' => "header_button_{$index}_text",
                    'label' => __('Button Text', RT_THEME_DOMAIN),
                    'section' => $section,
                    'default' => __('Your text', RT_THEME_DOMAIN),
                    'transport' => 'postMessage',
                ));

                $this->add_field(array(
                    'type' => 'text',
                    'settings' => "header_button_{$index}_link",
                    'label' => __('Button Link', RT_THEME_DOMAIN),
                    'section' => $section,
                    'default' => __('#', RT_THEME_DOMAIN),
                ));

                $this->add_field(array(
                    'type' => 'select',
                    'settings' => "header_button_{$index}_link_target",
                    'label' => __('Target', RT_THEME_DOMAIN),
                    'section' => $section,
                    'class' => 'header_btn_1',
                    'default' => 'blank',
                    'choices' => array(
                        'blank' => 'Open New Tab',
                        'self' => 'Keep on Page',
                    ),
                ));

                $this->add_field(array(
                    'type' => 'fontawesome',
                    'settings' => "header_button_{$index}_icon",
                    'label' => __('Icon', RT_THEME_DOMAIN),
                    'section' => $section,
                ));

                $this->add_field_button(array(
                    'settings' => "header_button_{$index}",
                    'section' => $section,
                    'element' => ".rt-btn.rt-btn--{$index}",
                ));

                $this->add_field_responsive(array(
                    'type' => 'dimensions',
                    'settings' => "header_btn_{$index}_margin",
                    'label' => __('Spacer', RT_THEME_DOMAIN),
                    'section' => $section,
                    'default' => array(
                        'left' => '0',
                        'right' => '5px',
                    ),
                    'output' => array(
                        array(
                            'element' => ".rt-btn.rt-btn--{$index}",
                            'property' => 'margin',
                        ),

                    ),

                    'transport' => 'auto',
                ));

            }
        }
    }

    public function add_element_search_form()
    {
        if (rt_is_premium()) {
            $section = 'header_search_section';
            $settings = 'header_search_form';

            $this->add_header(array(
                'label' => 'Search Form',
                'settings' => $settings,
                'section' => $section,
                'class' => 'header_search_form',
            ));

            $this->add_field_background(array(
                'settings' => $settings . '_background',
                'section' => $section,
                'class' => 'header_search_form',
                'element' => '.rt-header .rt-header__search-form .rt-search__input,
	                    .rt-header__topbar .rt-header__search-form .rt-search__input',
                'pseudo' => 'hover',
            ));

            $this->add_field_color(array(
                'settings' => $settings . '_color',
                'section' => $section,
                'class' => 'header_search_form',
                'element' => '.rt-header .rt-header__search-form .rt-search__input,
	                    .rt-header .rt-header__search-form i',
                'pseudo' => 'hover',
            ));

            $this->add_field_border_color(array(
                'settings' => $settings . '_border',
                'section' => $section,
                'class' => 'header_search_form',
                'element' => '.rt-header .rt-header__search-form .rt-search__input',
                'pseudo' => 'hover',
            ));

            $this->add_field_border_radius(array(
                'settings' => $settings . '_border_radius',
                'section' => $section,
                'class' => 'header_search_form',
                'element' => '.rt-header .rt-header__search-form .rt-search__input',
            ));

            $this->add_field(array(
                'type' => 'dimension',
                'settings' => 'header_search_form_width',
                'label' => __('Width (px/%)', RT_THEME_DOMAIN),
                'section' => $section,
                'class' => 'header_search_form',
                'default' => '100%',
                'output' => array(
                    array(
                        'element' => '.rt-header .rt-header__search-form',
                        'property' => 'width',
                    ),

                ),

                'transport' => 'auto',
            ));
        }
    }

    public function add_element_html()
    {

        $section = 'header_html_section';

        if (rt_is_premium()) {
            for ($index = 1; $index <= 5; $index++) {

                $this->add_header(array(
                    'label' => "HTML {$index}",
                    'settings' => "header_html_{$index}",
                    'section' => $section,
                ));

                $this->add_field(array(
                    'label' => 'Input Your Text/HTML',
                    'settings' => "header_html_{$index}",
                    'section' => $section,
                    'type' => 'textarea',
                ));

                $this->add_field(array(
                    'type' => 'fontawesome',
                    'settings' => "header_html_{$index}_icon",
                    'label' => __('Icon', RT_THEME_DOMAIN),
                    'section' => $section,
                ));

                $this->add_field(array(
                    'type' => 'toggle',
                    'settings' => "header_html_{$index}_shortcode",
                    'label' => __('Enable Shortcode', RT_THEME_DOMAIN),
                    'section' => $section,
                    'default' => '',
                ));

            }

        }

        if (rt_is_free()) {
            $this->add_header(array(
                'label' => "HTML 1",
                'settings' => "header_html_1",
                'section' => $section,
            ));

            $this->add_field(array(
                'label' => 'Input Your Text/HTML',
                'settings' => "header_html_1",
                'section' => $section,
                'type' => 'textarea',
            ));

            $this->add_field(array(
                'type' => 'fontawesome',
                'settings' => "header_html_1_icon",
                'label' => __('Icon', RT_THEME_DOMAIN),
                'section' => $section,
            ));

            $this->add_field(array(
                'type' => 'toggle',
                'settings' => "header_html_1_shortcode",
                'label' => __('Enable Shortcode', RT_THEME_DOMAIN),
                'section' => $section,
                'default' => '',
            ));

        }

    }

    public function add_cart_icon()
    {
        $section = 'header_cart_icon_section';

        $this->add_field(array(
            'type' => 'select',
            'section' => $section,
            'settings' => 'header_cart_icon',
            'label' => __('Icon', RT_THEME_DOMAIN),
            'multiple' => 1,
            'choices' => array(
                'cart-light' => 'Shopping Cart Light',
                'shopping-cart' => 'Shopping Cart Bold',
                'shopping-basket' => 'Shopping Cart Basket',
            ),

        ));
        // $this->add_field(array(
        //     'type' => 'toggle',
        //     'settings' => 'header_cart_total',
        //     'label' => __('Subtotal', RT_THEME_DOMAIN),
        //     'section' => $section,
        //     'default' => true,
        // ));
        $this->add_field_color(array(
            'settings' => 'cart_icon_color',
            'section' => $section,
            'element' => '.rt-header__cart',
            'pseudo' => 'hover',
        ));

        $this->add_field_background(array(
            'settings' => 'cart_icon_background',
            'section' => $section,
            'element' => '.rt-header__cart',
            'pseudo' => 'hover',
        ));
        $this->add_field_border_color(array(
            'settings' => 'cart_icon_border_color',
            'section' => $section,
            'element' => '.rt-header__cart',
            'pseudo' => 'hover',
        ));

        $this->add_field_border_radius(array(
            'settings' => 'cart_icon_border_radius',
            'section' => $section,
            'element' => '.rt-header__cart',
        ));

        $this->add_field_responsive(array(
            'type' => 'dimension',
            'label' => __('Box Size', RT_THEME_DOMAIN),
            'settings' => 'cart_icon_box_size',
            'section' => $section,
            'description' => 'Use css unit px, %',
            'output' => array(
                array(
                    'element' => '.rt-header__cart',
                    'property' => 'line-height',
                ),
                array(
                    'element' => '.rt-header__cart',
                    'property' => 'width',
                ),

            ),
            'transport' => 'auto',
        ));

         $this->add_field_responsive(array(
            'type' => 'dimension',
            'label' => __('Icon Size', RT_THEME_DOMAIN),
            'settings' => 'cart_icon_size',
            'section' => $section,
            'description' => 'Use css unit px, %',
            'output' => array(
                array(
                    'element' => '.rt-header__cart i',
                    'property' => 'font-size',
                ),

            ),
            'transport' => 'auto',
        ));

        $this->add_field_margin(array(
            'settings' => 'cart_icon_border_margin',
            'section' => $section,
            'element' => '.rt-header__cart',
        ));

        $this->add_header(array(
            'label' => 'Count',
            'settings' => 'cart_icon_count',
            'section' => $section,
        ));
        $this->add_field_color(array(
            'settings' => 'cart_icon_count_color',
            'section' => $section,
            'element' => '.rt-header__cart-count',
        ));
        $this->add_field_background(array(
            'settings' => 'cart_icon_count_background',
            'section' => $section,
            'element' => '.rt-header__cart-count',
        ));
    }

   public function add_user_icon()
    {
        $section = 'header_user_icon_section';


        $this->add_field_color(array(
            'settings' => 'user_icon_color',
            'section' => $section,
            'element' => '.rt-header__user',
            'pseudo' => 'hover',
        ));

        $this->add_field_background(array(
            'settings' => 'user_icon_background',
            'section' => $section,
            'element' => '.rt-header__user',
            'pseudo' => 'hover',
        ));

         $this->add_field_border_color(array(
            'settings' => 'user_icon_border_color',
            'section' => $section,
            'element' => '.rt-header__user',
            'pseudo' => 'hover',
        ));

        $this->add_field_border_radius(array(
            'settings' => 'user_icon_border_radius',
            'section' => $section,
            'element' => '.rt-header__user',
        ));

        $this->add_field_responsive(array(
            'type' => 'dimension',
            'label' => __('Box Size', RT_THEME_DOMAIN),
            'settings' => 'user_icon_box_size',
            'section' => $section,
            'description' => 'Use css unit px, %',
            'output' => array(
                array(
                    'element' => '.rt-header__user',
                    'property' => 'line-height',
                ),
                array(
                    'element' => '.rt-header__user',
                    'property' => 'width',
                ),

            ),
            'transport' => 'auto',
        ));

         $this->add_field_responsive(array(
            'type' => 'dimension',
            'label' => __('Icon Size', RT_THEME_DOMAIN),
            'settings' => 'user_icon_size',
            'section' => $section,
            'description' => 'Use css unit px, %',
            'output' => array(
                array(
                    'element' => '.rt-header__user i',
                    'property' => 'font-size',
                ),

            ),
            'transport' => 'auto',
        ));

        $this->add_field_margin(array(
            'settings' => 'user_icon_border_margin',
            'section' => $section,
            'element' => '.rt-header__user',
        ));
    } 
    
    public function add_search_icon()
    {
        $section = 'header_search_icon_section';


        $this->add_field_color(array(
            'settings' => 'search_icon_color',
            'section' => $section,
            'element' => '.rt-header__search',
            'pseudo' => 'hover',
        ));

        $this->add_field_background(array(
            'settings' => 'search_icon_background',
            'section' => $section,
            'element' => '.rt-header__search',
            'pseudo' => 'hover',
        ));

         $this->add_field_border_color(array(
            'settings' => 'search_icon_border_color',
            'section' => $section,
            'element' => '.rt-header__search',
            'pseudo' => 'hover',
        ));

        $this->add_field_border_radius(array(
            'settings' => 'search_icon_border_radius',
            'section' => $section,
            'element' => '.rt-header__search',
        ));

        $this->add_field_responsive(array(
            'type' => 'dimension',
            'label' => __('Box Size', RT_THEME_DOMAIN),
            'settings' => 'search_icon_box_size',
            'section' => $section,
            'description' => 'Use css unit px, %',
            'output' => array(
                array(
                    'element' => '.rt-header__search',
                    'property' => 'line-height',
                ),
                array(
                    'element' => '.rt-header__search',
                    'property' => 'width',
                ),

            ),
            'transport' => 'auto',
        ));

         $this->add_field_responsive(array(
            'type' => 'dimension',
            'label' => __('Icon Size', RT_THEME_DOMAIN),
            'settings' => 'search_icon_size',
            'section' => $section,
            'description' => 'Use css unit px, %',
            'output' => array(
                array(
                    'element' => '.rt-header__search i',
                    'property' => 'font-size',
                ),

            ),
            'transport' => 'auto',
        ));

        $this->add_field_margin(array(
            'settings' => 'search_icon_border_margin',
            'section' => $section,
            'element' => '.rt-header__search',
        ));
    } 
    // end class
}

new Header;
