
jQuery(document).ready(function ($) {

    $.fn.retheme_accordion = function (options) {
        return this.each(function () {
            var element = $(this);

            var defaults = {
                animatein: 'transition.fadeIn',
                animateout: 'transition.fadeOut',
                duration: '300',
                easing: 'easeIn',
                toggle: '',
            };

            var meta = element.data();
            var options = $.extend(defaults, options, meta);

            var selector = {
                nav: '.rt-accordion__title',
                item: '.rt-accordion__content',
            }
        
            /*
             * Event
             */
            element.find(selector.nav).on('click', function (event) {
                event.preventDefault();
                /*
                 * Close all Tabs
                 */
                if (options.toggle){
                    element.find('.rt-accordion__item').removeClass('is-active');
                  
                    element.find(selector.item).slideUp(options.duration);

                }
                

                /** active tab menu */
                $(this).parent().addClass('is-active');

                /** Open tab content */
                $(this).parent().find(selector.item).slideDown(options.duration);


            });
        });

    };

});
