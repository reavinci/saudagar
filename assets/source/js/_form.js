jQuery(document).ready(function ($) {
  
    /*=================================================;
    /* FORM CLICK
    /*================================================= */
    var form_check_val = function(element){
        element.each(function () {
          if ($(this).val() == "") {
            $(this).parents(".rt-form").removeClass("is-active");
          } else {
            $(this).parents(".rt-form").addClass("is-active");
          }
        });

        $('.rt-form--select2').addClass("is-active");

    }

    var form_overlay = function () {

        var form = $(".js-form-overlay .rt-form__input");

        form_check_val(form);
   
        form.on('focus', function () {
             form_check_val(form);
            $(this).parents('.rt-form').addClass("is-active");

        });



    }
    form_overlay();


    // end document
});
