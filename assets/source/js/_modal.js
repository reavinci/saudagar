;
(function ($, window, document, undefined) {
    $.fn.retheme_modal = function (args) {

        return this.each(function () {

            var element = $(this);

            var defaults = {
                animatein: 'transition.fadeIn',
                animateout: 'transition.fadeOut',
                duration: '300',
                easing: 'easeIn',
            };

            var meta = element.data();
            var options = $.extend(defaults, args, meta);

            var selector = {
                close: '.jq-modal-close',
            }



            var open = function () {

                element.velocity('transition.fadeIn').addClass('is-active');
                /** modal inner show */
                element.find('.rt-modal__inner').velocity(options.animatein, {
                    duration: options.duration,
                    easing: options.easing,
                }).addClass('is-active');

                $("html").css("overflow-y", "hidden");
            }

            var close = function () {
                element.velocity('reverse', {
                    display: 'none',
                }).removeClass('is-active');
                /** modal inner hidden */
                element.find('.rt-modal__inner').velocity(options.animateout, {
                    duration: options.duration,
                    easing: options.easing,
                }).removeClass('is-active');

                $('html').css('overflow-y', 'scroll');
            }


            if (options.action == 'open'){
                open();
            }

            if (options.action == "close" && element.hasClass('is-active')) {
              close();
            }

            /**
             * Modal action on click
             */
            $(options.trigger).on('click', function (event) {
                event.preventDefault();

                if (!$(this).hasClass("disabled")) {
                    open();
                }
                
            });

            /**
             * hidden if overlay back click
             */
            $(selector.close).on("click", function(event) {
              event.preventDefault();
              close();
            });     


        });
    };
})(jQuery, window, document);