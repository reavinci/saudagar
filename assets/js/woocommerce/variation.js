// 1.3.4
jQuery(document).ready(function ($) {
  /*=================================================;
    /* VARIATION - BUTTON CLICK
    /*================================================= */
  var retheme_variation_select = function () {
    var button = $(".product_addtocart, .product_to_whatsapp");
    $("form.variations_form").on("hide_variation", function () {
      button.addClass("disabled wc-variation-selection-needed");
    });
    $("form.variations_form").on("show_variation", function () {
      button.removeClass("disabled wc-variation-selection-needed");
    });
  };
  retheme_variation_select();

  /*=================================================;
    /* VARIATION - TRIGGER
    /*================================================= */
  var retheme_found_variation = function (selector, form) {
    var form = $("form.cart");

    var variation_id = $("[name='variation_id']").val();

    var button = $(".product_addtocart");

    // product data
    var product_title = $(".product_title");
    var product_price = $(".product_price");
    var product_onsale = $(".product_onsale");
    var product_quantity = $(".product_quantity");

    // Product Title
    var title_text = product_title.first().text();
    var title = title_text.trim();

    // Image
    var imageWrapper = $(".thumbnail_wrapper");
    var image = imageWrapper.find("img");
    var imageSrc = image.attr("src");
    var imageSrcset = image.attr("srcset") || "";

    // set default data product
    var data_product = {
      product_title: title,
      product_price_html: product_price.html(),
    };

    form.attr("data-product", JSON.stringify(data_product));

    // select variation action
    $(document).on("found_variation", "form.variations_form", function (
      event,
      variation
    ) {
      var element = $(this);

      // Get product title with attribute
      var attribute = Object.values(variation.attributes).join(", ");
      var variation_title = title + " - " + attribute;
      var variation_title_html = title + " <span> - " + attribute + "</span>";

      // get image url form `variation`.
      var fullSrc = variation.image.full_src;
      var imgSrc = variation.image.src;
      var thumbSrc = variation.image.thumb_src;
      var inStock = variation.is_in_stock;

      // price
      if (variation.display_price) {
        var price_sale =
          ((variation.display_regular_price - variation.display_price) /
            variation.display_regular_price) *
          100;
        product_onsale.html(Math.round(price_sale));
      }

      // check stock
      if (inStock > 0) {
        button.removeClass("disabled wc-variation-is-unavailable");
      } else {
        button.addClass("disabled wc-variation-is-unavailable");
      }

      // Change image src image.
      if (imageSrc !== imgSrc) {
        image.removeAttr("srcset");
        imageWrapper.addClass("image-loading");
        image.attr("src", imgSrc).one("load", function () {
          imageWrapper.removeClass("image-loading");
        });
      } else if (imgSrc) {
        image.attr("src", imgSrc);
      }

      // set attribute on form
      var data_product = {
        product_title: title,
        product_price_html: product_price.html(),
        variation_title: variation_title,
        variation_title_html: variation_title_html,
        variation_price_html: variation.display_price,
      };

      $(this).attr("data-product", JSON.stringify(data_product));

      // replace html dom with new value
      product_title.html(variation_title_html);

      // change price if product have price variation
      if (variation.price_html) {
        product_price.html(variation.price_html);
      }
    });

    // Reset variation link click
    $(".reset_variations").on("click", function (event) {
      event.preventDefault();

      // get data form product attribute
      var product_data = JSON.parse(form.attr("data-product"));

      // reset default data
      product_title.html(product_data.product_title);
      product_price.html(product_data.product_price_html);

      // Change src image.
      if (imageSrc) {
        image.attr("src", imageSrc);
        image.attr("srcset", imageSrcset);
      }
    });
  };
  retheme_found_variation();
});

class retheme_product_variation {
  get_variation(variation_id) {
    const form = document.querySelectorAll(".variations_form")[0];
    const variations = JSON.parse(form.getAttribute("data-product_variations"));
    return variations.find(
      (variation) => variation.variation_id === variation_id
    );
  }
  selection_needed() {
    var elements = document.querySelectorAll(".product_addtocart");
    elements.forEach(function (element, index) {
      element.addEventListener("click", function (event) {
        const el = event.target;
        if (el.classList.contains("wc-variation-selection-needed")) {
          window.alert(
            wc_add_to_cart_variation_params.i18n_make_a_selection_text
          );
        }
      });
    });
  }
  is_variantion() {
    var variations = document.querySelector("input.variation_id");
    if (variations) {
      return true;
    } else {
      return false;
    }
  }
}
class retheme_product_helper {
  money_format(number) {
    if (number) {
      var numberInt = parseInt(number);
      return "Rp." + Number(numberInt.toFixed(1)).toLocaleString();
    } else {
      console.log("not format number");
    }
  }
}

var variation = new retheme_product_variation();
variation.selection_needed();
