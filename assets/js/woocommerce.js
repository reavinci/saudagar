jQuery(document).ready(function ($) {
  /*=================================================;
   /* QTY FORM
   /*================================================= */
  var qty_count = function () {
    var form = $(".quantity_change");
    var quantity = form.find(".input-text.qty");

    $(document).on("click", ".rt-qty .minus, .rt-qty .plus", function (event) {
      event.preventDefault();

     
      // Get current quantity values
      var qty = $(this).closest(".rt-qty").find(".qty");
      var val = parseFloat(qty.val());
      var max = parseFloat(qty.attr("max"));
      var min = parseFloat(qty.attr("min"));
      var step = parseFloat(qty.attr("step"));

      // Change the value if plus or minus
      if ($(this).is(".plus")) {
        if (max && max <= val) {
          qty.val(max);
        } else {
          qty.val(val + step);
        }
        
      } else {
        if (min && min >= val) {
          qty.val(min);
        } else if (val > 1) {
          qty.val(val - step);
        }

      }
    });
  };
  qty_count();

  /** Action after cart update */
  // $(document.body).on("updated_cart_totals updated_checkout", function () {
  //   qty_count();
  // });

  /*=================================================;
    /* ACCOUNT LOGIN REGISTER
    /*================================================= 
    * @desc toggle form login and register on my account page
    */

  var account_login_register = function () {
    var trigger_login = $(".js-account-login");
    var trigger_register = $(".js-account-register");

    var box_login = $(".rt-form-login, .js-account-login-title");
    var box_register = $(".rt-form-register, .js-account-login-register");

    trigger_login.on("click", function (event) {
      event.preventDefault();

      box_login.show().addClass("is-active");
      box_register.hide().removeClass("is-active");
    });

    trigger_register.on("click", function (event) {
      event.preventDefault();
      box_register.show().addClass("is-active");
      box_login.hide().removeClass("is-active");
    });
  };
  account_login_register();

  /*=================================================;
    /* GALLERY CAROUSEL
    /*================================================= */
  var retheme_woocommerce_gallery_slider = function () {
    $(".flex-control-thumbs").addClass("rt-slider owl-carousel");
    $(".flex-control-thumbs").owlCarousel({
      loop: false,
      autoplay: false,
      margin: 10,
      dots: false,
      nav: true,
      navText: [
        "<i class='ti-angle-left'></i>",
        "<i class='ti-angle-right'></i>",
      ],
      responsive: {
        0: {
          items: 5,
        },
        720: {
          items: 5,
        },
        960: {
          items: 5,
        },
      },
    });
  };
  retheme_woocommerce_gallery_slider();

  /*=================================================;
  /* COUPON
  /*================================================= */
  var retheme_woocommerce_coupon = function () {
    var coupon = $(".js-form-coupon");
    var coupon_trigger = $(".js-coupon-trigger");
    // move coupon after review
    coupon.insertBefore(".rt-coupon-modal-action");

    coupon_trigger.on("click", function (event) {
      event.preventDefault();

      coupon.slideToggle().addClass("is-active");
    });
  };

  retheme_woocommerce_coupon();

  //end jquery
});
