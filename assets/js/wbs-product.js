(function ($, window, document, undefined) {
  /*=================================================;
  /* HELPER FUNCTION
  /*================================================= */
  $.fn.hasData = function (key) {
    return typeof $(this).data(key) != "undefined";
  };

  $.fn.getData = function (key) {
    var data = false;
    if ($(this).data(key) != "undefined") {
      var data = $(this).data(key);
    }
    return data;
  };

  /*=================================================;
    /* STICKY PRODUCT
    /*================================================= */
  var sticky_product = function () {
    var element = $(".js-sticky-product");
    var section = $("form.cart");
    var trigger = $(".js-sticky-product-action");

    $(window).on("scroll", function (event) {
      var position = $(".js-sticky-product-trigger").offset().top;

      if ($(this).scrollTop() > position - 100) {
        element.addClass("is-sticky");
        $("body").addClass("sticky-product");
      } else {
        element.removeClass("is-sticky");
        $("body").removeClass("sticky-product");
      }
    });

    // if variable product klik
    trigger.on("click", function (event) {
      section.velocity("scroll", {
        duration: 750,
        easing: "ease-in",
        offset: -100,
      });
    });
  };

  sticky_product();
})(jQuery, window, document);
