/*=================================================
*  AJAX CALL
/* =================================================== */
jQuery(document).ready(function ($) {

    var element = $(".js-post-counter");
    var data = {
      action: "ajax_post_count_result",
      query: post_counter.posts,
      post_id: element.attr("id"),
      check_nonce: post_counter.check_nonce,
    };

    jQuery.ajax({
      url: post_counter.ajaxurl,
      type: "post",
      dataType: "html",
      data: data,
      success: function (response) {
        element.text(response);
      },
    });
});