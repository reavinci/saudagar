<?php
foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
    $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
    $product_name = '';
    $product_attribute = '';
    if ($_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
        
        // get parent name
        if($_product->get_parent_id()){
            $product_parent = wc_get_product($_product->get_parent_id());
            $product_name = apply_filters('woocommerce_cart_item_name', $product_parent->get_name(), $cart_item, $cart_item_key);
            $product_attribute = implode(', ', $_product->get_attributes());
        }else{
            $product_name = apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key);
        }

        $thumbnail         	= apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image('thumbnail'), $cart_item, $cart_item_key );
        $product_price     	= apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
        $product_permalink 	= apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
        $product_subtotal 	= apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
        $product_price_quantity = apply_filters('woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf('%s &times; %s', $cart_item['quantity'], $product_price) . '</span>', $cart_item, $cart_item_key);

        // quantity
        if ($_product->is_sold_individually()) {
            $product_quantity = sprintf('<input type="hidden" name="quantity" value="1" />', $cart_item_key);
        } else {
            $product_quantity = woocommerce_quantity_input(array(
                'input_name' => "quantity",
                'input_value' => $cart_item['quantity'],
                'max_value' => $_product->get_max_purchase_quantity(),
                'min_value' => '0',
                'product_name' => $_product->get_name(),
            ), $_product, false);
        }

        $product_quantity = apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item); // PHPCS: XSS ok.

        ?>

        <div class="rt-cart-item js-cart-item">
            <div class="rt-cart-item__thumbnail rt-img rt-img-full">
                <?php echo $thumbnail ?>
            </div>
            <div class="rt-cart-item__body pall-0">
                
                <h5 class="rt-cart-item__title">
                    <?php if (empty($product_permalink)): ?>
                        <?php echo $product_name; ?>
                    <?php else: ?>
                        <a href="<?php echo esc_url($product_permalink); ?>">
                            <?php echo $product_name; ?>
                        </a>
                    <?php endif;?>
                </h5>

                <?php if ($product_attribute): ?>
                <div class="rt-cart-item__meta">
                    <?php echo $product_attribute ?>
                </div>
                <?php endif;?>

                <div class="rt-cart-item__quantity is-hidden">
                    <?php echo $product_quantity?>
                </div>

                <div class="rt-cart-item__price"><?php echo $product_price_quantity ?></div>
            </div>
            <div class="rt-cart-item__remove">  <a href="#" class="js-remove-cart-button">&times;</a></div>
            <input type="hidden" name="product_id" value="<?php echo $_product->get_id();?>">
            <input type="hidden" name="cart_item_key" value="<?php echo $cart_item_key?>">
        </div>

        <?php
    }
}
