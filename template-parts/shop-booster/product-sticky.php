<?php global $product; ?>

<div class="wsb-sticky-product js-sticky-product">
    <div class="page-container">
        <div class="wsb-sticky-product__item thumbnail_wrapper">
        
            <?php rt_the_post_thumbnail('thumbnail');?>

            <h6 class="mb-0">
                <span class="wsb-sticky-product__title product_title"><?php the_title() ?></span>
                <span class="wsb-sticky-product__price product_price"><?php echo $product->get_price_html(); ?></span>
            </h6>
            
        </div>

        
        <div class="wsb-sticky-product__action">
            <?php if($product->is_in_stock() || $product->is_type('variable')): ?>

                <?php do_action('wsb_before_add_to_cart_button')?>

                <?php if(rt_option('woocommerce_catalog', false) !== true): ?>

                    <?php if($product->is_type('variable')):?>
                        <a class="rt-btn rt-btn--action product_addtocart button single_add_to_cart_button"><?php _e('Select options', 'woocommerce') ?></a>
                    <?php else: ?>
                        <?php  woocommerce_template_loop_add_to_cart();?>
                    <?php endif;?>

                <?php endif; ?>
                
                <?php do_action('wsb_after_add_to_cart_button')?>

            <?php else: ?>
                <a class="rt-btn rt-btn--second" href="#"><?php echo __('Out of stock', 'woocommerce') ?></a>
            <?php endif ?>
        </div>

        
    </div>
</div>