<?php 
$account_url = '#';
$trigger_modal_class = 'jq-modal-login-trigger';

if (is_user_logged_in() || rt_is_woocommerce('account_page')) {
   $account_url = get_permalink( get_option('woocommerce_myaccount_page_id') );
   $trigger_modal_class = '';
}

?>

<div class="wsb-toolbar js-wsb-toolbar">

    <?php do_action('wsb_before_toolbar') ?>

    <?php if(rt_option('toolbar_shop', true) && rt_is_woocommerce('shop')): ?>
    <div class="wsb-toolbar__item">
         <a href="<?php echo get_site_url(); ?>">
            <span class="wsb-toolbar__icon"><i class="fa fa-home"></i></span>
            <span class="wsb-toolbar__label"><?php _e('Home', 'webforia-shop-booster') ?></span>
        </a>
    </div>
    <?php endif ?>

    <?php if (rt_option('toolbar_shop', true) && !rt_is_woocommerce('shop')): ?>
    <div class="wsb-toolbar__item">
         <a href="<?php echo get_permalink(woocommerce_get_page_id('shop')); ?>">
            <span class="wsb-toolbar__icon"><i class="fa fa-home"></i></span>
            <span class="wsb-toolbar__label"><?php _e('Shop', 'webforia-shop-booster')?></span>
        </a>
    </div>
    <?php endif?>


    <?php if(rt_option('toolbar_filter', true) && rt_is_woocommerce('shop')): ?>
    <div class="wsb-toolbar__item">
         <a href="#" class="js-filter-panel-trigger">
            <span class="wsb-toolbar__icon"><i class="fa fa-filter"></i></span>
            <span class="wsb-toolbar__label"><?php _e('Filter', 'webforia-shop-booster') ?></span>
        </a>
    </div>
    <?php endif ?>

    <?php if(rt_option('toolbar_account', true)): ?>
    <div class="wsb-toolbar__item">
         <a class="<?php echo $trigger_modal_class?>" href="<?php echo esc_url($account_url)?>">
            <span class="wsb-toolbar__icon"><i class="fas fa-user"></i></span>
            <span class="wsb-toolbar__label"><?php _e('User', 'webforia-shop-booster') ?></span>
        </a>
    </div>
    <?php endif ?>

    <?php if(rt_option('toolbar_cart', true)): ?>
    <div class="wsb-toolbar__item">
        <a href="#" class="js-cart-trigger">
            <span class="wsb-toolbar__icon">
                 <i class="fa fa-shopping-cart"></i><span class="rt-header__cart-count wsb-toolbar__cart-count js-cart-total">0</span>
            </span>
            <span class="wsb-toolbar__label"><?php _e('Cart', 'webforia-shop-booster') ?></span>
        </a>
    </div>
    <?php endif ?>

    <?php do_action('wsb_after_toolbar')?>

</div>