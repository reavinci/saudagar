<?php
$classes[] = 'page-header';
$classes[] = 'page-header--' . rt_option('page_header_style', 'left');
?>


<section id="page-header" <?php rt_set_class('rt_page_header_class', $classes)?>>
  <div class="page-container">

    <?php do_action('rt_page_header_prepend')?>

    <?php if (rt_get_page_title()): ?>
    <div class="page-header__inner">

      <h1 class="page-header__title"><?php echo rt_get_page_title() ?></h1>
      
      <?php if (rt_get_page_desc()): ?>
      <div class="page-header__desc">
        <?php echo rt_get_page_desc() ?>
      </div>
      <?php endif;?>

    </div>
    <?php endif?>


   <?php rt_breadcrumb()?>

    <?php do_action('rt_page_header_append')?>

  </div>
</section>