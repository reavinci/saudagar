
<?php

$classes[] = 'rt-menu rt-menu--canvas js-mobile-menu';
$classes[] = !empty(rt_get_field('header_overlay')) ? 'is-overlay' : '';

/** Options */
$elements = rt_option_header();

if (rt_is_premium()) {
    $sticky = rt_option('header_mobile_sticky', true);
}

if (rt_is_free()) {
    $sticky = false;
}
?>

<div id="header-mobile" class="rt-header-mobile js-header-mobile"  data-sticky='<?php echo $sticky ?>'>

  <div class="rt-header-mobile__main">
    <div class="page-container">

       <div id="header-mobile-left" class="rt-header-mobile__column" >
         <a class="rt-header__element rt-header-icon" href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>">
            <i class="ti-arrow-left rt-header__cart-icon"></i>
        </a>
      </div>

       <div id="header-mobile-center" class="rt-header-mobile__column">
           <h5 class="rt-header__element mall-0"><?php echo rt_the_title(25) ?></h5>
      </div>

       <div id="header-mobile-right" class="rt-header-mobile__column" >
           <div class="rt-header__element rt-header-icon rt-header__cart">
            <i class="ti-shopping-cart rt-header__cart-icon js-cart-trigger"></i>  <span class="rt-header__cart-count js-cart-total">0</span>
            </div>
      </div>

    </div>

  </div>

  <?php rt_get_template_part('header/header-search');?>

</div>