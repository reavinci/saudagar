<?php // Plan - Premium ?>
<?php 
$classes[] = 'rt-main-canvas-menu   rt-sidepanel rt-sidepanel--left js-sidepanel'; 
$classes[] = 'rt-main-canvas-menu--' . rt_option('header_drawer_menu_schema', 'dark');

?>

<div <?php rt_set_class('header_mobile_drawer', $classes); ?> data-trigger=".js-mobile-menu-trigger">

    <div class="rt-sidepanel__overlay js-sidepanel-close"></div>

    <div class="rt-sidepanel__inner">


        <div class="rt-sidepanel__header">
            <a class="rt-sidepanel__close js-sidepanel-close"><i class="ti-close"></i></a>
            <h4 class="rt-sidepanel__title"><?php _e('Menu', RT_THEME_DOMAIN)?></h4>
        </div>

        <div class="rt-sidepanel__body">
            <?php if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('mobile_drawer')): ?>
              <?php do_action('rt_mobile_drawer')?>
             <?php endif ?>
        </div>

    </div>

</div>