<?php // Plan - Premium ?>

<?php if (rt_option('header_sticky', true)): ?>

<?php $elements = rt_option_header(); ?> 
  
  <div id="header-sticky" class="rt-header__sticky">
      <div class="page-container">

        <div id="header-sticky-left" class="rt-header__column" data-alignment="<?php echo $elements['sticky_left_alignment'] ?>" data-display="<?php echo $elements['sticky_left_display'] ?>">
          <?php  do_action('rt_header_sticky_left')?>
      </div>
      
       <div id="header-sticky-center" class="rt-header__column" data-alignment="<?php echo $elements['sticky_center_alignment'] ?>" data-display="<?php echo $elements['sticky_center_display'] ?>">
         <?php  do_action('rt_header_sticky_center')?>
      </div>

       <div id="header-sticky-right" class="rt-header__column" data-alignment="<?php echo $elements['sticky_right_alignment'] ?>" data-display="<?php echo $elements['sticky_right_display'] ?>">
         <?php  do_action('rt_header_sticky_right')?>
      </div>


      </div>
  </div>
<?php endif; ?>
