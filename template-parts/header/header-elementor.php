<?php // Plan - Premium ?>

<?php if (rt_option('header_layout_builder_elementor') && rt_option('header_layout_builder_elementor') != get_the_ID() && did_action( 'elementor/loaded' )): ?>
<?php
$classes[] = 'rt-header-builder';
?>

<div <?php rt_set_class('rt_header_builder', $classes)?>>
    <?php echo Elementor\Plugin::instance()->frontend->get_builder_content_for_display(rt_option('header_layout_builder_elementor')); ?>
</div>

<?php endif?>


