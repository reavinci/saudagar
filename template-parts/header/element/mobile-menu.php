<?php
$classes[] = 'rt-menu rt-menu--vertical js-menu';
?>

<div  <?php rt_set_class('rt_mobile_menu_class', $classes)?>>

<?php
if (has_nav_menu('mobile')) {
    $args = array(
        'container' =>  '',
        'menu_class' => 'rt-menu__main',
        'theme_location' => 'mobile',
        'fallback_cb' => 'rt_menu_fallback',
        'walker' => ''
    );
}else{
	$args = array(
        'container' =>  '',
        'menu_class' => 'rt-menu__main',
        'theme_location' => 'primary',
        'fallback_cb' => 'rt_menu_fallback',
        'walker' => ''
    );
}

wp_nav_menu($args);
?>

</div>
