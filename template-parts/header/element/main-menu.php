<?php

$classes[] = 'rt-menu rt-menu--horizontal js-menu';
?>

<div  <?php rt_set_class('rt_main_menu_class', $classes) ?>
data-animatein='<?php echo rt_option('header_main_submenu_animation', 'transition.fadeIn') ?>'
data-duration='<?php echo rt_option('header_main_submenu_animation_duration', '300') ?>'>

<?php 
if(has_nav_menu('primary')){
    $args = array(
        'container' =>  false,
        'menu_class' => 'rt-menu__main',
        'theme_location' => 'primary',
        'fallback_cb' => 'rt_menu_fallback',
    );

    wp_nav_menu($args);
}
?>
</div>