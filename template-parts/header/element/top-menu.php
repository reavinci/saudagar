<?php
if (has_nav_menu('topbar')) {
	$args = array(

		'title_li' => '',
		'container' => 'top-menu',
		'theme_location' => 'topbar',
		'items_wrap' => '<div class="rt-menu rt-menu--horizontal"><ul class="rt-menu__main">%3$s</ul></div>',
		'fallback_cb' => 'rt_menu_fallback',
        'walker' => ''

	);
	wp_nav_menu($args);
}
