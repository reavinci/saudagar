<div class="rt-entry-content">
    <h2><?php echo __("Unfortunately, the page you requested could not be found.", RT_THEME_DOMAIN) ?></h2>
    <p><?php echo __("It looks like the link pointing here was faulty. Maybe try searching?", RT_THEME_DOMAIN) ?></p>
    <?php get_template_part('searchform') ?>
</div>