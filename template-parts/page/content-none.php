<div class="rt-entry-content">
	<?php 
	if(is_home() && current_user_can('publish_posts')){
		$content = sprintf(esc_html__('Ready to publish your first post? %1$sGet started here%2$s.', RT_THEME_DOMAIN), '<a href="'. esc_url(admin_url('post-new.php')) .'" target="_blank">', '</a>');
	}elseif(is_search()){
		$content = __("Sorry, but nothing matched your search terms. Please try again with different keywords.", RT_THEME_DOMAIN);
		get_template_part('searchform');
	}elseif(is_category()){
		$content = esc_html_e("There aren't any posts currently published in this category.", RT_THEME_DOMAIN);
	}elseif(is_tax()) {
		$content = esc_html_e("There aren't any posts currently published under this taxonomy.", RT_THEME_DOMAIN);
	}elseif(is_tag()){
		$content = esc_html_e("There aren't any posts currently published under this tag.", RT_THEME_DOMAIN); 
	}else{
		$content = esc_html_e('It seems we can&rsquo;t find what you&rsquo;re looking for.', RT_THEME_DOMAIN);
	}

	echo $content;
	
	?>
</div><!-- .page-content -->